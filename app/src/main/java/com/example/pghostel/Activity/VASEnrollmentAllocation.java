package com.example.pghostel.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.ModelClass.VASEnrollmentListResponse;
import com.example.pghostel.ModelClass.VASListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VASEnrollmentAllocation extends AppCompatActivity {

    private MaterialBetterSpinner tenant;
    private MaterialBetterSpinner VASList;
    private MaterialBetterSpinner CotList;
    private Spinner BillPlan;
    private EditText startDate;
    private Button vasAllocation;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private VASListResponse vasListResponse;
    private TenantListResponse tenantListResponse;
    private VASEnrollmentListResponse vasEnrollmentListResponse1;
    private int tenantID, vasID;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String VID,planBill = "";
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(VASEnrollmentAllocation.this,VASEnrollmentList.class));
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(VASEnrollmentAllocation.this,VASEnrollmentList.class));
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotenrollment);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        tenant = (MaterialBetterSpinner) findViewById(R.id.material_spinner4);
        VASList = (MaterialBetterSpinner) findViewById(R.id.material_spinner5);
        VASList.setHint("VAS");
        CotList = (MaterialBetterSpinner) findViewById(R.id.material_spinner3);
        CotList.setVisibility(View.GONE);
        BillPlan = (Spinner) findViewById(R.id.material_spinner6);
        startDate = (EditText) findViewById(R.id.password);
        vasAllocation = (Button) findViewById(R.id.adduser);

        VID = getIntent().getStringExtra("VID");

        if(Objects.equals(VID, "")) {
            vasAllocation.setText("VAS Allocation");

            pDialog = ProgressDialog.show(VASEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");
        }
        else {
            vasAllocation.setText("Update VAS Allocation");

            Map<String, Object> requestBody = new HashMap<>();
            Map RoomDetails = new LinkedHashMap(1);
            RoomDetails.put("VASEnrollmentId", VID);
            requestBody.put("Operator", "R");
            requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentAllocation.this, "JWT"));
            requestBody.put("VASEnrollment", RoomDetails);

            pDialog = ProgressDialog.show(VASEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

            getcotEnrollmentlist(requestBody);
        }

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("BranchId", manager.getSharedPreferencesValues(VASEnrollmentAllocation.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentAllocation.this, "JWT"));


        getVaslist(requestBody);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(VASEnrollmentAllocation.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String dayConverted = ""+dayOfMonth;
                                if(dayOfMonth<10){
                                    dayConverted = "0"+dayConverted;
                                }

                                String monthConverted = ""+(monthOfYear + 1);
                                if((monthOfYear + 1)<10){
                                    monthConverted = "0"+monthConverted;
                                }

                                startDate.setText(year + "-" + monthConverted + "-" + dayConverted);
                                //picker.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        tenant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                tenantID = position;
            }
        });
        VASList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                vasID = position;
            }
        });

        final List<String> billplanlist = new ArrayList<String>();
        billplanlist.add("Daily");
        billplanlist.add("Weekly");
        billplanlist.add("Monthly");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(VASEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, billplanlist);
        BillPlan.setAdapter(adapter1);
        BillPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                planBill =  billplanlist.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        vasAllocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(planBill.equals("")){
                    Toast.makeText(VASEnrollmentAllocation.this, "", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map AppUserDetails = new LinkedHashMap(3);
                AppUserDetails.put("TenantId", tenantListResponse.getResponse().get(tenantID).getTenantId());
                AppUserDetails.put("VASId", vasListResponse.getResponse().get(vasID).getVASId());
                AppUserDetails.put("StartDate", startDate.getText().toString().trim());
                AppUserDetails.put("BillPlan", "Monthly");
                //AppUserDetails.put("BillPlan", planBill);
                AppUserDetails.put("VASEnrollmentActiveInd", "Y");
                requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentAllocation.this, "JWT"));



                if(Objects.equals(VID, "")) {
                    requestBody.put("Operator", "C");
                }else {
                    requestBody.put("Operator", "U");
                    AppUserDetails.put("VASEnrollmentId", VID);
                }
                requestBody.put("VASEnrollment", AppUserDetails);

                if(Objects.equals(VID, "")) {
                    pDialog = ProgressDialog.show(VASEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
                    AllocatingCot(requestBody,"Added");
                }else {
                    pDialog = ProgressDialog.show(VASEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
                    AllocatingCot(requestBody,"Updated");
                }
            }
        });
    }

    private void getcotEnrollmentlist(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getVASEnrollmentList(requestBody).enqueue(new Callback<VASEnrollmentListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<VASEnrollmentListResponse> call, Response<VASEnrollmentListResponse> response) {
                try {
                    pDialog.dismiss();
                    vasEnrollmentListResponse1 = new VASEnrollmentListResponse();
                    vasEnrollmentListResponse1 = response.body();
                    if (vasEnrollmentListResponse1 == null) {
                        Toast.makeText(VASEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (vasEnrollmentListResponse1.getMessage().trim().equals("Success")) {
                        startDate.setText(vasEnrollmentListResponse1.getResponse().get(0).getStartDate());
                    } else {
                        Toast.makeText(VASEnrollmentAllocation.this, vasEnrollmentListResponse1.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VASEnrollmentListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void AllocatingCot(Map<String, Object> requestBody, final String s) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddVASEnrollment(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    AddBranchResponse addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(VASEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(VASEnrollmentAllocation.this, "Allocating has been "+s+" successfully..!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(VASEnrollmentAllocation.this,VASEnrollmentList.class));
                        finish();
                    } else {
                        Toast.makeText(VASEnrollmentAllocation.this, addBranchResponse.getError(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getVaslist(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getVASList(requestBody).enqueue(new Callback<VASListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<VASListResponse> call, Response<VASListResponse> response) {
                try {

                    vasListResponse = response.body();
                    if (vasListResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(VASEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (vasListResponse.getResponse().isEmpty()) {
                        pDialog.dismiss();
                        Toast.makeText(VASEnrollmentAllocation.this, "No Room Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<String> usertypelist = new ArrayList<String>();
                    if (vasListResponse != null) {
                        for (VASListResponse.Response list : vasListResponse.getResponse()) {
                            usertypelist.add(list.getVAS());
                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(VASEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, usertypelist);
                    VASList.setAdapter(adapter);
                    Map<String, Object> requestBody = new HashMap<>();

                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentAllocation.this, "JWT"));

                    getListTenant(requestBody);

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VASListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getListTenant(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListTenant(requestBody).enqueue(new Callback<TenantListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<TenantListResponse> call, Response<TenantListResponse> response) {
                try {
                    pDialog.dismiss();
                    tenantListResponse = new TenantListResponse();
                    tenantListResponse = response.body();

                    if (tenantListResponse == null) {
                        Toast.makeText(VASEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (tenantListResponse.getMessage().trim().equals("Success")) {

                        if (tenantListResponse.getResponse().isEmpty()) {
                            Toast.makeText(VASEnrollmentAllocation.this, "Tenant list is empty..!!", Toast.LENGTH_SHORT).show();
                        }
                        List<String> employeelist = new ArrayList<String>();
                        if (tenantListResponse != null) {
                            for (TenantListResponse.Response list : tenantListResponse.getResponse()) {
                                employeelist.add(list.getTenantName());
                            }
                        }
                        if(employeelist.isEmpty()){
                            employeelist.add("Tenant is Empty..!!");
                            //tenant.setEnabled(false);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(VASEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, employeelist);
                        tenant.setAdapter(adapter);

                    } else {

                        Toast.makeText(VASEnrollmentAllocation.this, tenantListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TenantListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(VASEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

