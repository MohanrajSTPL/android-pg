package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AppUserListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.appUserListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppUserList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private AppUserListResponse appUserListResponse;
    private SessionManager manager;
    private ProgressDialog pDialog;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(AppUserList.this, 1));

        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(AppUserList.this, 1));

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(AppUserList.this, "JWT"));

        getCotList(requestBody);

    }


    public void Refereshview() {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(AppUserList.this, "JWT"));
        getCotList(requestBody);
    }



    private void getCotList(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAppUserList(requestBody).enqueue(new Callback<AppUserListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AppUserListResponse> call, Response<AppUserListResponse> response) {
                try {
                    appUserListResponse = new AppUserListResponse();
                    appUserListResponse = response.body();
                    if (appUserListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(AppUserList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (appUserListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (appUserListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        appUserListAdapter b = new appUserListAdapter(AppUserList.this, appUserListResponse.getResponse());
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(AppUserList.this, appUserListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(AppUserList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AppUserListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(AppUserList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {

    }
    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }


}


