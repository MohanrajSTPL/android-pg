package com.example.pghostel.Activity;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.ModelClass.RoomFacilityListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.RoomFacilityAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomFacility extends AppCompatActivity {


    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private RoomFacilityListResponse roomFacilityListResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private int RT_id;
    RoomFacilityAdapter roomFacilityAdapter;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewfacility);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(RoomFacility.this, 1));

        RT_id = getIntent().getIntExtra("ID", 0);

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("RoomTypeId", String.valueOf(RT_id));
        requestBody.put("JWT", manager.getSharedPreferencesValues(RoomFacility.this, "JWT"));

        getBranchListDetails(requestBody);

    }
    public void onClickCalled() {
        // Call another acitivty here and pass some arguments to it.
        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("RoomTypeId", String.valueOf(RT_id));
        requestBody.put("JWT", manager.getSharedPreferencesValues(RoomFacility.this, "JWT"));

        getBranchListDetails(requestBody);
    }
    public void getBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getRoomTypeFacilityListDetails(requestBody).enqueue(new Callback<RoomFacilityListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<RoomFacilityListResponse> call, Response<RoomFacilityListResponse> response) {
                try {
                    roomFacilityListResponse = response.body();
                    if (roomFacilityListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(RoomFacility.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (roomFacilityListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (roomFacilityListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                            return;
                        }
                        roomFacilityAdapter = new RoomFacilityAdapter(RoomFacility.this, roomFacilityListResponse.getResponse());
                        recyclerView.setAdapter(roomFacilityAdapter);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(RoomFacility.this, roomFacilityListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(RoomFacility.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RoomFacilityListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(RoomFacility.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
