package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CotEnrollmentListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("BillPlan")
        private String BillPlan;
        @Expose
        @SerializedName("RoomType")
        private String RoomType;
        @Expose
        @SerializedName("RoomTypeId")
        private String RoomTypeId;
        @Expose
        @SerializedName("RoomNo")
        private String RoomNo;
        @Expose
        @SerializedName("CotNo")
        private String CotNo;
        @Expose
        @SerializedName("CotId")
        private String CotId;
        @Expose
        @SerializedName("TenantName")
        private String TenantName;
        @Expose
        @SerializedName("TenantNo")
        private String TenantNo;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;
        @Expose
        @SerializedName("CotEnrollmentId")
        private String CotEnrollmentId;

        public String getStartDate() {
            return StartDate;
        }

        public String getBillPlan() {
            return BillPlan;
        }

        public String getRoomType() {
            return RoomType;
        }

        public String getRoomTypeId() {
            return RoomTypeId;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public String getCotNo() {
            return CotNo;
        }

        public String getCotId() {
            return CotId;
        }

        public String getTenantName() {
            return TenantName;
        }

        public String getTenantNo() {
            return TenantNo;
        }

        public String getTenantId() {
            return TenantId;
        }

        public String getCotEnrollmentId() {
            return CotEnrollmentId;
        }
    }
}
