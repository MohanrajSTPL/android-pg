package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.ModelClass.VASListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.RoomListAdapter;
import com.example.pghostel.adapter.VASListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class VASList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private VASListResponse vasListResponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(VASList.this, 1));


        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(VASList.this, 1));

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("BranchId", manager.getSharedPreferencesValues(VASList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(VASList.this, "JWT"));


        getVASList(requestBody);
        //getServiceTypeDetails(requestBody);


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(VASList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(VASList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(VASList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<VASListResponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (VASListResponse.Response list : vasListResponse.getResponse()) {

                        if (list.getVASActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (VASListResponse.Response list : vasListResponse.getResponse()) {

                        if (list.getVASActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = vasListResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(VASList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                VASListAdapter b = new VASListAdapter(VASList.this, responses, VASList.this);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    public void Refereshview() {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("BranchId", manager.getSharedPreferencesValues(VASList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(VASList.this, "JWT"));
        getVASList(requestBody);
    }


    private void getVASList(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getVASList(requestBody).enqueue(new Callback<VASListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<VASListResponse> call, Response<VASListResponse> response) {
                try {
                    // if (response.code() == 201) {
                    vasListResponse = new VASListResponse();
                    vasListResponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (vasListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (vasListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (vasListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        VASListAdapter b = new VASListAdapter(VASList.this, vasListResponse.getResponse(), VASList.this);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASList.this, vasListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(VASList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VASListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(VASList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(VASList.this)).inflate(R.layout.addroompopup, viewGroup, false);


        final TextView roomtittle = (TextView) dialogView.findViewById(R.id.roomtittle);
        final TextView roomnametv = (TextView) dialogView.findViewById(R.id.roomnametv);
        final TextView costperdaytv = (TextView) dialogView.findViewById(R.id.costperdaytv);
        final TextView costpermonthtv = (TextView) dialogView.findViewById(R.id.costpermonthtv);
        final EditText name = (EditText) dialogView.findViewById(R.id.roomname);
        final EditText cot = (EditText) dialogView.findViewById(R.id.Noofcot);
        final LinearLayout cotLL = (LinearLayout) dialogView.findViewById(R.id.noofcotLL);
        final EditText cost = (EditText) dialogView.findViewById(R.id.cost);
        final EditText costpermonth = (EditText) dialogView.findViewById(R.id.costpermonth);
        final EditText desc = (EditText) dialogView.findViewById(R.id.note);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);
        roomnametv.setText("VAS");
        roomtittle.setText("VAS");
        costperdaytv.setText("CostPerDay");
        costpermonthtv.setText("CostPerMonth");
        cotLL.setVisibility(View.GONE);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(VASList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0) {
                    Toast.makeText(VASList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("VAS", name.getText().toString().trim());
                RoomDetails.put("VASDesc", desc.getText().toString().trim());
                RoomDetails.put("CostPerMonth", costpermonth.getText().toString().trim());
                RoomDetails.put("CostPerDay", cost.getText().toString().trim());
                RoomDetails.put("VASActiveInd", "Y");
                RoomDetails.put("BranchId", manager.getSharedPreferencesValues(VASList.this, "BranchID"));
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(VASList.this, "JWT"));
                requestBody.put("ValueAddedServices", RoomDetails);
                bar.setVisibility(View.VISIBLE);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(VASList.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddVAS(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(VASList.this, "VAS has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();

                    } else {
                        Toast.makeText(VASList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(VASList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(VASList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {

    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appuserlist, menu);
        MenuItem item = menu.findItem(R.id.appuserlist);
        item.setTitle("VAS Enrollment");
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.appuserlist) {
            Intent i = new Intent(VASList.this, VASEnrollmentList.class);
            i.putExtra("CID","");
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
