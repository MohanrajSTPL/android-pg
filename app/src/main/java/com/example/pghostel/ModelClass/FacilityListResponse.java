package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FacilityListResponse {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("BranchId")
        @Expose
        private String branchId;
        @SerializedName("FacilityId")
        @Expose
        private String facilityId;
        @SerializedName("FacilityName")
        @Expose
        private String facilityName;
        @SerializedName("FacilityDesc")
        @Expose
        private String facilityDesc;
        @SerializedName("CostPerMonth")
        @Expose
        private String costPerMonth;
        @SerializedName("Notes")
        @Expose
        private String notes;
        @SerializedName("BranchFacilityActiveInd")
        @Expose
        private String branchFacilityActiveInd;

        public String getBranchId() {
            return branchId;
        }

        public void setBranchId(String branchId) {
            this.branchId = branchId;
        }

        public String getFacilityId() {
            return facilityId;
        }

        public void setFacilityId(String facilityId) {
            this.facilityId = facilityId;
        }

        public String getFacilityName() {
            return facilityName;
        }

        public void setFacilityName(String facilityName) {
            this.facilityName = facilityName;
        }

        public String getFacilityDesc() {
            return facilityDesc;
        }

        public void setFacilityDesc(String facilityDesc) {
            this.facilityDesc = facilityDesc;
        }

        public String getCostPerMonth() {
            return costPerMonth;
        }

        public void setCostPerMonth(String costPerMonth) {
            this.costPerMonth = costPerMonth;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getBranchFacilityActiveInd() {
            return branchFacilityActiveInd;
        }

        public void setBranchFacilityActiveInd(String branchFacilityActiveInd) {
            this.branchFacilityActiveInd = branchFacilityActiveInd;
        }
    }
}
