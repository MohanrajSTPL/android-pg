package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportsResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("NoOfCotsAvailable")
        private String NoOfCotsAvailable;
        @Expose
        @SerializedName("IsOccupied")
        private String IsOccupied;
        @Expose
        @SerializedName("RoomNo")
        private String RoomNo;
        @Expose
        @SerializedName("RoomId")
        private String RoomId;
        @Expose
        @SerializedName("RoomType")
        private String RoomType;
        @Expose
        @SerializedName("RoomTypeId")
        private String RoomTypeId;
        @Expose
        @SerializedName("BranchName")
        private String BranchName;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;

        public String getNoOfCotsAvailable() {
            return NoOfCotsAvailable;
        }

        public String getIsOccupied() {
            return IsOccupied;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public String getRoomId() {
            return RoomId;
        }

        public String getRoomType() {
            return RoomType;
        }

        public String getRoomTypeId() {
            return RoomTypeId;
        }

        public String getBranchName() {
            return BranchName;
        }

        public String getBranchId() {
            return BranchId;
        }
    }
}
