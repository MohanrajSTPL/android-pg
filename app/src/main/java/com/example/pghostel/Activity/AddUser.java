package com.example.pghostel.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.EmployeeListResponse;
import com.example.pghostel.ModelClass.UserTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.searchablespinnerclass.SearchableSpinner;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUser extends AppCompatActivity {

    private SearchableSpinner employee;
    private MaterialBetterSpinner userType;
    private EditText password;
    private Button addUser;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private UserTypeResponse userTypeResponse;
    private EmployeeListResponse employeeListResponse;
    private int empID,userTypeID;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        employee = (SearchableSpinner) findViewById(R.id.material_spinner1);
        userType = (MaterialBetterSpinner) findViewById(R.id.material_spinner2);
        password = (EditText) findViewById(R.id.password);
        addUser = (Button) findViewById(R.id.adduser);


        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(AddUser.this, "JWT"));

        pDialog = ProgressDialog.show(AddUser.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

        getUserTypeList(requestBody);


        employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
               // String mSelectedText = adapterView.getItemAtPosition(position).toString();
                empID = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        userType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // String mSelectedText = adapterView.getItemAtPosition(position).toString();
                userTypeID = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Map<String, Object> requestBody = new HashMap<>();
                Map AppUserDetails = new LinkedHashMap(3);
                AppUserDetails.put("EmployeeId", employeeListResponse.getResponse().get(empID).getEmployeeId());
                AppUserDetails.put("UserTypeId", userTypeResponse.getResponse().get(userTypeID).getUserTypeId());
                AppUserDetails.put("Password", password.getText().toString().trim());
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(AddUser.this, "JWT"));
                requestBody.put("AppUser", AppUserDetails);

                pDialog = ProgressDialog.show(AddUser.this, Html.fromHtml("<b><font color='#ff8f61'>Adding User..</font></b>"), "Please wait ...");
                AddAppUser(requestBody);
            }
        });
    }
    private void AddAppUser(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAppUser(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    AddBranchResponse addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(AddUser.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(AddUser.this, "App User has been added successfully..!!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddUser.this, addBranchResponse.getError(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(AddUser.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(AddUser.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserTypeList(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getUserTypeList(requestBody).enqueue(new Callback<UserTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<UserTypeResponse> call, Response<UserTypeResponse> response) {
                try {

                    userTypeResponse = response.body();
                    if (userTypeResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(AddUser.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (userTypeResponse.getResponse().isEmpty()) {
                        pDialog.dismiss();
                        Toast.makeText(AddUser.this, "No User Type Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<String> usertypelist = new ArrayList<String>();
                    if (userTypeResponse != null) {
                        for (UserTypeResponse.Response list : userTypeResponse.getResponse()) {
                            usertypelist.add(list.getUserType());
                        }
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddUser.this, android.R.layout.simple_dropdown_item_1line, usertypelist);

                    userType.setAdapter(adapter);

                    Map<String, Object> requestBody = new HashMap<>();

                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(AddUser.this, "JWT"));

                    getListEmployee(requestBody);

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(AddUser.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserTypeResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(AddUser.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getListEmployee(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getEmployeeList(requestBody).enqueue(new Callback<EmployeeListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<EmployeeListResponse> call, Response<EmployeeListResponse> response) {
                try {
                    pDialog.dismiss();
                    employeeListResponse = new EmployeeListResponse();
                    employeeListResponse = response.body();

                    if (employeeListResponse == null) {

                        Toast.makeText(AddUser.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (employeeListResponse.getMessage().trim().equals("Success")) {

                        if (employeeListResponse.getResponse().isEmpty()) {

                            Toast.makeText(AddUser.this, "Employee list is empty..!!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        List<String> employeelist = new ArrayList<String>();
                        if (employeeListResponse != null) {
                            for (EmployeeListResponse.Response list : employeeListResponse.getResponse()) {
                                employeelist.add(list.getEmployeeName()+" - EmpNo: "+list.getEmployeeNo());
                            }
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddUser.this, android.R.layout.simple_dropdown_item_1line, employeelist);

                        employee.setAdapter(adapter);

                    } else {

                        Toast.makeText(AddUser.this, employeeListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(AddUser.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EmployeeListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(AddUser.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appuserlist, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.appuserlist) {
            Intent i = new Intent(AddUser.this, AppUserList.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
