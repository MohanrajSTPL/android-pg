package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VASEnrollmentListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("BillPlan")
        private String BillPlan;
        @Expose
        @SerializedName("VASEnrollmentId")
        private String VASEnrollmentId;
        @Expose
        @SerializedName("VAS")
        private String VAS;
        @Expose
        @SerializedName("VASId")
        private String VASId;
        @Expose
        @SerializedName("TenantName")
        private String TenantName;
        @Expose
        @SerializedName("TenantNo")
        private String TenantNo;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;

        public String getStartDate() {
            return StartDate;
        }

        public String getBillPlan() {
            return BillPlan;
        }

        public String getVASEnrollmentId() {
            return VASEnrollmentId;
        }

        public String getVAS() {
            return VAS;
        }

        public String getVASId() {
            return VASId;
        }

        public String getTenantName() {
            return TenantName;
        }

        public String getTenantNo() {
            return TenantNo;
        }

        public String getTenantId() {
            return TenantId;
        }
    }
}
