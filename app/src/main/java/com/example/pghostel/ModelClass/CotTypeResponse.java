package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CotTypeResponse {

    @Expose
    @SerializedName("Response")
    private List<ResponseEntity> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<ResponseEntity> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class ResponseEntity {
        @Expose
        @SerializedName("CotTypeActiveInd")
        private String CotTypeActiveInd;
        @Expose
        @SerializedName("CotTypeDesc")
        private String CotTypeDesc;
        @Expose
        @SerializedName("CotType")
        private String CotType;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("CotTypeId")
        private String CotTypeId;

        public String getCotTypeActiveInd() {
            return CotTypeActiveInd;
        }

        public String getCotTypeDesc() {
            return CotTypeDesc;
        }

        public String getCotType() {
            return CotType;
        }

        public String getBranchId() {
            return BranchId;
        }

        public String getCotTypeId() {
            return CotTypeId;
        }
    }
}
