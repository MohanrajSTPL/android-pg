package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdhoServiceListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("AdhocServiceActiveInd")
        private String AdhocServiceActiveInd;
        @Expose
        @SerializedName("CostPerUse")
        private String CostPerUse;
        @Expose
        @SerializedName("AdhocServiceDesc")
        private String AdhocServiceDesc;
        @Expose
        @SerializedName("AdhocService")
        private String AdhocService;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("AdhocServiceId")
        private String AdhocServiceId;

        public String getAdhocServiceActiveInd() {
            return AdhocServiceActiveInd;
        }

        public String getCostPerUse() {
            return CostPerUse;
        }

        public String getAdhocServiceDesc() {
            return AdhocServiceDesc;
        }

        public String getAdhocService() {
            return AdhocService;
        }

        public String getBranchId() {
            return BranchId;
        }

        public String getAdhocServiceId() {
            return AdhocServiceId;
        }
    }
}
