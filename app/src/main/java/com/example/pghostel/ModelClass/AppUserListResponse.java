package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppUserListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("BranchName")
        private String BranchName;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("EmployeeNo")
        private String EmployeeNo;
        @Expose
        @SerializedName("EmployeeName")
        private String EmployeeName;
        @Expose
        @SerializedName("UserType")
        private String UserType;
        @Expose
        @SerializedName("UserTypeId")
        private String UserTypeId;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("AppUserId")
        private String AppUserId;

        public String getBranchName() {
            return BranchName;
        }

        public String getBranchId() {
            return BranchId;
        }

        public String getEmployeeNo() {
            return EmployeeNo;
        }

        public String getEmployeeName() {
            return EmployeeName;
        }

        public String getUserType() {
            return UserType;
        }

        public String getUserTypeId() {
            return UserTypeId;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public String getEmailId() {
            return EmailId;
        }

        public String getAppUserId() {
            return AppUserId;
        }
    }
}
