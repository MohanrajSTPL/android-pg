package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.CotList;
import com.example.pghostel.Activity.CotTypesList;
import com.example.pghostel.Activity.PhysicalRoomList;
import com.example.pghostel.Activity.RoomFacility;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.PhysicalRoomListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhysicalRoomListAdapter extends RecyclerView.Adapter<PhysicalRoomListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AlertDialog alertDialog;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<PhysicalRoomListResponse.ResponseEntity> responses;
    private AddBranchResponse addBranchResponse;

    //getting the context and product list with constructor
    public PhysicalRoomListAdapter(Context mCtx, List<PhysicalRoomListResponse.ResponseEntity> rommfacilitylist) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = rommfacilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final PhysicalRoomListResponse.ResponseEntity roomfacilitylist = responses.get(position);

        //binding the data with the viewholder views
        holder.facilityname.setText("Room No: "+roomfacilitylist.getRoomNo());
        holder.facilityPrice.setText("Block: "+roomfacilitylist.getBlock() + " & Floor: " + roomfacilitylist.getFloor());
        holder.facilityNotes.setText("RoomType: "+roomfacilitylist.getRoomType());


        if (roomfacilitylist.getRoomActiveInd().equals("N")) {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.facilityStatus.setText("IN ACTIVE");
        } else {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.facilityStatus.setText("ACTIVE");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //showViewDialog(responses.get(position));
                Intent i=new Intent(mCtx, CotList.class);
                i.putExtra("ID",Integer.parseInt(responses.get(position).getRoomId()));
                mCtx.startActivity(i);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if (!responses.get(position).getRoomActiveInd().equals("N")) {
                    AlertDialog diaBox = Option(responses.get(position));
                    //AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;


        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
            // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showViewDialog(final PhysicalRoomListResponse.ResponseEntity branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton roomsdetails = (RadioButton) dialogView.findViewById(R.id.roomsdetails);
        final RadioButton facilitydetails = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton roomdetails = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton room = (RadioButton) dialogView.findViewById(R.id.room);

        roomsdetails.setVisibility(View.GONE);
        roomdetails.setText("Room Details");
        facilitydetails.setText("Cot List");
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if(selectedId==-1){
                    Toast.makeText(mCtx,"Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(radioButton.getText().toString().equals("Room Details")){
                    showCustomDialog(branchlist);
                }else if(radioButton.getText().toString().equals("Cot List")){
                    Intent i=new Intent(mCtx, CotList.class);
                    i.putExtra("ID",Integer.parseInt(branchlist.getRoomId()));
                    mCtx.startActivity(i);
                }

                alertDialog.dismiss();

            }
        });
    }
    private AlertDialog Option(final PhysicalRoomListResponse.ResponseEntity branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Alert")
                .setMessage("Are you going to do?")
                //.setMessage("Are you sure want to Delete this Room Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();

                        Map RoomDetails = new LinkedHashMap(1);
                        RoomDetails.put("RoomId", branchlist.getRoomId());
                        requestBody.put("Operator", "D");
                        requestBody.put("BranchId", branchlist.getBranchId());
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("Rooms", RoomDetails);

                        DeleteRoom(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {
                        showCustomDialog(branchlist);
                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    private AlertDialog AskOption(final PhysicalRoomListResponse.ResponseEntity branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Room?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        Map<String, Object> requestBody = new HashMap<>();

                        Map RoomDetails = new LinkedHashMap(1);
                        RoomDetails.put("RoomId", branchlist.getRoomId());
                        requestBody.put("Operator", "D");
                        requestBody.put("BranchId", branchlist.getBranchId());
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("Rooms", RoomDetails);

                        DeleteRoom(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final PhysicalRoomListResponse.ResponseEntity branchlist) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //viewGroup = root.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addroom, viewGroup, false);


        Button add = (Button) dialogView.findViewById(R.id.buttonadd);
        final EditText name = (EditText) dialogView.findViewById(R.id.name);
        final EditText roomno = (EditText) dialogView.findViewById(R.id.roomno);
        final EditText floor = (EditText) dialogView.findViewById(R.id.floor);
        final EditText block = (EditText) dialogView.findViewById(R.id.block);
        final EditText area = (EditText) dialogView.findViewById(R.id.area);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        add.setText("Update");
        roomno.setText(branchlist.getRoomNo());
        floor.setText(branchlist.getFloor());
        block.setText(branchlist.getBlock());
        area.setText(branchlist.getArea());


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, Object> requestBody = new HashMap<>();

                Map RoomDetails = new LinkedHashMap(7);
                RoomDetails.put("RoomId", branchlist.getRoomId());
                RoomDetails.put("RoomTypeId", branchlist.getRoomTypeId());
                RoomDetails.put("RoomNo", roomno.getText().toString().trim());
                RoomDetails.put("Floor", floor.getText().toString().trim());
                RoomDetails.put("Block", block.getText().toString().trim());
                RoomDetails.put("Area", area.getText().toString().trim());
                RoomDetails.put("RoomActiveInd", "Y");
                requestBody.put("Operator", "U");
                requestBody.put("BranchId", branchlist.getBranchId());
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("Rooms", RoomDetails);

                UpdateRoom(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
            }
        });
    }

    private void DeleteRoom(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddPhysicalRoom(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, "Room has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((PhysicalRoomList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void UpdateRoom(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddPhysicalRoom(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    alertDialog.dismiss();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                        ((PhysicalRoomList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
