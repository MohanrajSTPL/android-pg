package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.FacilityListResponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.Branchlistadapter;
import com.example.pghostel.adapter.FacilityListAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Facility extends AppCompatActivity {


    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private FacilityListResponse facilityListResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private int B_id;
    FacilityListAdapter facilityListAdapter;
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewfacility);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(Facility.this, 1));

        B_id = getIntent().getIntExtra("ID", 0);

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("JWT", manager.getSharedPreferencesValues(Facility.this, "JWT"));

        getBranchListDetails(requestBody);

    }
    public void onClickCalled() {
        // Call another acitivty here and pass some arguments to it.
        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("JWT", manager.getSharedPreferencesValues(Facility.this, "JWT"));

        getBranchListDetails(requestBody);
    }
    public void getBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getFacilityListDetails(requestBody).enqueue(new Callback<FacilityListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<FacilityListResponse> call, Response<FacilityListResponse> response) {
                try {
                    facilityListResponse = response.body();
                    if (facilityListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(Facility.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (facilityListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (facilityListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                            return;
                        }
                        facilityListAdapter = new FacilityListAdapter(Facility.this, facilityListResponse.getResponse());
                        recyclerView.setAdapter(facilityListAdapter);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(Facility.this, facilityListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(Facility.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FacilityListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(Facility.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
