package com.example.pghostel.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.pghostel.ModelClass.EmployeeListResponse;
import com.example.pghostel.R;
import androidx.annotation.RequiresApi;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeCreateActivity extends AppCompatActivity {

    EditText Branchname;
    EditText picker;
    EditText Address,officeAddress;
    EditText City,officeCity;
    EditText State;
    EditText Country;
    EditText Pincode;
    EditText MobileNo;
    EditText profession;
    EditText officeState;
    EditText officeCountry;
    EditText officePincode;
    EditText homephoneno;
    EditText officeMobileNo;
    EditText EmailID;
    RadioButton male,female;
    RadioGroup gender;
    Button update;
    private SessionManager manager;
    private AddBranchResponse addBranchResponse;
    int status,BranchID;
    String T_id;
    private EmployeeListResponse employeeListResponse;
    private ProgressDialog pDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_create);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        Branchname = (EditText)findViewById(R.id.branch_name);
        picker = (EditText) findViewById(R.id.dob);
        Address = (EditText)findViewById(R.id.address);
        City = (EditText)findViewById(R.id.city);
        State = (EditText)findViewById(R.id.state);
        Country = (EditText)findViewById(R.id.country);
        Pincode = (EditText)findViewById(R.id.pincode);
        MobileNo = (EditText)findViewById(R.id.mobileno);
        profession = (EditText)findViewById(R.id.profession);
        officeAddress = (EditText)findViewById(R.id.officeaddress);
        officeCity = (EditText)findViewById(R.id.officecity);
        officeState = (EditText)findViewById(R.id.officestate);
        officeCountry = (EditText)findViewById(R.id.officecountry);
        officePincode = (EditText)findViewById(R.id.officepincode);
        officeMobileNo = (EditText)findViewById(R.id.phoneno);
        homephoneno = (EditText)findViewById(R.id.homephoneno);
        EmailID = (EditText)findViewById(R.id.emailID);
        male = (RadioButton)findViewById(R.id.radio_gender_male);
        female = (RadioButton)findViewById(R.id.radio_gender_female);
        gender = (RadioGroup)findViewById(R.id.radio_group_gender);
        update = (Button)findViewById(R.id.updatebranchdetails);


        status = getIntent().getIntExtra("status", 0);
        BranchID = getIntent().getIntExtra("BranchID", 0);
        T_id = getIntent().getStringExtra("ID");

        if(status==0) {
            update.setText("Add");
        }
        else {
            update.setText("Update");

            Map<String, Object> requestBody = new HashMap<>();
            final Map EmpDetails = new LinkedHashMap(1);
            //TenantDetail.put("TenantMobileNo", "9630852740");
            EmpDetails.put("BranchId", BranchID);
            requestBody.put("Operator", "R");
            requestBody.put("JWT", manager.getSharedPreferencesValues(EmployeeCreateActivity.this, "JWT"));
            requestBody.put("EmployeeDetails", EmpDetails);
            getEmpDetails(requestBody);
            pDialog = ProgressDialog.show(EmployeeCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

        }


        picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EmployeeCreateActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String dayConverted = ""+dayOfMonth;
                                if(dayOfMonth<10){
                                    dayConverted = "0"+dayConverted;
                                }

                                String monthConverted = ""+(monthOfYear + 1);
                                if((monthOfYear + 1)<10){
                                    monthConverted = "0"+monthConverted;
                                }

                                picker.setText(year + "-" + monthConverted + "-" + dayConverted);
                                //picker.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = gender.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if(selectedId==-1){
                    Toast.makeText(EmployeeCreateActivity.this,"Plese select the Gender.!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String radio_value = "";
                if(radioButton.getText().toString().equals("Men"))
                    radio_value = "MALE";
                else
                    radio_value = "FEMALE";

                Map<String, Object> requestBody = new HashMap<>();
                Map EmpDetails = new LinkedHashMap(21);
//                if(status==1) {
//                    EmpDetails.put("BranchId", String.valueOf(B_id));
//                }
                EmpDetails.put("BranchId", BranchID);
                EmpDetails.put("EmployeeName", Branchname.getText().toString().trim());
                EmpDetails.put("EmployeeGender", radio_value);
                EmpDetails.put("EmployeeDOB", picker.getText().toString().trim());
                EmpDetails.put("EmployeeProfession", profession.getText().toString().trim());
                EmpDetails.put("EmployeeCurrentAddress", Address.getText().toString());
                EmpDetails.put("EmployeeCurrentCity", City.getText().toString());
                EmpDetails.put("EmployeeCurrentState", State.getText().toString());
                EmpDetails.put("EmployeeCurrentCountry", Country.getText().toString());
                EmpDetails.put("EmployeeCurrentPincode", Pincode.getText().toString());
                EmpDetails.put("EmployeeEmailId", EmailID.getText().toString());
                EmpDetails.put("EmployeeMobileNo", MobileNo.getText().toString());
                EmpDetails.put("EmployeeCurrentPhone", homephoneno.getText().toString());
                EmpDetails.put("EmployeePermanentAddress", officeAddress.getText().toString());
                EmpDetails.put("EmployeePermanentCity", officeCity.getText().toString());
                EmpDetails.put("EmployeePermanentState", officeState.getText().toString());
                EmpDetails.put("EmployeePermanentCountry", officeCountry.getText().toString());
                EmpDetails.put("EmployeePermanentPincode", officePincode.getText().toString());
                EmpDetails.put("EmployeePermanentPhone", officeMobileNo.getText().toString());
                EmpDetails.put("EmployeeActiveInd", "Y");
                if(status==1) {
                    requestBody.put("Operator", "U");
                    EmpDetails.put("EmployeeId", T_id);
                }else {
                    requestBody.put("Operator", "C");
                }

                requestBody.put("JWT", manager.getSharedPreferencesValues(EmployeeCreateActivity.this, "JWT"));
                requestBody.put("EmployeeDetails", EmpDetails);

                if(status==1) {
                    pDialog = ProgressDialog.show(EmployeeCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
                    updateEmpDetails(requestBody,"Updated");
                }else {
                    pDialog = ProgressDialog.show(EmployeeCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
                    updateEmpDetails(requestBody,"Added");
                }

            }
        });
    }

    private void getEmpDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getEmployeeList(requestBody).enqueue(new Callback<EmployeeListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<EmployeeListResponse> call, Response<EmployeeListResponse> response) {
                try {
                    // if (response.code() == 201) {
                    pDialog.dismiss();
                    employeeListResponse = new EmployeeListResponse();
                    employeeListResponse = response.body();

                    if (employeeListResponse == null) {
                        Toast.makeText(EmployeeCreateActivity.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (employeeListResponse.getMessage().trim().equals("Success")) {

                        for (EmployeeListResponse.Response list : employeeListResponse.getResponse()) {
                            Branchname.setText(list.getEmployeeName());
                            City.setText(list.getEmployeeCurrentCity());
                            picker.setText(list.getEmployeeDOB());
                            Address.setText(list.getEmployeeCurrentAddress());
                            State.setText(list.getEmployeeCurrentState());
                            Country.setText(list.getEmployeeCurrentCountry());
                            Pincode.setText(list.getEmployeeCurrentPincode());
                            MobileNo.setText(list.getEmployeeMobileNo());
                            profession.setText(list.getEmployeeProfession());
                            EmailID.setText(list.getEmployeeEmailId());
                            homephoneno.setText(list.getEmployeeCurrentPhone());
                            officeCity.setText(list.getEmployeePermanentCity());
                            officeAddress.setText(list.getEmployeePermanentAddress());
                            officeState.setText(list.getEmployeePermanentState());
                            officeCountry.setText(list.getEmployeePermanentCountry());
                            officePincode.setText(list.getEmployeePermanentPincode());
                            officeMobileNo.setText(list.getEmployeePermanentPhone());
                            if(list.getEmployeeGender().equals("MALE")){
                                male.setChecked(true);
                            }else {
                                female.setChecked(true);
                            }
                        }
                        pDialog.dismiss();
                    } else {
                        Toast.makeText(EmployeeCreateActivity.this, employeeListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(EmployeeCreateActivity.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EmployeeListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(EmployeeCreateActivity.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateEmpDetails(Map<String, Object> requestBody, final String s) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddEmployee(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(EmployeeCreateActivity.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(EmployeeCreateActivity.this, "Employee has been "+s+" successfully..!!", Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(EmployeeCreateActivity.this, EmployeeList.class);
                        i.putExtra("ID",BranchID);
                        startActivity(i);
                    } else {
                        if(addBranchResponse.getError()!=null && !addBranchResponse.getError().equals("")) {
                            Toast.makeText(EmployeeCreateActivity.this, addBranchResponse.getError(), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(EmployeeCreateActivity.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(EmployeeCreateActivity.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(EmployeeCreateActivity.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

