package com.example.pghostel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


/*
        {
            "Operator": "R",
            "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3d3cuc21hcnRpZmljaWEuY29tIiwiYXVkIjoicGd1c2VycyIsImlhdCI6MTU4MTU1MjgxMCwibmJmIjoxNTgxNTUyODEwLCJkYXRhIjp7IkFwcFVzZXJJZCI6IjM5IiwiVXNlclR5cGVJZCI6IjIiLCJVc2VyVHlwZSI6IldBUkRFTiIsIkFjY291bnRJZCI6IjEiLCJCcmFuY2hJZCI6IjEiLCJTdWJzY3JpcHRpb25FbmREdCI6IjIwMjAtMDUtMDUgMjE6NDg6NDUifX0.edhxw_w6lQ2Fu8Nq7hzB7Ee1GSPruMzp5-N3RQlojHY",
            "VacancyReport": {
        }
        }


        {
            "ResponseCode": 1,
            "Message": "Success",
            "Response": [
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "3"
            }
            ]
        }

        {
            "Operator": "R",
            "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3d3cuc21hcnRpZmljaWEuY29tIiwiYXVkIjoicGd1c2VycyIsImlhdCI6MTU4MTU1MjgxMCwibmJmIjoxNTgxNTUyODEwLCJkYXRhIjp7IkFwcFVzZXJJZCI6IjM5IiwiVXNlclR5cGVJZCI6IjIiLCJVc2VyVHlwZSI6IldBUkRFTiIsIkFjY291bnRJZCI6IjEiLCJCcmFuY2hJZCI6IjEiLCJTdWJzY3JpcHRpb25FbmREdCI6IjIwMjAtMDUtMDUgMjE6NDg6NDUifX0.edhxw_w6lQ2Fu8Nq7hzB7Ee1GSPruMzp5-N3RQlojHY",
            "VacancyReport": {
            "BranchId": "1"
        }
        }

        {
            "ResponseCode": 1,
            "Message": "Success",
            "Response": [
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "2"
            },
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "2",
                "RoomType": "Tripple Share Delux AC",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "1"
            }
            ]
        }

        {
            "Operator": "R",
            "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3d3cuc21hcnRpZmljaWEuY29tIiwiYXVkIjoicGd1c2VycyIsImlhdCI6MTU4MTU1MjgxMCwibmJmIjoxNTgxNTUyODEwLCJkYXRhIjp7IkFwcFVzZXJJZCI6IjM5IiwiVXNlclR5cGVJZCI6IjIiLCJVc2VyVHlwZSI6IldBUkRFTiIsIkFjY291bnRJZCI6IjEiLCJCcmFuY2hJZCI6IjEiLCJTdWJzY3JpcHRpb25FbmREdCI6IjIwMjAtMDUtMDUgMjE6NDg6NDUifX0.edhxw_w6lQ2Fu8Nq7hzB7Ee1GSPruMzp5-N3RQlojHY",
            "VacancyReport": {
            "BranchId": "1",
            "RoomTypeId": "1"
        }
        }

        {
            "ResponseCode": 1,
            "Message": "Success",
            "Response": [
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "RoomId": "5",
                "RoomNo": "102",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "2"
            }
            ]
        }


        {
            "Operator": "R",
            "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3d3cuc21hcnRpZmljaWEuY29tIiwiYXVkIjoicGd1c2VycyIsImlhdCI6MTU4MTU1MjgxMCwibmJmIjoxNTgxNTUyODEwLCJkYXRhIjp7IkFwcFVzZXJJZCI6IjM5IiwiVXNlclR5cGVJZCI6IjIiLCJVc2VyVHlwZSI6IldBUkRFTiIsIkFjY291bnRJZCI6IjEiLCJCcmFuY2hJZCI6IjEiLCJTdWJzY3JpcHRpb25FbmREdCI6IjIwMjAtMDUtMDUgMjE6NDg6NDUifX0.edhxw_w6lQ2Fu8Nq7hzB7Ee1GSPruMzp5-N3RQlojHY",
            "VacancyReport": {
            "BranchId": "1",
            "RoomTypeId": "1",
            "RoomId": "5"
        }
        }

        {
            "ResponseCode": 1,
            "Message": "Success",
            "Response": [
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "RoomId": "5",
                "RoomNo": "102",
                "CotId": "6",
                "CotNo": "1",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "1"
            },
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "RoomId": "5",
                "RoomNo": "102",
                "CotId": "7",
                "CotNo": "2",
                "IsOccupied": "N",
                "NoOfCotsAvailable": "1"
            }
            ]
        }


        {
            "Operator": "R",
            "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3d3cuc21hcnRpZmljaWEuY29tIiwiYXVkIjoicGd1c2VycyIsImlhdCI6MTU4MTU1MjgxMCwibmJmIjoxNTgxNTUyODEwLCJkYXRhIjp7IkFwcFVzZXJJZCI6IjM5IiwiVXNlclR5cGVJZCI6IjIiLCJVc2VyVHlwZSI6IldBUkRFTiIsIkFjY291bnRJZCI6IjEiLCJCcmFuY2hJZCI6IjEiLCJTdWJzY3JpcHRpb25FbmREdCI6IjIwMjAtMDUtMDUgMjE6NDg6NDUifX0.edhxw_w6lQ2Fu8Nq7hzB7Ee1GSPruMzp5-N3RQlojHY",
            "VacancyReport": {
            "BranchId": "1",
            "RoomTypeId": "1",
            "RoomId": "5"
        }
        }

        {
            "ResponseCode": 1,
            "Message": "Success",
            "Response": [
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "RoomId": "5",
                "RoomNo": "102",
                "CotId": "6",
                "CotNo": "1",
                "IsOccupied": "N"
            },
            {
                "BranchId": "1",
                "BranchName": "Branch 1",
                "RoomTypeId": "1",
                "RoomType": "Double Share Delux AC",
                "RoomId": "5",
                "RoomNo": "102",
                "CotId": "7",
                "CotNo": "2",
                "IsOccupied": "N"
            }
            ]
        }*/
    }
}
