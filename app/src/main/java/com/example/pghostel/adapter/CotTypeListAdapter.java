package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.CotTypesList;
import com.example.pghostel.Activity.RoomFacility;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.CotTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CotTypeListAdapter extends RecyclerView.Adapter<CotTypeListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AlertDialog alertDialog;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<CotTypeResponse.ResponseEntity> responses;
    private AddBranchResponse addBranchResponse;

    //getting the context and product list with constructor
    public CotTypeListAdapter(Context mCtx, List<CotTypeResponse.ResponseEntity> rommfacilitylist) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = rommfacilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final CotTypeResponse.ResponseEntity roomfacilitylist = responses.get(position);

        //binding the data with the viewholder views
        holder.facilityname.setText(roomfacilitylist.getCotType());
        holder.facilityPrice.setText("");
        holder.facilityNotes.setText(roomfacilitylist.getCotTypeDesc());


        if (roomfacilitylist.getCotTypeActiveInd().equals("N")) {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.facilityStatus.setText("IN ACTIVE");
        } else {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.facilityStatus.setText("ACTIVE");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                showCustomDialog(responses.get(position));
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if(!responses.get(position).getCotTypeActiveInd().equals("N")) {
                    AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;


        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
            // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }

    private AlertDialog AskOption(final CotTypeResponse.ResponseEntity branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Cot Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        Map<String, Object> requestBody = new HashMap<>();

                        Map CotTypeDetails = new LinkedHashMap(1);
                        CotTypeDetails.put("CotTypeId", branchlist.getCotTypeId());
                        requestBody.put("Operator", "D");
                        requestBody.put("BranchId", branchlist.getBranchId());
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("CotTypes", CotTypeDetails);

                        DeleteCotType(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final CotTypeResponse.ResponseEntity branchlist) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //viewGroup = root.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addcottypepopup, viewGroup, false);


        Button add = (Button) dialogView.findViewById(R.id.buttonadd);
        final EditText name = (EditText) dialogView.findViewById(R.id.cottypename);
        final EditText desc = (EditText) dialogView.findViewById(R.id.cottypenote);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        add.setText("Update");
        name.setText(branchlist.getCotType());
        // cost.setText(branchlist.getCostPerMonth());
        desc.setText(branchlist.getCotTypeDesc());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, Object> requestBody = new HashMap<>();

                Map CotTypeDetails = new LinkedHashMap(4);
                CotTypeDetails.put("CotTypeId", branchlist.getCotTypeId());
                CotTypeDetails.put("CotType", name.getText().toString());
                CotTypeDetails.put("CotTypeDesc", desc.getText().toString());
                CotTypeDetails.put("CotTypeActiveInd", "Y");
                requestBody.put("Operator", "U");
                requestBody.put("BranchId", branchlist.getBranchId());
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("CotTypes", CotTypeDetails);

                UpdateCotType(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
            }
        });
    }

    private void DeleteCotType(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddCotType(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, "Cot Type has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((CotTypesList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void UpdateCotType( Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddCotType(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    alertDialog.dismiss();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                        ((CotTypesList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
