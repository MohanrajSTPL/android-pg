package com.example.pghostel.Activity;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.PaymentListResponse;
import com.example.pghostel.ModelClass.PaymentModeResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.CotTypeListAdapter;
import com.example.pghostel.adapter.PaymentListAdapter;
import com.example.pghostel.searchablespinnerclass.SearchableSpinner;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private PaymentListResponse paymentListResponse;
    private AddBranchResponse addBranchResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private Spinner paymentmode;
    private SearchableSpinner Tenantspin;
    private ProgressDialog pDialog;
    private PaymentModeResponse paymentModeResponsespinner;
    private TenantListResponse tenantListResponse;
    private int TenantID = 0;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(PaymentList.this, 1));



        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(PaymentList.this, 1));


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });
        gettenantList();


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(PaymentList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(PaymentList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(PaymentList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<PaymentListResponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (PaymentListResponse.Response list : paymentListResponse.getResponse()) {

//                        if (list.g().equals("Y")) {
//                            responses.add(list);
//                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (PaymentListResponse.Response list : paymentListResponse.getResponse()) {

//                        if (list.getCotTypeActiveInd().equals("N")) {
//                            responses.add(list);
//                        }

                    }
                } else {
                    responses = paymentListResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(PaymentList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                PaymentListAdapter b = new PaymentListAdapter(PaymentList.this, responses);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    private void PendingVeiw(){
        bar.setVisibility(View.VISIBLE);
        Map<String, Object> requestBody = new HashMap<>();
        Map CotTypeDetails = new LinkedHashMap(2);
        CotTypeDetails.put("TenantId", tenantListResponse.getResponse().get(TenantID).getTenantId());
        CotTypeDetails.put("Status", "Pending");
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(PaymentList.this, "JWT"));
        requestBody.put("PaymentTxn", CotTypeDetails);
        getPaymentList(requestBody);
    }
    public void Refereshview() {
        bar.setVisibility(View.VISIBLE);
        Map<String, Object> requestBody = new HashMap<>();
        Map CotTypeDetails = new LinkedHashMap(1);
        CotTypeDetails.put("TenantId", tenantListResponse.getResponse().get(TenantID).getTenantId());
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(PaymentList.this, "JWT"));
        requestBody.put("PaymentTxn", CotTypeDetails);
        getPaymentList(requestBody);
    }



    private void getPaymentList(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getPaymentList(requestBody).enqueue(new Callback<PaymentListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<PaymentListResponse> call, Response<PaymentListResponse> response) {
                try {
                    norecordsLL.setVisibility(View.GONE);
                    paymentListResponse = new PaymentListResponse();
                    paymentListResponse = response.body();
                    if (paymentListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(PaymentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (paymentListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (paymentListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        PaymentListAdapter b = new PaymentListAdapter(PaymentList.this, paymentListResponse.getResponse());
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(PaymentList.this, paymentListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(PaymentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(PaymentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(PaymentList.this)).inflate(R.layout.addpaymentpopup, viewGroup, false);


        paymentmode = (Spinner) dialogView.findViewById(R.id.Paymentmode);
        final EditText amount = (EditText) dialogView.findViewById(R.id.amount);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(PaymentList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();

        getPayModeList();
        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (amount.length() == 0) {
                    Toast.makeText(PaymentList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Map<String, Object> requestBody = new HashMap<>();
                Map CotTypeDetails = new LinkedHashMap(3);
                CotTypeDetails.put("PaymentTxnType", "BILL_PAY");
                CotTypeDetails.put("Amount", amount.getText().toString().trim());
                CotTypeDetails.put("PaymentMode", paymentModeResponsespinner.getResponse().get(paymentmode.getSelectedItemPosition()).getPaymentModeId());
                CotTypeDetails.put("TenantId", tenantListResponse.getResponse().get(TenantID).getTenantId());

                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(PaymentList.this, "JWT"));
                requestBody.put("PaymentTxn", CotTypeDetails);
                bar.setVisibility(View.VISIBLE);
                AddPayment(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(PaymentList.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }
    private void getPayModeList() {
        pDialog = ProgressDialog.show(PaymentList.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Payment Mode..</font></b>"), "Please wait ...");

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(PaymentList.this, "JWT"));

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getPaymentModeList(requestBody).enqueue(new Callback<PaymentModeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<PaymentModeResponse> call, Response<PaymentModeResponse> response) {
                try {
                    pDialog.dismiss();
                    paymentModeResponsespinner = response.body();
                    if (paymentModeResponsespinner == null) {
                        pDialog.dismiss();
                        Toast.makeText(PaymentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (paymentModeResponsespinner.getResponse().isEmpty()) {
                        pDialog.dismiss();
                        Toast.makeText(PaymentList.this, "No Payment Mode Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<String> usertypelist = new ArrayList<String>();
                    if (paymentModeResponsespinner != null) {
                        for (PaymentModeResponse.Response list : paymentModeResponsespinner.getResponse()) {
                            usertypelist.add(list.getPaymentMode());
                        }
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaymentList.this, android.R.layout.simple_dropdown_item_1line, usertypelist);

                    paymentmode.setAdapter(adapter);


                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(PaymentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentModeResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(PaymentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void AddPayment(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddPayment(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(PaymentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(PaymentList.this, "Payment has been initiated successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();
                    } else {
                        Toast.makeText(PaymentList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(PaymentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(PaymentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {

    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard, menu);
        MenuItem item = menu.findItem(R.id.roomtypefacility);
        item.setTitle("Select Tenant");
        MenuItem item1 = menu.findItem(R.id.cottype);
        item1.setTitle("Pending");
        return true;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.roomtypefacility:
                showTenantpopup();
                return true;
            case R.id.cottype:
                PendingVeiw();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showTenantpopup(){
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(PaymentList.this)).inflate(R.layout.tenantpopup, viewGroup, false);


        Tenantspin = (SearchableSpinner) dialogView.findViewById(R.id.tenantSpin);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(PaymentList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        int pos = 0;
        int intex = 0;
        List<String> usertypelist = new ArrayList<String>();
        if (tenantListResponse != null) {
            for (TenantListResponse.Response list : tenantListResponse.getResponse()) {
                usertypelist.add(list.getTenantName()+" - TenantNo: "+list.getTenantNo());
                if(list.getTenantId().equals(tenantListResponse.getResponse().get(TenantID).getTenantId())){
                    pos=intex;
                }
                intex ++;
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaymentList.this, android.R.layout.simple_dropdown_item_1line, usertypelist);
        Tenantspin.setAdapter(adapter);

       // if(TenantID!=0){
            Tenantspin.setSelection(pos);
        //}
        Tenantspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // String mSelectedText = adapterView.getItemAtPosition(position).toString();
                TenantID = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Refereshview();
                alertDialog.dismiss();
            }
        });
    }

    private void gettenantList() {
        pDialog = ProgressDialog.show(PaymentList.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Tenant List..</font></b>"), "Please wait ...");

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(PaymentList.this, "JWT"));

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListTenant(requestBody).enqueue(new Callback<TenantListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<TenantListResponse> call, Response<TenantListResponse> response) {
                try {
                    pDialog.dismiss();
                    tenantListResponse = response.body();
                    if (tenantListResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(PaymentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (tenantListResponse.getResponse().isEmpty()) {
                        pDialog.dismiss();
                        Toast.makeText(PaymentList.this, "No Payment Mode Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    showTenantpopup();

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(PaymentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TenantListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(PaymentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
