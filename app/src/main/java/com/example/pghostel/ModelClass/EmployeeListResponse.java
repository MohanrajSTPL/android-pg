package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeeListResponse {


    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("EmployeeActiveInd")
        private String EmployeeActiveInd;
        @Expose
        @SerializedName("EmployeePermanentPhone")
        private String EmployeePermanentPhone;
        @Expose
        @SerializedName("EmployeePermanentPincode")
        private String EmployeePermanentPincode;
        @Expose
        @SerializedName("EmployeePermanentCountry")
        private String EmployeePermanentCountry;
        @Expose
        @SerializedName("EmployeePermanentState")
        private String EmployeePermanentState;
        @Expose
        @SerializedName("EmployeePermanentCity")
        private String EmployeePermanentCity;
        @Expose
        @SerializedName("EmployeePermanentAddress")
        private String EmployeePermanentAddress;
        @Expose
        @SerializedName("EmployeeCurrentPhone")
        private String EmployeeCurrentPhone;
        @Expose
        @SerializedName("EmployeeCurrentPincode")
        private String EmployeeCurrentPincode;
        @Expose
        @SerializedName("EmployeeCurrentCountry")
        private String EmployeeCurrentCountry;
        @Expose
        @SerializedName("EmployeeCurrentState")
        private String EmployeeCurrentState;
        @Expose
        @SerializedName("EmployeeCurrentCity")
        private String EmployeeCurrentCity;
        @Expose
        @SerializedName("EmployeeCurrentAddress")
        private String EmployeeCurrentAddress;
        @Expose
        @SerializedName("EmployeeProfession")
        private String EmployeeProfession;
        @Expose
        @SerializedName("EmployeeGender")
        private String EmployeeGender;
        @Expose
        @SerializedName("EmployeeDOB")
        private String EmployeeDOB;
        @Expose
        @SerializedName("EmployeeEmailId")
        private String EmployeeEmailId;
        @Expose
        @SerializedName("EmployeeMobileNo")
        private String EmployeeMobileNo;
        @Expose
        @SerializedName("EmployeeName")
        private String EmployeeName;
        @Expose
        @SerializedName("EmployeeNo")
        private String EmployeeNo;
        @Expose
        @SerializedName("EmployeeId")
        private String EmployeeId;
        @Expose
        @SerializedName("BranchName")
        private String BranchName;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;

        public String getEmployeeActiveInd() {
            return EmployeeActiveInd;
        }

        public String getEmployeePermanentPhone() {
            return EmployeePermanentPhone;
        }

        public String getEmployeePermanentPincode() {
            return EmployeePermanentPincode;
        }

        public String getEmployeePermanentCountry() {
            return EmployeePermanentCountry;
        }

        public String getEmployeePermanentState() {
            return EmployeePermanentState;
        }

        public String getEmployeePermanentCity() {
            return EmployeePermanentCity;
        }

        public String getEmployeePermanentAddress() {
            return EmployeePermanentAddress;
        }

        public String getEmployeeCurrentPhone() {
            return EmployeeCurrentPhone;
        }

        public String getEmployeeCurrentPincode() {
            return EmployeeCurrentPincode;
        }

        public String getEmployeeCurrentCountry() {
            return EmployeeCurrentCountry;
        }

        public String getEmployeeCurrentState() {
            return EmployeeCurrentState;
        }

        public String getEmployeeCurrentCity() {
            return EmployeeCurrentCity;
        }

        public String getEmployeeCurrentAddress() {
            return EmployeeCurrentAddress;
        }

        public String getEmployeeProfession() {
            return EmployeeProfession;
        }

        public String getEmployeeGender() {
            return EmployeeGender;
        }

        public String getEmployeeDOB() {
            return EmployeeDOB;
        }

        public String getEmployeeEmailId() {
            return EmployeeEmailId;
        }

        public String getEmployeeMobileNo() {
            return EmployeeMobileNo;
        }

        public String getEmployeeName() {
            return EmployeeName;
        }

        public String getEmployeeNo() {
            return EmployeeNo;
        }

        public String getEmployeeId() {
            return EmployeeId;
        }

        public String getBranchName() {
            return BranchName;
        }

        public String getBranchId() {
            return BranchId;
        }
    }
}
