package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.BranchDetails;
import com.example.pghostel.Activity.Facility;
import com.example.pghostel.Activity.Rooms;
import com.example.pghostel.ModelClass.BranchListResponse;
import com.example.pghostel.ModelClass.FacilityListResponse;
import com.example.pghostel.ModelClass.RoomFacilityListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FacilityListAdapter extends RecyclerView.Adapter<FacilityListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AlertDialog alertDialog;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<FacilityListResponse.Response> responses;
    private FacilityListResponse facilityListResponse;

    //getting the context and product list with constructor
    public FacilityListAdapter(Context mCtx, List<FacilityListResponse.Response> facilitylist) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = facilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final FacilityListResponse.Response facilitylist = responses.get(position);

        //binding the data with the viewholder views
        holder.facilityname.setText(facilitylist.getFacilityName());
        holder.facilityPrice.setText(mCtx.getResources().getString(R.string.Rs)+" "+facilitylist.getCostPerMonth());
        holder.facilityNotes.setText(facilitylist.getFacilityDesc());


        if(facilitylist.getBranchFacilityActiveInd().equals("N")){
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.facilityStatus.setText("IN ACTIVE");
        }else {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.facilityStatus.setText("ACTIVE");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                showCustomDialog(responses.get(position));
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!responses.get(position).getBranchFacilityActiveInd().equals("N")) {
                    AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;



        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
           // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }

    private AlertDialog AskOption(final FacilityListResponse.Response branchlist)
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Branch Facility?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        JsonObject jo = new JsonObject();
                        JsonObject jsonObject = new JsonObject();

                        jsonObject.addProperty("FacilityId",branchlist.getFacilityId());
                        jsonObject.addProperty("CostPerMonth","0.00");
                        jsonObject.addProperty("Notes","");
                        jsonObject.addProperty("BranchFacilityActiveInd","N");

                        JsonArray j = new JsonArray();
                        j.add(jsonObject);

                        jo.addProperty("Operator", "CU");
                        jo.addProperty("BranchId", branchlist.getBranchId());
                        jo.addProperty("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        jo.add("Facilities",j);

                        DeleteFacilityListDetails(jo);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final FacilityListResponse.Response branchlist) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //viewGroup = root.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addfacility, viewGroup, false);


         Button add = (Button) dialogView.findViewById(R.id.buttonadd);
         TextView name = (TextView) dialogView.findViewById(R.id.fname);
         final TextView cost = (TextView) dialogView.findViewById(R.id.cost);
         final TextView note = (TextView) dialogView.findViewById(R.id.note);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        name.setText(branchlist.getFacilityName());
        cost.setText(branchlist.getCostPerMonth());
        note.setText(branchlist.getNotes());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JsonObject jo = new JsonObject();
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("FacilityId",branchlist.getFacilityId());
                jsonObject.addProperty("CostPerMonth",cost.getText().toString());
                jsonObject.addProperty("Notes",note.getText().toString());
                jsonObject.addProperty("BranchFacilityActiveInd","Y");

                JsonArray j = new JsonArray();
                j.add(jsonObject);

                jo.addProperty("Operator", "CU");
                jo.addProperty("BranchId", branchlist.getBranchId());
                jo.addProperty("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                jo.add("Facilities",j);

                alertDialog.dismiss();
                getFacilityListDetails(jo);

                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }
    private void DeleteFacilityListDetails(JsonObject requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddFacilityDetails(requestBody).enqueue(new Callback<FacilityListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<FacilityListResponse> call, Response<FacilityListResponse> response) {
                try {
                    pDialog.dismiss();
                    facilityListResponse = response.body();
                    if (facilityListResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (facilityListResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, "Facility has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((Facility) mCtx).onClickCalled();
                    } else {
                        Toast.makeText(mCtx, facilityListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FacilityListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getFacilityListDetails(JsonObject requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddFacilityDetails(requestBody).enqueue(new Callback<FacilityListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<FacilityListResponse> call, Response<FacilityListResponse> response) {
                try {
                    pDialog.dismiss();
                    facilityListResponse = response.body();
                    alertDialog.dismiss();
                    if (facilityListResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (facilityListResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, facilityListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                        ((Facility) mCtx).onClickCalled();
                    } else {
                        Toast.makeText(mCtx, facilityListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FacilityListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
