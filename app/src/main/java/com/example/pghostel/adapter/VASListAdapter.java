package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.PhysicalRoomList;
import com.example.pghostel.Activity.Rooms;
import com.example.pghostel.Activity.VASList;
import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.VASListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VASListAdapter extends RecyclerView.Adapter<VASListAdapter.BranchlistViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AdapterInterface buttonListener;
    //we are storing all the products in a list
    private List<VASListResponse.Response> responses;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private AddBranchResponse addBranchResponse;
    private AlertDialog alertDialog;

    public VASListAdapter(Context mCtx, List<VASListResponse.Response> roomlist, AdapterInterface fragment) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = roomlist;
        this.buttonListener = fragment;

    }

    @Override
    public VASListAdapter.BranchlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childbranchlist, null);
        return new VASListAdapter.BranchlistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VASListAdapter.BranchlistViewHolder holder, final int position) {
        //getting the product of the specified position
        final VASListResponse.Response roomlist = responses.get(position);

        //binding the data with the viewholder views
        holder.branchname.setText(roomlist.getVAS());
        holder.Servicetype.setText(roomlist.getVASDesc());
        holder.branchgender.setText(mCtx.getResources().getString(R.string.Rs)+" "+ roomlist.getCostPerDay()+ "/D & "+mCtx.getResources().getString(R.string.Rs)+" "+ roomlist.getCostPerMonth()+"/M");

        if (roomlist.getVASActiveInd().equals("N")) {
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.branchStatus.setText("IN ACTIVE");
        } else {
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.branchStatus.setText("ACTIVE");
        }
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onClick(View v) {
//                if (responses.get(position).getVASActiveInd().equals("Y")) {
//                    //showCustomPopup(responses.get(position));
//                    Intent i=new Intent(mCtx, PhysicalRoomList.class);
//                    i.putExtra("ID",Integer.parseInt(responses.get(position).getBranchId()));
//                    i.putExtra("RTID",Integer.parseInt(responses.get(position).getRoomTypeId()));
//                    mCtx.startActivity(i);
//                } else {
//                    AlertDialog diaBox = AskOption(responses.get(position));
//                    diaBox.show();
//                }
//            }
//        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!responses.get(position).getVASActiveInd().equals("N")) {
                    AlertDialog diaBox = Option(responses.get(position));
                    diaBox.show();
                }
                return true;
            }
        });


    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class BranchlistViewHolder extends RecyclerView.ViewHolder {

        TextView branchname;
        TextView Servicetype;
        TextView branchgender;
        TextView branchStatus;


        public BranchlistViewHolder(View itemView) {
            super(itemView);

            branchname = itemView.findViewById(R.id.branchname);
            Servicetype = itemView.findViewById(R.id.Servicetype);
            branchgender = itemView.findViewById(R.id.branchgender);
            branchStatus = itemView.findViewById(R.id.branchStatus);

        }
    }
    private AlertDialog Option(final VASListResponse.Response branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Alert")
                .setMessage("Are you going to do?")
                //.setMessage("Are you sure want to Delete this Room Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        final Map RoomDetails = new LinkedHashMap(1);


                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        RoomDetails.put("BranchId", branchlist.getBranchId());
                        RoomDetails.put("VASId", branchlist.getVASId());
                        requestBody.put("ValueAddedServices", RoomDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("Update", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {
                        showCustomDialog(branchlist);
                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddVAS(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(mCtx, "VAS has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((VASList) mCtx).Refereshview();


                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddVAS(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(mCtx, "VAS has been Updated successfully..!!", Toast.LENGTH_SHORT).show();
                        buttonListener.onMethodCallbackActivate();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final VASListResponse.Response branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addroompopup, viewGroup, false);


        final TextView roomtittle = (TextView) dialogView.findViewById(R.id.roomtittle);
        final TextView roomnametv = (TextView) dialogView.findViewById(R.id.roomnametv);
        final TextView costperdaytv = (TextView) dialogView.findViewById(R.id.costperdaytv);
        final TextView costpermonthtv = (TextView) dialogView.findViewById(R.id.costpermonthtv);
        final EditText name = (EditText) dialogView.findViewById(R.id.roomname);
        final EditText cot = (EditText) dialogView.findViewById(R.id.Noofcot);
        final LinearLayout cotLL = (LinearLayout) dialogView.findViewById(R.id.noofcotLL);
        final EditText cost = (EditText) dialogView.findViewById(R.id.cost);
        final EditText costpermonth = (EditText) dialogView.findViewById(R.id.costpermonth);
        final EditText desc = (EditText) dialogView.findViewById(R.id.note);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);

        roomnametv.setText("VAS");
        roomtittle.setText("VAS");
        costperdaytv.setText("CostPerDay");
        costpermonthtv.setText("CostPerMonth");
        cotLL.setVisibility(View.GONE);
        buttonadd.setText("Update");
        desc.setText(branchlist.getVASDesc());
        cost.setText(branchlist.getCostPerDay());
        costpermonth.setText(branchlist.getCostPerMonth());
        //cot.setText(branchlist.getNoOfCosts());
        name.setText(branchlist.getVAS());
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0) {
                    Toast.makeText(mCtx, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("VAS", name.getText().toString().trim());
                RoomDetails.put("VASId", branchlist.getVASId());
                RoomDetails.put("VASDesc", desc.getText().toString().trim());
                RoomDetails.put("CostPerMonth", costpermonth.getText().toString().trim());
                RoomDetails.put("CostPerDay", cost.getText().toString().trim());
                RoomDetails.put("VASActiveInd", "Y");

                RoomDetails.put("BranchId",branchlist.getBranchId());
                requestBody.put("Operator", "U");
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("ValueAddedServices", RoomDetails);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating...</font></b>"), "Please wait ...");
            }
        });


    }
}

