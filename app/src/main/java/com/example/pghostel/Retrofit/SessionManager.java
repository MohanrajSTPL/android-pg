package com.example.pghostel.Retrofit;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private SharedPreferences sharedPreferences;

    public void setjwt(Context context, String key, String jwt) {
        sharedPreferences = context.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();

        loginPrefsEditor.putString(key, jwt);
        loginPrefsEditor.apply();
    }

    public String getSharedPreferencesValues(Context context, String key) {
        sharedPreferences = context.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "notfound");
    }

    public void Clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
