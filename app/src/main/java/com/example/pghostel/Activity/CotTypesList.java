package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.CotTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.CotTypeListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CotTypesList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private CotTypeResponse cotTypeResponse;
    private AddBranchResponse addBranchResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;
    private int B_id;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(CotTypesList.this, 1));

        B_id = getIntent().getIntExtra("ID", 0);

        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(CotTypesList.this, 1));

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(CotTypesList.this, "JWT"));


        getCotTypesList(requestBody);

        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(CotTypesList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(CotTypesList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(CotTypesList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<CotTypeResponse.ResponseEntity> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (CotTypeResponse.ResponseEntity list : cotTypeResponse.getResponse()) {

                        if (list.getCotTypeActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (CotTypeResponse.ResponseEntity list : cotTypeResponse.getResponse()) {

                        if (list.getCotTypeActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = cotTypeResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(CotTypesList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                CotTypeListAdapter b = new CotTypeListAdapter(CotTypesList.this, responses);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    public void Refereshview() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(CotTypesList.this, "JWT"));
        getCotTypesList(requestBody);
    }



    private void getCotTypesList(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListCotType(requestBody).enqueue(new Callback<CotTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<CotTypeResponse> call, Response<CotTypeResponse> response) {
                try {
                    // if (response.code() == 201) {
                    cotTypeResponse = new CotTypeResponse();
                    cotTypeResponse = response.body();
                    if (cotTypeResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(CotTypesList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (cotTypeResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (cotTypeResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        CotTypeListAdapter b = new CotTypeListAdapter(CotTypesList.this, cotTypeResponse.getResponse());
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(CotTypesList.this, cotTypeResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(CotTypesList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CotTypeResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(CotTypesList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(CotTypesList.this)).inflate(R.layout.addcottypepopup, viewGroup, false);


        final EditText name = (EditText) dialogView.findViewById(R.id.cottypename);
        final EditText desc = (EditText) dialogView.findViewById(R.id.cottypenote);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(CotTypesList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0) {
                    Toast.makeText(CotTypesList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Map<String, Object> requestBody = new HashMap<>();
                Map CotTypeDetails = new LinkedHashMap(3);
                CotTypeDetails.put("CotType", name.getText().toString().trim());
                CotTypeDetails.put("CotTypeDesc", desc.getText().toString().trim());
                CotTypeDetails.put("CotTypeActiveInd", "Y");

                requestBody.put("BranchId", String.valueOf(B_id));
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(CotTypesList.this, "JWT"));
                requestBody.put("CotTypes", CotTypeDetails);
                bar.setVisibility(View.VISIBLE);
                AddCotType(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(CotTypesList.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }

    private void AddCotType(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddCotType(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(CotTypesList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(CotTypesList.this, "Cot Type has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();

                    } else {
                        Toast.makeText(CotTypesList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(CotTypesList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(CotTypesList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {
        AlertDialog diaBox = AskOption(pos);
        diaBox.show();
    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }

    private AlertDialog AskOption(final int pos) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(CotTypesList.this)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Room?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        final Map RoomDetails = new LinkedHashMap(1);


                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(CotTypesList.this, "JWT"));
                        requestBody.put("BranchId", String.valueOf(cotTypeResponse.getResponse().get(pos).getBranchId()));

                        RoomDetails.put("RoomTypeId", String.valueOf(cotTypeResponse.getResponse().get(pos).getBranchId()));
                        requestBody.put("RoomTypes", RoomDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(CotTypesList.this, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddRoomTypeDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(CotTypesList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(CotTypesList.this, "Room has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        Refereshview();

                    } else {
                        Toast.makeText(CotTypesList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(CotTypesList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(CotTypesList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
