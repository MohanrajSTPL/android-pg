package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {


    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response {

        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("AccountName")
        @Expose
        private String accountName;
        @SerializedName("JWT")
        @Expose
        private String jWT;
        @SerializedName("SubscriptionEndDt")
        @Expose
        private String subscriptionEndDt;
        @SerializedName("UserType")
        @Expose
        private String userType;
        @SerializedName("BranchId")
        @Expose
        private String branchId;
        @SerializedName("ActiveInd")
        @Expose
        private String activeInd;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getJWT() {
            return jWT;
        }

        public void setJWT(String jWT) {
            this.jWT = jWT;
        }

        public String getSubscriptionEndDt() {
            return subscriptionEndDt;
        }

        public void setSubscriptionEndDt(String subscriptionEndDt) {
            this.subscriptionEndDt = subscriptionEndDt;
        }
        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getBranchId() {
            return branchId;
        }

        public void setBranchId(String branchId) {
            this.branchId = branchId;
        }

        public String getActiveInd() {
            return activeInd;
        }

        public void setActiveInd(String activeInd) {
            this.activeInd = activeInd;
        }

    }
}
