package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BranchDetailResponse {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response {

        @SerializedName("BranchId")
        @Expose
        private String branchId;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("ServiceType")
        @Expose
        private String serviceType;
        @SerializedName("BranchGender")
        @Expose
        private String branchGender;
        @SerializedName("BranchAddress")
        @Expose
        private String branchAddress;
        @SerializedName("BranchCity")
        @Expose
        private String branchCity;
        @SerializedName("BranchState")
        @Expose
        private String branchState;
        @SerializedName("BranchCountry")
        @Expose
        private String branchCountry;
        @SerializedName("BranchPincode")
        @Expose
        private String branchPincode;
        @SerializedName("BranchEmailId")
        @Expose
        private String branchEmailId;
        @SerializedName("BranchMobileNo")
        @Expose
        private String branchMobileNo;
        @SerializedName("BranchActiveInd")
        @Expose
        private String branchActiveInd;

        public String getBranchId() {
            return branchId;
        }

        public void setBranchId(String branchId) {
            this.branchId = branchId;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getBranchGender() {
            return branchGender;
        }

        public void setBranchGender(String branchGender) {
            this.branchGender = branchGender;
        }

        public String getBranchAddress() {
            return branchAddress;
        }

        public void setBranchAddress(String branchAddress) {
            this.branchAddress = branchAddress;
        }

        public String getBranchCity() {
            return branchCity;
        }

        public void setBranchCity(String branchCity) {
            this.branchCity = branchCity;
        }

        public String getBranchState() {
            return branchState;
        }

        public void setBranchState(String branchState) {
            this.branchState = branchState;
        }

        public String getBranchCountry() {
            return branchCountry;
        }

        public void setBranchCountry(String branchCountry) {
            this.branchCountry = branchCountry;
        }

        public String getBranchPincode() {
            return branchPincode;
        }

        public void setBranchPincode(String branchPincode) {
            this.branchPincode = branchPincode;
        }

        public String getBranchEmailId() {
            return branchEmailId;
        }

        public void setBranchEmailId(String branchEmailId) {
            this.branchEmailId = branchEmailId;
        }

        public String getBranchMobileNo() {
            return branchMobileNo;
        }

        public void setBranchMobileNo(String branchMobileNo) {
            this.branchMobileNo = branchMobileNo;
        }

        public String getBranchActiveInd() {
            return branchActiveInd;
        }

        public void setBranchActiveInd(String branchActiveInd) {
            this.branchActiveInd = branchActiveInd;
        }

    }

}
