package com.example.pghostel.ServiceAPI;



import com.example.pghostel.Activity.Reports;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.AdhoServiceListResponse;
import com.example.pghostel.ModelClass.AdhoServiceTransactionListResponse;
import com.example.pghostel.ModelClass.AppUserListResponse;
import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.BranchListResponse;
import com.example.pghostel.ModelClass.CotEnrollmentListResponse;
import com.example.pghostel.ModelClass.CotListResponse;
import com.example.pghostel.ModelClass.CotTypeResponse;
import com.example.pghostel.ModelClass.EmployeeListResponse;
import com.example.pghostel.ModelClass.FacilityListResponse;
import com.example.pghostel.ModelClass.LoginResponse;
import com.example.pghostel.ModelClass.NewuseraccountResponse;
import com.example.pghostel.ModelClass.PaymentListResponse;
import com.example.pghostel.ModelClass.PaymentModeResponse;
import com.example.pghostel.ModelClass.PhysicalRoomListResponse;
import com.example.pghostel.ModelClass.ReportsResponse;
import com.example.pghostel.ModelClass.RoomFacilityListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.ModelClass.UserTypeResponse;
import com.example.pghostel.ModelClass.VASEnrollmentListResponse;
import com.example.pghostel.ModelClass.VASListResponse;
import com.example.pghostel.Retrofit.URL;
import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.LOGIN)
    Call<LoginResponse> getLoginDetails(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.SIGNUP)
    Call<NewuseraccountResponse> getSignupDetails(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.BRANCH)
    Call<AddBranchResponse> getAddbranchDetails(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.BRANCH)
    Call<BranchListResponse> getBranchListDetails(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.BRANCH)
    Call<BranchDetailResponse> getBranchItemDetails(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.SERVICETYPE)
    Call<ServiceTypeResponse> getServiceTypeDetails(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.USERTYPE)
    Call<UserTypeResponse> getUserTypeList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.FACILITY)
    Call<FacilityListResponse> getFacilityListDetails(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOMFACILITY)
    Call<RoomFacilityListResponse> getRoomTypeFacilityListDetails(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOMFACILITY)
    Call<AddBranchResponse> AddRoomTypeFacilityDetails(@Body  JsonObject body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.FACILITY)
    Call<FacilityListResponse> AddFacilityDetails(@Body JsonObject body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOMTYPE)
    Call<RoomListresponse> getRoomTypeListDetails(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOMTYPE)
    Call<AddBranchResponse> AddRoomTypeDetails(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COTTYPE)
    Call<AddBranchResponse> AddCotType(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COTTYPE)
    Call<CotTypeResponse> getListCotType(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOM)
    Call<AddBranchResponse> AddPhysicalRoom(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ROOM)
    Call<PhysicalRoomListResponse> getListPhysicalRoom(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COT)
    Call<AddBranchResponse> AddCot(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COT)
    Call<CotListResponse> getListCot(@Body  Map<String, String> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.TENANT)
    Call<AddBranchResponse> AddTenant(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.TENANT)
    Call<TenantListResponse> getListTenant(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.EMPLOYEE)
    Call<AddBranchResponse> AddEmployee(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.EMPLOYEE)
    Call<EmployeeListResponse> getEmployeeList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.APPUSER)
    Call<AddBranchResponse> AddAppUser(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.APPUSER)
    Call<AppUserListResponse> getAppUserList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COTENROLLMENT)
    Call<AddBranchResponse> AddCotEnrollment(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.COTENROLLMENT)
    Call<CotEnrollmentListResponse> getCotEnrollmentList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.VASENROLLMENT)
    Call<AddBranchResponse> AddVASEnrollment(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.VASENROLLMENT)
    Call<VASEnrollmentListResponse> getVASEnrollmentList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.VAS)
    Call<AddBranchResponse> AddVAS(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.VAS)
    Call<VASListResponse> getVASList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.PAYMENTMODE)
    Call<PaymentModeResponse> getPaymentModeList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.PAYMENTTXN)
    Call<AddBranchResponse> AddPayment(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.PAYMENTTXN)
    Call<PaymentListResponse> getPaymentList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ADHOCSERVICETRANSACTION)
    Call<AddBranchResponse> AddAdhoServiceTransaction(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ADHOCSERVICETRANSACTION)
    Call<AdhoServiceTransactionListResponse> getAdhoServiceTransactionList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ADHOCSERVICE)
    Call<AddBranchResponse> AddAdhoService(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.ADHOCSERVICE)
    Call<AdhoServiceListResponse> getAdhoServiceList(@Body  Map<String, Object> body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST(URL.REPORTSVACANCY)
    Call<ReportsResponse> getReportsList(@Body  Map<String, Object> body);
}
