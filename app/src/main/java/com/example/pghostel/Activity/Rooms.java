package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.RoomListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Rooms extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private RoomListresponse roomListresponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;
    private int B_id;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(Rooms.this, 1));

        B_id = getIntent().getIntExtra("ID", 0);

        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(Rooms.this, 1));

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(Rooms.this, "JWT"));


        getBranchListDetails(requestBody);
        //getServiceTypeDetails(requestBody);


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(Rooms.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(Rooms.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(Rooms.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<RoomListresponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (RoomListresponse.Response list : roomListresponse.getResponse()) {

                        if (list.getRoomTypeActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (RoomListresponse.Response list : roomListresponse.getResponse()) {

                        if (list.getRoomTypeActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = roomListresponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(Rooms.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                RoomListAdapter b = new RoomListAdapter(Rooms.this, responses, Rooms.this);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    public void Refereshview() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("BranchId", String.valueOf(B_id));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(Rooms.this, "JWT"));
        getBranchListDetails(requestBody);
    }

    private void getServiceTypeDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getServiceTypeDetails(requestBody).enqueue(new Callback<ServiceTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                try {
                    // if (response.code() == 201 || response.code() == 200) {
                    serviceTypeResponse = response.body();
                    if (serviceTypeResponse == null) {
                        Toast.makeText(Rooms.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (serviceTypeResponse.getResponse().isEmpty()) {
                        Toast.makeText(Rooms.this, "No ServiceType Records Found..!!", Toast.LENGTH_SHORT).show();
                    }
                    //  }

                } catch (Throwable e) {
                    Toast.makeText(Rooms.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                Toast.makeText(Rooms.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBranchListDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getRoomTypeListDetails(requestBody).enqueue(new Callback<RoomListresponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<RoomListresponse> call, Response<RoomListresponse> response) {
                try {
                    // if (response.code() == 201) {
                    roomListresponse = new RoomListresponse();
                    roomListresponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (roomListresponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(Rooms.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (roomListresponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (roomListresponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        RoomListAdapter b = new RoomListAdapter(Rooms.this, roomListresponse.getResponse(), Rooms.this);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(Rooms.this, roomListresponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(Rooms.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RoomListresponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(Rooms.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(Rooms.this)).inflate(R.layout.addroompopup, viewGroup, false);


        final TextView roomtittle = (TextView) dialogView.findViewById(R.id.roomtittle);
        final TextView roomnametv = (TextView) dialogView.findViewById(R.id.roomnametv);
        final EditText name = (EditText) dialogView.findViewById(R.id.roomname);
        final EditText cot = (EditText) dialogView.findViewById(R.id.Noofcot);
        final EditText cost = (EditText) dialogView.findViewById(R.id.cost);
        final EditText costpermonth = (EditText) dialogView.findViewById(R.id.costpermonth);
        final EditText desc = (EditText) dialogView.findViewById(R.id.note);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);
        roomnametv.setText("Room Type");
        roomtittle.setText("Room Type");

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(Rooms.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0 || cot.length() == 0 || cost.length() == 0) {
                    Toast.makeText(Rooms.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("RoomType", name.getText().toString().trim());
                RoomDetails.put("RoomTypeDesc", desc.getText().toString().trim());
                RoomDetails.put("NoOfCots", cot.getText().toString().trim());
                RoomDetails.put("CostPerCotPerMonth", costpermonth.getText().toString().trim());
                RoomDetails.put("CostPerCotPerDay", cost.getText().toString().trim());
                RoomDetails.put("RoomTypeActiveInd", "Y");

                requestBody.put("BranchId", String.valueOf(B_id));
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(Rooms.this, "JWT"));
                requestBody.put("RoomTypes", RoomDetails);
                bar.setVisibility(View.VISIBLE);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(Rooms.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddRoomTypeDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(Rooms.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(Rooms.this, "Room Type has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();

                    } else {
                        Toast.makeText(Rooms.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(Rooms.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(Rooms.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {
        AlertDialog diaBox = AskOption(pos);
        diaBox.show();
    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }

    private AlertDialog AskOption(final int pos) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(Rooms.this)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Room Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        final Map RoomDetails = new LinkedHashMap(1);


                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(Rooms.this, "JWT"));
                        requestBody.put("BranchId", String.valueOf(roomListresponse.getResponse().get(pos).getBranchId()));

                        RoomDetails.put("RoomTypeId", String.valueOf(roomListresponse.getResponse().get(pos).getRoomTypeId()));
                        requestBody.put("RoomTypes", RoomDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(Rooms.this, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddRoomTypeDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(Rooms.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(Rooms.this, "Room Type has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        Refereshview();

                    } else {
                        Toast.makeText(Rooms.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(Rooms.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(Rooms.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
