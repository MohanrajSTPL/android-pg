package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchDetails extends AppCompatActivity {

    EditText Branchname;
    Spinner ServiceType;
    EditText Address;
    EditText City;
    EditText State;
    EditText Country;
    EditText Pincode;
    EditText MobileNo;
    EditText EmailID;
    RadioButton male,female;
    RadioGroup gender;
    Button update;
    private SessionManager manager;
    private BranchDetailResponse branchDetailResponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    int B_id;
    private ProgressDialog pDialog;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_details);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        Branchname = (EditText)findViewById(R.id.branch_name);
        ServiceType = (Spinner) findViewById(R.id.input_servicetype);
        Address = (EditText)findViewById(R.id.address);
        City = (EditText)findViewById(R.id.city);
        State = (EditText)findViewById(R.id.state);
        Country = (EditText)findViewById(R.id.country);
        Pincode = (EditText)findViewById(R.id.pincode);
        MobileNo = (EditText)findViewById(R.id.mobileno);
        EmailID = (EditText)findViewById(R.id.emailID);
        male = (RadioButton)findViewById(R.id.radio_gender_male);
        female = (RadioButton)findViewById(R.id.radio_gender_female);
        gender = (RadioGroup)findViewById(R.id.radio_group_gender);
        update = (Button)findViewById(R.id.updatebranchdetails);


        B_id = getIntent().getIntExtra("ID", 0);

        Map<String, String> requestBody1 = new HashMap<>();

        requestBody1.put("Operator", "R");
        requestBody1.put("JWT", manager.getSharedPreferencesValues(BranchDetails.this, "JWT"));

        pDialog = ProgressDialog.show(BranchDetails.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");
        getServiceTypeDetails(requestBody1);



        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = gender.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if(selectedId==-1){
                    Toast.makeText(BranchDetails.this,"Plese select the Gender.!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String radio_value = "";
                if(radioButton.getText().toString().equals("Male"))
                    radio_value = "MEN";
                else
                    radio_value = "WOMEN";

                Map<String, Object> requestBody = new HashMap<>();
                Map BranchDetails = new LinkedHashMap(11);
                BranchDetails.put("BranchId", String.valueOf(B_id));
                BranchDetails.put("BranchName", Branchname.getText().toString().trim());
                BranchDetails.put("ServiceType", ServiceType.getSelectedItem().toString());
                BranchDetails.put("BranchGender", radioButton.getText().toString().toUpperCase());
                BranchDetails.put("BranchAddress", Address.getText().toString());
                BranchDetails.put("BranchCity", City.getText().toString());
                BranchDetails.put("BranchState", State.getText().toString());
                BranchDetails.put("BranchCountry", Country.getText().toString());
                BranchDetails.put("BranchPincode", Pincode.getText().toString());
                BranchDetails.put("BranchEmailId", EmailID.getText().toString());
                BranchDetails.put("BranchMobileNo", MobileNo.getText().toString());
                BranchDetails.put("BranchActiveInd", "Y");
                requestBody.put("Operator", "U");
                requestBody.put("JWT", manager.getSharedPreferencesValues(BranchDetails.this, "JWT"));
                requestBody.put("BranchDetails", BranchDetails);

                pDialog = ProgressDialog.show(BranchDetails.this, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
                updateBranchDetails(requestBody);
            }
        });
//        AlertDialog diaBox = AskOption();
//        diaBox.show();
    }
    private void getServiceTypeDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getServiceTypeDetails(requestBody).enqueue(new Callback<ServiceTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                try {
                    // if (response.code() == 201 || response.code() == 200) {
                    serviceTypeResponse = response.body();
                    if (serviceTypeResponse == null) {
                        Toast.makeText(BranchDetails.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (serviceTypeResponse.getResponse().isEmpty()) {
                        Toast.makeText(BranchDetails.this, "No ServiceType Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    List<String> servicetypelist = new ArrayList<String>();

                    if (serviceTypeResponse != null) {
                        for (ServiceTypeResponse.Response list : serviceTypeResponse.getResponse()) {
                            servicetypelist.add(list.getSERVICETYPE());
                        }
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(BranchDetails.this,
                            android.R.layout.simple_dropdown_item_1line, servicetypelist);

                    ServiceType.setAdapter(arrayAdapter);

                    Map<String, Object> requestBody = new HashMap<>();
                    final Map BranchDetails = new LinkedHashMap(1);
                    BranchDetails.put("BranchId", String.valueOf(B_id));
                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(BranchDetails.this, "JWT"));
                    requestBody.put("BranchDetails", BranchDetails);
                    getBranchItemDetails(requestBody);

                } catch (Throwable e) {
                    Toast.makeText(BranchDetails.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                Toast.makeText(BranchDetails.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(BranchDetails.this)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Do you want to Delete")

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void getBranchItemDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getBranchItemDetails(requestBody).enqueue(new Callback<BranchDetailResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<BranchDetailResponse> call, Response<BranchDetailResponse> response) {
                try {
                    // if(response.code()==201) {
                    branchDetailResponse = response.body();
                    if (branchDetailResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(BranchDetails.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (branchDetailResponse.getMessage().equals("Success")) {
                        for (BranchDetailResponse.Response list : branchDetailResponse.getResponse()) {
                            Branchname.setText(list.getBranchName());
                            ServiceType.setSelection(getIndex(ServiceType, list.getServiceType()));
                            City.setText(list.getBranchCity());
                            Address.setText(list.getBranchAddress());
                            State.setText(list.getBranchState());
                            Country.setText(list.getBranchCountry());
                            Pincode.setText(list.getBranchPincode());
                            MobileNo.setText(list.getBranchMobileNo());
                            EmailID.setText(list.getBranchEmailId());

                            if(list.getBranchGender().equals("MEN")){
                                male.setChecked(true);
                            }else {
                                female.setChecked(true);
                            }
                        }
                        pDialog.dismiss();
                        
                    } else {
                        pDialog.dismiss();
                        Toast.makeText(BranchDetails.this, branchDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(BranchDetails.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchDetailResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(BranchDetails.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }
    private void updateBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAddbranchDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(BranchDetails.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(BranchDetails.this, "Branch has been updated successfully..!!", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(BranchDetails.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(BranchDetails.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(BranchDetails.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
