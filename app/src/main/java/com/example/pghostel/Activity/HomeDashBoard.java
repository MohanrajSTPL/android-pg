package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import com.example.pghostel.R;

import java.util.Objects;

public class HomeDashBoard extends AppCompatActivity {


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_dash_board);

        Objects.requireNonNull(getSupportActionBar()).hide();

        GridLayout gridLayout=(GridLayout)findViewById(R.id.mainGrid);

        setSingleEvent(gridLayout);
    }
    private void setSingleEvent(GridLayout gridLayout) {
        for(int i = 0; i<gridLayout.getChildCount();i++){
            CardView cardView=(CardView)gridLayout.getChildAt(i);
            final int finalI= i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI==0) {
                        Intent it = new Intent(HomeDashBoard.this, BranchList.class);
                        it.putExtra("status",0);
                        startActivity(it);
                    }else if(finalI==1){
                        Intent it = new Intent(HomeDashBoard.this, BranchDashboard.class);
                       // startActivity(it);
                    }else if(finalI==5) {
                        Intent it = new Intent(HomeDashBoard.this, AddUser.class);
                        startActivity(it);
                    }else if(finalI==3){
                        Intent i=new Intent(HomeDashBoard.this, Reports.class);
                        startActivity(i);
                    }else if(finalI==4) {
                        Intent it = new Intent(HomeDashBoard.this, SettingsPage.class);
                        startActivity(it);
                    }
                }
            });
        }
    }

}
