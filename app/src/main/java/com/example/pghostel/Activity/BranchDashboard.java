package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.example.pghostel.R;
import com.example.pghostel.Retrofit.SessionManager;

import java.util.Objects;

public class BranchDashboard extends AppCompatActivity {


    private  int ID;
    private SessionManager manager;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_dashboard);

        Objects.requireNonNull(getSupportActionBar()).hide();

        manager = new SessionManager();

       // ID = getIntent().getIntExtra("ID",0);
        ID = Integer.parseInt(manager.getSharedPreferencesValues(BranchDashboard.this, "BranchID"));

        GridLayout gridLayout=(GridLayout)findViewById(R.id.mainGridBranch);

        TextView more = (TextView)findViewById(R.id.textGrid);

        SessionManager manager = new SessionManager();

        more.setText(manager.getSharedPreferencesValues(BranchDashboard.this, "BranchName"));
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(BranchDashboard.this, BranchDetails.class);
                i.putExtra("ID",ID);
                startActivity(i);
            }
        });

        setSingleEvent(gridLayout);
    }
    private void setSingleEvent(GridLayout gridLayout) {
        for(int i = 0; i<gridLayout.getChildCount();i++){
            CardView cardView=(CardView)gridLayout.getChildAt(i);
            final int finalI= i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI==0) {
                        Intent i=new Intent(BranchDashboard.this, Facility.class);
                        i.putExtra("ID",ID);
                        startActivity(i);
                    }else if(finalI==1){
                        Intent i=new Intent(BranchDashboard.this, Rooms.class);
                        i.putExtra("ID",ID);
                        startActivity(i);
                    }else if(finalI==2){
                        Intent i=new Intent(BranchDashboard.this, TenantList.class);
                        i.putExtra("ID",ID);
                        startActivity(i);
                        finish();
                    }else if(finalI==3){
                        Intent i=new Intent(BranchDashboard.this, PaymentDashboard.class);
                        startActivity(i);
                    }else if(finalI==5){
                        Intent i=new Intent(BranchDashboard.this, Reports.class);
                        startActivity(i);
                    }else if(finalI==6){
                        Intent i=new Intent(BranchDashboard.this, EmployeeList.class);
                        i.putExtra("ID",ID);
                        startActivity(i);
                        finish();
                    }else if(finalI==7){
                        Intent i=new Intent(BranchDashboard.this, SettingsPage.class);
                        startActivity(i);
                    }else if(finalI==8){
                        Intent i=new Intent(BranchDashboard.this, AllRoomList.class);
                        i.putExtra("ID",ID);
                        startActivity(i);
                    }else if(finalI==9){
                        Intent i=new Intent(BranchDashboard.this, AdhoServiceList.class);
                        startActivity(i);
                    }else if(finalI==10){
                        Intent i=new Intent(BranchDashboard.this, cotEnrollmentList.class);
                        startActivity(i);
                    }else if(finalI==11){
                        Intent i=new Intent(BranchDashboard.this, VASList.class);
                        startActivity(i);
                    }
                }
            });
        }
    }

}
