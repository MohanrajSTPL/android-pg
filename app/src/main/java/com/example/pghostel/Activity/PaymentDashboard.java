package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.example.pghostel.R;
import com.example.pghostel.Retrofit.SessionManager;

import java.util.Objects;

import static android.os.Build.ID;

public class PaymentDashboard extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_dashboard);

        Objects.requireNonNull(getSupportActionBar()).hide();

        GridLayout gridLayout=(GridLayout)findViewById(R.id.PaymentGridBranch);

        setSingleEvent(gridLayout);
    }
    private void setSingleEvent(GridLayout gridLayout) {
        for(int i = 0; i<gridLayout.getChildCount();i++){
            CardView cardView=(CardView)gridLayout.getChildAt(i);
            final int finalI= i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI==0) {
                        Intent i=new Intent(PaymentDashboard.this, PaymentList.class);
                        startActivity(i);
                    }
                }
            });
        }
    }

}

