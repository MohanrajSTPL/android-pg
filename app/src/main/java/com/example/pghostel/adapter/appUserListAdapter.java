package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.AppUserList;
import com.example.pghostel.Activity.CotList;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.AppUserListResponse;
import com.example.pghostel.ModelClass.EmployeeListResponse;
import com.example.pghostel.ModelClass.UserTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.searchablespinnerclass.SearchableSpinner;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class appUserListAdapter extends RecyclerView.Adapter<appUserListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AlertDialog alertDialog;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<AppUserListResponse.Response> responses;
    private AddBranchResponse addBranchResponse;


    private SearchableSpinner employee;
    private MaterialBetterSpinner userType;
    private EditText password;
    private Button addUser;
    private UserTypeResponse userTypeResponse;
    private EmployeeListResponse employeeListResponse;
    private int empID,userTypeID;
    private String empNo,userTID;
    
    //getting the context and product list with constructor
    public appUserListAdapter(Context mCtx, List<AppUserListResponse.Response> rommfacilitylist) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = rommfacilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final AppUserListResponse.Response roomfacilitylist = responses.get(position);

        //binding the data with the viewholder views
        holder.facilityname.setText("Role: " + roomfacilitylist.getUserType());
        holder.facilityPrice.setText("Mobile No: "+roomfacilitylist.getMobileNo());
        holder.facilityNotes.setText("Email Id: "+roomfacilitylist.getEmailId());


        if (roomfacilitylist.getUserTypeId().equals("N")) {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.facilityStatus.setText("IN ACTIVE");
        } else {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.facilityStatus.setText("ACTIVE");
        }




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {


            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if(!responses.get(position).getUserTypeId().equals("N")) {
                    AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }else {
                    AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;


        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
            // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }

    private AlertDialog AskOption(final AppUserListResponse.Response branchlist) {

        return new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Alert")
                .setMessage("Are you going to do?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Map<String, Object> requestBody = new HashMap<>();

                        Map CotTypeDetails = new LinkedHashMap(1);
                        CotTypeDetails.put("AppUserId", branchlist.getAppUserId());
                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("AppUser", CotTypeDetails);

                        DeleteAppuser(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();

                    }

                })
                .setNegativeButton("Update", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {
                        showCustomDialog(branchlist);
                        dialog.dismiss();
                    }
                })
                .create();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final AppUserListResponse.Response branchlist) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //viewGroup = root.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.activity_add_user, viewGroup, false);


        employee = (SearchableSpinner) dialogView.findViewById(R.id.material_spinner1);
        userType = (MaterialBetterSpinner) dialogView.findViewById(R.id.material_spinner2);
        password = (EditText) dialogView.findViewById(R.id.password);
        addUser = (Button) dialogView.findViewById(R.id.adduser);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));

        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

        empNo = branchlist.getEmployeeNo();
        userTID = branchlist.getUserTypeId();
        userTypeID = Integer.parseInt(branchlist.getUserTypeId());
        getUserTypeList(requestBody);


        employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // String mSelectedText = adapterView.getItemAtPosition(position).toString();
                empID = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        userType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // String mSelectedText = adapterView.getItemAtPosition(position).toString();
                userTypeID = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        addUser.setText("Update");
        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Map<String, Object> requestBody = new HashMap<>();

                Map CotDetails = new LinkedHashMap(3);
                CotDetails.put("Password", password.getText().toString().trim());
                CotDetails.put("UserTypeId", userTypeID);
                CotDetails.put("AppUserId", branchlist.getAppUserId());

                requestBody.put("Operator", "U");
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("AppUser", CotDetails);

                UpdateAppuser(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
            }
        });
        
        
    }

    private void DeleteAppuser(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAppUser(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, "App User has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((AppUserList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void UpdateAppuser(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAppUser(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    alertDialog.dismiss();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                        ((AppUserList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserTypeList(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getUserTypeList(requestBody).enqueue(new Callback<UserTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<UserTypeResponse> call, Response<UserTypeResponse> response) {
                try {
                    userTypeResponse = response.body();
                    if (userTypeResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (userTypeResponse.getResponse().isEmpty()) {
                        Toast.makeText(mCtx, "No User Type Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<String> usertypelist = new ArrayList<String>();
                    int pos = 0;
                    int index = 0;
                    if (userTypeResponse != null) {
                        for (UserTypeResponse.Response list : userTypeResponse.getResponse()) {
                            usertypelist.add(list.getUserType());
                            if(list.getUserTypeId().equals(userTID)){
                                pos = index;
                            }
                            index++;
                        }

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mCtx, android.R.layout.simple_dropdown_item_1line, usertypelist);

                    userType.setAdapter(adapter);
                    userType.setSelection(pos);

                    Map<String, Object> requestBody = new HashMap<>();

                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));

                    getListEmployee(requestBody);

                } catch (Throwable e) {
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserTypeResponse> call, Throwable t) {
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getListEmployee(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getEmployeeList(requestBody).enqueue(new Callback<EmployeeListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<EmployeeListResponse> call, Response<EmployeeListResponse> response) {
                try {
                    pDialog.dismiss();
                    employeeListResponse = new EmployeeListResponse();
                    employeeListResponse = response.body();

                    if (employeeListResponse == null) {

                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (employeeListResponse.getMessage().trim().equals("Success")) {

                        if (employeeListResponse.getResponse().isEmpty()) {

                            Toast.makeText(mCtx, "Employee list is empty..!!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        List<String> employeelist = new ArrayList<String>();
                        int pos = 0;
                        int index = 0;
                        if (employeeListResponse != null) {
                            for (EmployeeListResponse.Response list : employeeListResponse.getResponse()) {
                                employeelist.add(list.getEmployeeName());
                                if(list.getEmployeeNo().equals(empNo)){
                                    pos = index;
                                }
                                index++;
                            }
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mCtx, android.R.layout.simple_dropdown_item_1line, employeelist);

                        employee.setAdapter(adapter);
                        //employee.setSelection(getIndex(employee, empNo));
                        employee.setSelection(pos);
                        employee.setEnabled(false);
                    } else {

                        Toast.makeText(mCtx, employeeListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EmployeeListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }

}

