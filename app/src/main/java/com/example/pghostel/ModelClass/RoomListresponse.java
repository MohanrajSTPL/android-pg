package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoomListresponse {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("BranchId")
        @Expose
        private String branchId;
        @SerializedName("RoomTypeId")
        @Expose
        private String roomTypeId;
        @SerializedName("RoomType")
        @Expose
        private String roomType;
        @SerializedName("RoomTypeDesc")
        @Expose
        private String roomTypeDesc;
        @SerializedName("NoOfCots")
        @Expose
        private String noOfCosts;
        @SerializedName("CostPerCotPerMonth")
        @Expose
        private String costPerCotsPerMonth;
        @SerializedName("CostPerCotPerDay")
        @Expose
        private String costPerCotsPerDay;
        @SerializedName("RoomTypeActiveInd")
        @Expose
        private String roomTypeActiveInd;

        public String getBranchId() {
            return branchId;
        }

        public void setBranchId(String branchId) {
            this.branchId = branchId;
        }

        public String getRoomTypeId() {
            return roomTypeId;
        }

        public void setRoomTypeId(String roomTypeId) {
            this.roomTypeId = roomTypeId;
        }

        public String getRoomType() {
            return roomType;
        }

        public void setRoomType(String roomType) {
            this.roomType = roomType;
        }

        public String getRoomTypeDesc() {
            return roomTypeDesc;
        }

        public void setRoomTypeDesc(String roomTypeDesc) {
            this.roomTypeDesc = roomTypeDesc;
        }

        public String getNoOfCosts() {
            return noOfCosts;
        }

        public void setNoOfCosts(String noOfCosts) {
            this.noOfCosts = noOfCosts;
        }

        public String getCostPerCotsPerMonth() {
            return costPerCotsPerMonth;
        }

        public void setCostPerCotsPerMonth(String costPerCotsPerMonth) {
            this.costPerCotsPerMonth = costPerCotsPerMonth;
        }

        public String getCostPerCotsPerDay() {
            return costPerCotsPerDay;
        }

        public void setCostPerCotsPerDay(String costPerCotsPerDay) {
            this.costPerCotsPerDay = costPerCotsPerDay;
        }

        public String getRoomTypeActiveInd() {
            return roomTypeActiveInd;
        }

        public void setRoomTypeActiveInd(String roomTypeActiveInd) {
            this.roomTypeActiveInd = roomTypeActiveInd;
        }

    }

}
