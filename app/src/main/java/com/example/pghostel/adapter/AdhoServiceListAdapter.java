package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.AdhoServiceList;
import com.example.pghostel.Activity.AdhoServiceTransaction;
import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.AdhoServiceListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdhoServiceListAdapter extends RecyclerView.Adapter<AdhoServiceListAdapter.BranchlistViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AdapterInterface buttonListener;
    //we are storing all the products in a list
    private List<AdhoServiceListResponse.Response> responses;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private AddBranchResponse addBranchResponse;
    private AlertDialog alertDialog;

    public AdhoServiceListAdapter(Context mCtx, List<AdhoServiceListResponse.Response> roomlist, AdapterInterface fragment) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = roomlist;
        this.buttonListener = fragment;

    }

    @Override
    public AdhoServiceListAdapter.BranchlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childbranchlist, null);
        return new AdhoServiceListAdapter.BranchlistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdhoServiceListAdapter.BranchlistViewHolder holder, final int position) {
        //getting the product of the specified position
        final AdhoServiceListResponse.Response roomlist = responses.get(position);

        //binding the data with the viewholder views
        holder.branchname.setText(roomlist.getAdhocService());
        holder.Servicetype.setText(roomlist.getAdhocServiceDesc());
        holder.branchgender.setText(roomlist.getCostPerUse());

        if (roomlist.getAdhocServiceActiveInd().equals("N")) {
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.branchStatus.setText("IN ACTIVE");
        } else {
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.branchStatus.setText("ACTIVE");
        }

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!responses.get(position).getAdhocServiceActiveInd().equals("N")) {
                    //buttonListener.onMethodCallback(position);
                    AlertDialog diaBox = Option(responses.get(position));
                    diaBox.show();
                }
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mCtx, AdhoServiceTransaction.class);
                i.putExtra("AdhocSerID",responses.get(position).getAdhocServiceId());
                mCtx.startActivity(i);
            }
        });


    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class BranchlistViewHolder extends RecyclerView.ViewHolder {

        TextView branchname;
        TextView Servicetype;
        TextView branchgender;
        TextView branchStatus;


        public BranchlistViewHolder(View itemView) {
            super(itemView);

            branchname = itemView.findViewById(R.id.branchname);
            Servicetype = itemView.findViewById(R.id.Servicetype);
            branchgender = itemView.findViewById(R.id.branchgender);
            branchStatus = itemView.findViewById(R.id.branchStatus);

        }
    }
    private AlertDialog Option(final AdhoServiceListResponse.Response branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Alert")
                .setMessage("Are you going to do?")
                //.setMessage("Are you sure want to Delete this Room Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        final Map RoomDetails = new LinkedHashMap(1);

                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));

                        RoomDetails.put("BranchId", branchlist.getBranchId());
                        RoomDetails.put("AdhocServiceId", branchlist.getAdhocServiceId());
                        requestBody.put("AdhocServices", RoomDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("Update", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {
                        showCustomDialog(branchlist);
                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAdhoService(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(mCtx, "Adho Service has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((AdhoServiceList) mCtx).Refereshview();


                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private AlertDialog AskOption(final RoomListresponse.Response branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Activate")
                .setMessage("Are you sure want to Activate this Adho Service?")


                .setPositiveButton("Activate", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        Map<String, Object> requestBody = new HashMap<>();
                        Map RoomDetails = new LinkedHashMap(6);
                        RoomDetails.put("BranchId", branchlist.getBranchId());
                        RoomDetails.put("AdhocServiceId", branchlist.getRoomTypeId());
                        RoomDetails.put("AdhocService", branchlist.getRoomType());
                        RoomDetails.put("AdhocServiceDesc", branchlist.getRoomTypeDesc());
                        RoomDetails.put("Notes", branchlist.getNoOfCosts());
                        RoomDetails.put("CostPerUse", branchlist.getCostPerCotsPerMonth());
                        requestBody.put("Operator", "U");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("AdhocServices", RoomDetails);
                        AddBranchListDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Activating...</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAdhoService(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(mCtx, "Adho Service has been Updated successfully..!!", Toast.LENGTH_SHORT).show();
                        buttonListener.onMethodCallbackActivate();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final AdhoServiceListResponse.Response branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.adhoservicepopup, viewGroup, false);


        final EditText adhoservice = (EditText) dialogView.findViewById(R.id.adhoservice);
        final EditText adhoservicedesc = (EditText) dialogView.findViewById(R.id.adhoservicedesc);
        final EditText adhoservicenotes = (EditText) dialogView.findViewById(R.id.adhoservicenotes);
        final EditText adhoservicecost = (EditText) dialogView.findViewById(R.id.adhoservicecost);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);


        buttonadd.setText("Update");
        adhoservice.setText(branchlist.getAdhocService());
        adhoservicedesc.setText(branchlist.getAdhocServiceDesc());
        //adhoservicenotes.setText(branchlist.g());
        adhoservicecost.setText(branchlist.getCostPerUse());
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (adhoservicedesc.length() == 0 || adhoservicecost.length() == 0) {
                    Toast.makeText(mCtx, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("AdhocServiceId", branchlist.getAdhocServiceId());
                RoomDetails.put("AdhocService", adhoservice.getText().toString().trim());
                RoomDetails.put("AdhocServiceDesc", adhoservicedesc.getText().toString().trim());
                RoomDetails.put("Notes", adhoservicenotes.getText().toString().trim());
                RoomDetails.put("CostPerUse", adhoservicecost.getText().toString().trim());
                RoomDetails.put("BranchId",branchlist.getBranchId());
                requestBody.put("Operator", "U");
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("AdhocServices", RoomDetails);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating...</font></b>"), "Please wait ...");
            }
        });


    }
}
