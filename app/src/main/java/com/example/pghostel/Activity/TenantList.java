package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.TenantListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TenantList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private TenantListResponse tenantListResponse;
    private AddBranchResponse addBranchResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;
    private int B_id;

    @Override
    public boolean onSupportNavigateUp() {
        Intent i=new Intent(TenantList.this, BranchDashboard.class);
        i.putExtra("ID",B_id);
        startActivity(i);
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(TenantList.this, 1));

        B_id = getIntent().getIntExtra("ID", 0);

        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(TenantList.this, 1));

        Map<String, Object> requestBody = new HashMap<>();
        final Map RoomDetails = new LinkedHashMap(1);
        //RoomDetails.put("TenantMobileNo", "9630852740");
        RoomDetails.put("BranchId", B_id);
        requestBody.put("TenantDetails", RoomDetails);
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(TenantList.this, "JWT"));


        getListTenant(requestBody);

        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TenantList.this,TenantCreateActivity.class);
                i.putExtra("status",0);
                i.putExtra("ID",0);
                i.putExtra("BranchID",B_id);
                startActivity(i);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(TenantList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(TenantList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(TenantList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<TenantListResponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (TenantListResponse.Response list : tenantListResponse.getResponse()) {

                        if (list.getTenantActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (TenantListResponse.Response list : tenantListResponse.getResponse()) {

                        if (list.getTenantActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = tenantListResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(TenantList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                TenantListAdapter b = new TenantListAdapter(TenantList.this, responses);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    public void Refereshview() {
        Map<String, Object> requestBody = new HashMap<>();
        final Map RoomDetails = new LinkedHashMap(1);
        //RoomDetails.put("TenantMobileNo", "9630852740");
        RoomDetails.put("BranchId", B_id);
        requestBody.put("TenantDetails", RoomDetails);
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(TenantList.this, "JWT"));
        getListTenant(requestBody);
    }



    private void getListTenant(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListTenant(requestBody).enqueue(new Callback<TenantListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<TenantListResponse> call, Response<TenantListResponse> response) {
                try {
                    // if (response.code() == 201) {
                    tenantListResponse = new TenantListResponse();
                    tenantListResponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (tenantListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(TenantList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (tenantListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (tenantListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        TenantListAdapter b = new TenantListAdapter(TenantList.this, tenantListResponse.getResponse());
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(TenantList.this, tenantListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(TenantList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TenantListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(TenantList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(TenantList.this, BranchDashboard.class);
        i.putExtra("ID",B_id);
        startActivity(i);
        finish();
    }
}


