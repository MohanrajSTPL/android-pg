package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.ModelClass.VASEnrollmentListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.VASEnrollmentAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VASEnrollmentList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private VASEnrollmentListResponse vasEnrollmentListResponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(VASEnrollmentList.this, 1));



        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(VASEnrollmentList.this, 1));

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("BranchId", manager.getSharedPreferencesValues(VASEnrollmentList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentList.this, "JWT"));


        getBranchListDetails(requestBody);
        //getServiceTypeDetails(requestBody);


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    public void Refereshview() {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("BranchId", manager.getSharedPreferencesValues(VASEnrollmentList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentList.this, "JWT"));
        getBranchListDetails(requestBody);
    }


    private void getBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getVASEnrollmentList(requestBody).enqueue(new Callback<VASEnrollmentListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<VASEnrollmentListResponse> call, Response<VASEnrollmentListResponse> response) {
                try {
                    vasEnrollmentListResponse = new VASEnrollmentListResponse();
                    vasEnrollmentListResponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (vasEnrollmentListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASEnrollmentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (vasEnrollmentListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (vasEnrollmentListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        VASEnrollmentAdapter b = new VASEnrollmentAdapter(VASEnrollmentList.this, vasEnrollmentListResponse.getResponse(), VASEnrollmentList.this);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASEnrollmentList.this, vasEnrollmentListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(VASEnrollmentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VASEnrollmentListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(VASEnrollmentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(VASEnrollmentList.this)).inflate(R.layout.addroompopup, viewGroup, false);


        final TextView roomtittle = (TextView) dialogView.findViewById(R.id.roomtittle);
        final TextView roomnametv = (TextView) dialogView.findViewById(R.id.roomnametv);
        final EditText name = (EditText) dialogView.findViewById(R.id.roomname);
        final EditText cot = (EditText) dialogView.findViewById(R.id.Noofcot);
        final EditText cost = (EditText) dialogView.findViewById(R.id.cost);
        final EditText costpermonth = (EditText) dialogView.findViewById(R.id.costpermonth);
        final EditText desc = (EditText) dialogView.findViewById(R.id.note);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);
        roomnametv.setText("Room Type");
        roomtittle.setText("Room Type");

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(VASEnrollmentList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0 || cot.length() == 0 || cost.length() == 0) {
                    Toast.makeText(VASEnrollmentList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("RoomType", name.getText().toString().trim());
                RoomDetails.put("RoomTypeDesc", desc.getText().toString().trim());
                RoomDetails.put("NoOfCots", cot.getText().toString().trim());
                RoomDetails.put("CostPerCotPerMonth", costpermonth.getText().toString().trim());
                RoomDetails.put("CostPerCotPerDay", cost.getText().toString().trim());
                RoomDetails.put("RoomTypeActiveInd", "Y");

                requestBody.put("BranchId", manager.getSharedPreferencesValues(VASEnrollmentList.this, "BranchID"));
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(VASEnrollmentList.this, "JWT"));
                requestBody.put("RoomTypes", RoomDetails);
                bar.setVisibility(View.VISIBLE);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(VASEnrollmentList.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddRoomTypeDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(VASEnrollmentList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(VASEnrollmentList.this, "Room Type has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();

                    } else {
                        Toast.makeText(VASEnrollmentList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(VASEnrollmentList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(VASEnrollmentList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {

    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appuserlist, menu);
        MenuItem item = menu.findItem(R.id.appuserlist);
        item.setTitle("VAS Enrollment Allocation");
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.appuserlist) {
            Intent i = new Intent(VASEnrollmentList.this, VASEnrollmentAllocation.class);
            i.putExtra("VID","");
            startActivity(i);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
