package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhysicalRoomListResponse {


    @Expose
    @SerializedName("Response")
    private List<ResponseEntity> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<ResponseEntity> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class ResponseEntity {
        @Expose
        @SerializedName("RoomActiveInd")
        private String RoomActiveInd;
        @Expose
        @SerializedName("CostPerCot")
        private String CostPerCot;
        @Expose
        @SerializedName("NoOfCots")
        private String NoOfCots;
        @Expose
        @SerializedName("RoomType")
        private String RoomType;
        @Expose
        @SerializedName("Area")
        private String Area;
        @Expose
        @SerializedName("Block")
        private String Block;
        @Expose
        @SerializedName("Floor")
        private String Floor;
        @Expose
        @SerializedName("RoomNo")
        private String RoomNo;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("RoomTypeId")
        private String RoomTypeId;
        @Expose
        @SerializedName("RoomId")
        private String RoomId;

        public String getRoomActiveInd() {
            return RoomActiveInd;
        }

        public String getCostPerCot() {
            return CostPerCot;
        }

        public String getNoOfCots() {
            return NoOfCots;
        }

        public String getRoomType() {
            return RoomType;
        }

        public String getArea() {
            return Area;
        }

        public String getBlock() {
            return Block;
        }

        public String getFloor() {
            return Floor;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public String getBranchId() {
            return BranchId;
        }

        public String getRoomTypeId() {
            return RoomTypeId;
        }

        public String getRoomId() {
            return RoomId;
        }
    }
}
