package com.example.pghostel.Activity.informatiomReports;

import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.ModelClass.ReportsResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.ReportsListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomTypeReports extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private ReportsResponse reportsResponse;
    private SessionManager manager;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(RoomTypeReports.this, 1));


        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(RoomTypeReports.this, 1));


        Map<String, Object> requestBody = new HashMap<>();
        Map VacancyReportDetails = new LinkedHashMap(1);

        VacancyReportDetails.put("BranchId", getIntent().getIntExtra("BID", 0));
        VacancyReportDetails.put("RoomTypeId", getIntent().getStringExtra("RTID"));

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(RoomTypeReports.this, "JWT"));
        requestBody.put("VacancyReport", VacancyReportDetails);

        getVacancyReportDetails(requestBody);

    }

    private void getVacancyReportDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getReportsList(requestBody).enqueue(new Callback<ReportsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ReportsResponse> call, Response<ReportsResponse> response) {
                try {
                    reportsResponse = new ReportsResponse();
                    reportsResponse = response.body();
                    if (reportsResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(RoomTypeReports.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (reportsResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (reportsResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        ReportsListAdapter b = new ReportsListAdapter(RoomTypeReports.this, reportsResponse.getResponse(),2);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(RoomTypeReports.this, reportsResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(RoomTypeReports.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ReportsResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(RoomTypeReports.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}


