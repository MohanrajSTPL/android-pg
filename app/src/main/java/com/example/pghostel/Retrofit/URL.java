package com.example.pghostel.Retrofit;

public class URL {

    public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

    public static final String SIGNUP = "register_new_account_user";
    public static final String LOGIN = "user_login";
    public static final String BRANCH = "account_branch";
    public static final String SERVICETYPE = "service_type";
    public static final String FACILITY = "branch_facility";
    public static final String ROOMTYPE = "room_type";
    public static final String ROOMFACILITY = "room_type_facility";
    public static final String COTTYPE = "cot_type";
    public static final String ROOM = "room";
    public static final String COT = "cot";
    public static final String TENANT = "tenant";
    public static final String EMPLOYEE = "employee";
    public static final String APPUSER = "app_user";
    public static final String USERTYPE = "user_type";
    public static final String COTENROLLMENT = "cot_enrollment";
    public static final String VAS = "vas";
    public static final String VASENROLLMENT = "vas_enrollment";
    public static final String PAYMENTTXN = "payment_txn";
    public static final String PAYMENTMODE = "payment_mode";
    public static final String ADHOCSERVICE = "adhoc_service";
    public static final String ADHOCSERVICETRANSACTION = "adhoc_service_transaction";
    public static final String REPORTSVACANCY = "reports_vacancy";


}
