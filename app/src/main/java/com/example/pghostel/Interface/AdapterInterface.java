package com.example.pghostel.Interface;

public interface AdapterInterface {
    void onMethodCallback(int pos);
    void onMethodCallbackActivate();
}
