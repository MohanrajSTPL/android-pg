package com.example.pghostel.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.BranchListResponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.Branchlistadapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchList extends AppCompatActivity implements AdapterInterface {

    private ActionBarDrawerToggle toggle;
    DrawerLayout drawer;

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private BranchListResponse branchListResponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;
    private int status = 0;


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout actionBarLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.actionbar_layout, null);
        TextView actionBarTitleview = (TextView) actionBarLayout.findViewById(R.id.actionbar_titleview);
        actionBarTitleview.setText("Branch List");
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.LEFT);

        status = getIntent().getIntExtra("status", 0);

        ImageView drawerImageViewCheck = (ImageView) actionBarLayout.findViewById(R.id.drawer_imageview_done);

        drawerImageViewCheck.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                showFilterDialog();
            }
        });

        actionBar.setCustomView(actionBarLayout, params);
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Menudrawer 6 line working
//        drawer = findViewById(R.id.drawer_layout);
//
//        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = findViewById(R.id.nav_view);
//        setupDrawerContent(navigationView);

        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(BranchList.this, 1));

        Map<String, String> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(BranchList.this, "JWT"));


        getBranchListDetails(requestBody);
        getServiceTypeDetails(requestBody);


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(BranchList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(BranchList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(BranchList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<BranchListResponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (BranchListResponse.Response list : branchListResponse.getResponse()) {

                        if (list.getBranchActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (BranchListResponse.Response list : branchListResponse.getResponse()) {

                        if (list.getBranchActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = branchListResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(BranchList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Branchlistadapter b = new Branchlistadapter(BranchList.this, responses, BranchList.this,status);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    private void Refereshview() {
        Map<String, String> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(BranchList.this, "JWT"));
        getBranchListDetails(requestBody);
    }

    private void getServiceTypeDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getServiceTypeDetails(requestBody).enqueue(new Callback<ServiceTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                try {
                    // if (response.code() == 201 || response.code() == 200) {
                    serviceTypeResponse = response.body();
                    if (serviceTypeResponse == null) {
                        Toast.makeText(BranchList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (serviceTypeResponse.getResponse().isEmpty()) {
                        Toast.makeText(BranchList.this, "No ServiceType Records Found..!!", Toast.LENGTH_SHORT).show();
                    }

                    //  }

                } catch (Throwable e) {
                    Toast.makeText(BranchList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                Toast.makeText(BranchList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBranchListDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getBranchListDetails(requestBody).enqueue(new Callback<BranchListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<BranchListResponse> call, Response<BranchListResponse> response) {
                try {
                    // if (response.code() == 201) {
                    branchListResponse = new BranchListResponse();
                    branchListResponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (branchListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(BranchList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (branchListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (branchListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        Branchlistadapter b = new Branchlistadapter(BranchList.this, branchListResponse.getResponse(), BranchList.this,status);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(BranchList.this, branchListResponse.getError().trim(), Toast.LENGTH_SHORT).show();
                        if (branchListResponse.getError().equals("Expired token")) {
                            //Toast.makeText(BranchList.this, branchListResponse.getError(), Toast.LENGTH_SHORT).show();

                            manager.Clear(BranchList.this);
                            startActivity(new Intent(BranchList.this,Login.class));
                            finish();
                        }
                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(BranchList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(BranchList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(BranchList.this)).inflate(R.layout.addbranchpopup, viewGroup, false);


        final TextView branchname = (TextView) dialogView.findViewById(R.id.input_branchname);
        final Spinner servicetype = (Spinner) dialogView.findViewById(R.id.input_servicetype);
        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);
        final RadioGroup radioSexGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group_gender);

        List<String> servicetypelist = new ArrayList<String>();

        if (serviceTypeResponse != null) {
            for (ServiceTypeResponse.Response list : serviceTypeResponse.getResponse()) {
                servicetypelist.add(list.getSERVICETYPE());
            }
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(BranchList.this,
                android.R.layout.simple_dropdown_item_1line, servicetypelist);

        servicetype.setAdapter(arrayAdapter);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(BranchList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioSexGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(BranchList.this, "Plese select the Gender.!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String radio_value = "";
                if (radioButton.getText().toString().equals("Male"))
                    radio_value = "MEN";
                else
                    radio_value = "WOMEN";

                if (branchname.length() == 0 || servicetype.getSelectedItem().toString().length() == 0 || radio_value.length() == 0) {
                    Toast.makeText(BranchList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map BranchDetails = new LinkedHashMap(11);
                BranchDetails.put("BranchName", branchname.getText().toString().trim());
                BranchDetails.put("ServiceType", servicetype.getSelectedItem().toString());
                BranchDetails.put("BranchGender", radioButton.getText().toString().toUpperCase());
                BranchDetails.put("BranchAddress", "");
                BranchDetails.put("BranchCity", "");
                BranchDetails.put("BranchState", "");
                BranchDetails.put("BranchCountry", "");
                BranchDetails.put("BranchPincode", "");
                BranchDetails.put("BranchEmailId", "");
                BranchDetails.put("BranchMobileNo", "");
                BranchDetails.put("BranchActiveInd", "");
                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(BranchList.this, "JWT"));
                requestBody.put("BranchDetails", BranchDetails);
                bar.setVisibility(View.VISIBLE);
                AddBranchListDetails(requestBody);

            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAddbranchDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(BranchList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(BranchList.this, "Branch has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Map<String, String> requestBody = new HashMap<>();

                        requestBody.put("Operator", "R");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(BranchList.this, "JWT"));


                        getBranchListDetails(requestBody);

                    } else {
                        Toast.makeText(BranchList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(BranchList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(BranchList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {
        AlertDialog diaBox = AskOption(pos);
        diaBox.show();
    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }

    private AlertDialog AskOption(final int pos) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(BranchList.this)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Branch?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        final Map BranchDetails = new LinkedHashMap(1);
                        BranchDetails.put("BranchId", String.valueOf(branchListResponse.getResponse().get(pos).getBranchId()));
                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(BranchList.this, "JWT"));
                        requestBody.put("BranchDetails", BranchDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(BranchList.this, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAddbranchDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(BranchList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(BranchList.this, "Branch has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        Refereshview();

                    } else {
                        Toast.makeText(BranchList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(BranchList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(BranchList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        int id = menuItem.getItemId();

                        if (id == R.id.nav_share) {
                            ShareCompat.IntentBuilder.from(BranchList.this)
                                    .setType("text/plain")
                                    .setChooserTitle("Chooser title")
                                    .setText("http://play.google.com/store/apps/details?id=" + BranchList.this.getPackageName())
                                    .startChooser();
                        }else if (id == R.id.nav_logout) {
                            new AlertDialog.Builder(BranchList.this)
                                    .setTitle("Logout")
                                    .setMessage("Are you sure you want to Logout?")
                                    .setIcon(R.drawable.ic_exit)
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            manager.Clear(BranchList.this);
                                            startActivity(new Intent(BranchList.this,Login.class));
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                        }

                        return true;
                    }
                });
    }
}
