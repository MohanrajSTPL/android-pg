package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserTypeResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("UserTypeDesc")
        private String UserTypeDesc;
        @Expose
        @SerializedName("UserType")
        private String UserType;
        @Expose
        @SerializedName("UserTypeId")
        private String UserTypeId;

        public String getUserTypeDesc() {
            return UserTypeDesc;
        }

        public String getUserType() {
            return UserType;
        }

        public String getUserTypeId() {
            return UserTypeId;
        }
    }
}
