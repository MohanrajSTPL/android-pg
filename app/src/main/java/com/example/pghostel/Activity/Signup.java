package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.ModelClass.NewuseraccountResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.Retrofit.URL;
import com.example.pghostel.Retrofit.Utils;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity {

    EditText emailid;
    EditText Accountname;
    EditText password;
    EditText Confirmpassword;
    EditText mobileno;
    TextView alreadyAccount;
    Button signupBtn;
    ProgressDialog pDialog;
    NewuseraccountResponse newuseraccountResponse;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Accountname = (EditText) findViewById(R.id.signup_user);
        emailid = (EditText) findViewById(R.id.signup_emailid);
        password = (EditText)findViewById(R.id.signup_password);
        Confirmpassword = (EditText)findViewById(R.id.signup_confirmPassword);
        mobileno = (EditText)findViewById(R.id.signup_mobileno);
        alreadyAccount = (TextView)findViewById(R.id.alreadyAccount);
        signupBtn = (Button) findViewById(R.id.signupBtn);

        Objects.requireNonNull(getSupportActionBar()).hide();


        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Pattern p = Pattern.compile(URL.regEx);
                Matcher m = p.matcher(emailid.getText().toString().trim());

                if(emailid.getText().toString().trim().length()==0 || mobileno.getText().toString().trim().length()==0 || Accountname.getText().toString().trim().length()==0 || password.getText().toString().trim().length()==0){
                    Toast.makeText(Signup.this, "All Fields are Required..!!", Toast.LENGTH_SHORT).show();
                }else if(!m.find()){
                    Toast.makeText(Signup.this, "Your Email Id is Invalid...!!", Toast.LENGTH_SHORT).show();
                }else if(!password.getText().toString().trim().equals(Confirmpassword.getText().toString().trim())){
                    Toast.makeText(Signup.this, "Both password doesn't match....!!", Toast.LENGTH_SHORT).show();
                }else {
                    pDialog = ProgressDialog.show(Signup.this, Html.fromHtml("<b><font color='#ff8f61'>Signup In</font></b>"), "Please wait ...");

                    Map<String, String> requestBody = new HashMap<>();

                    requestBody.put("EmailId", emailid.getText().toString().trim());
                    requestBody.put("MobileNo", mobileno.getText().toString().trim());
                    requestBody.put("Password", password.getText().toString().trim());
                    requestBody.put("AccountName", Accountname.getText().toString().trim());

                    signup(requestBody);
                }
            }
        });
        Confirmpassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Pattern p = Pattern.compile(URL.regEx);
                    Matcher m = p.matcher(emailid.getText().toString().trim());

                    if(emailid.getText().toString().trim().length()==0 || mobileno.getText().toString().trim().length()==0 || Accountname.getText().toString().trim().length()==0 || password.getText().toString().trim().length()==0){
                        Toast.makeText(Signup.this, "All Fields are Required..!!", Toast.LENGTH_SHORT).show();
                    }else if(!m.find()){
                        Toast.makeText(Signup.this, "Your Email Id is Invalid...!!", Toast.LENGTH_SHORT).show();
                    }else if(!password.getText().toString().trim().equals(Confirmpassword.getText().toString().trim())){
                        Toast.makeText(Signup.this, "Both password doesn't match....!!", Toast.LENGTH_SHORT).show();
                    }else {
                        pDialog = ProgressDialog.show(Signup.this, Html.fromHtml("<b><font color='#ff8f61'>Signup In</font></b>"), "Please wait ...");

                        Map<String, String> requestBody = new HashMap<>();

                        requestBody.put("EmailId", emailid.getText().toString().trim());
                        requestBody.put("MobileNo", mobileno.getText().toString().trim());
                        requestBody.put("Password", password.getText().toString().trim());
                        requestBody.put("AccountName", Accountname.getText().toString().trim());

                        signup(requestBody);
                    }
                    return true;
                }
                return false;
            }
        });
        alreadyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Signup.super.onBackPressed();
                finish();
            }
        });
    }

    private void signup( Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getSignupDetails(requestBody).enqueue(new Callback<NewuseraccountResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<NewuseraccountResponse> call, Response<NewuseraccountResponse> response) {
                try {
                   // if(response.code()==201) {
                        newuseraccountResponse = response.body();
                        if(newuseraccountResponse==null){
                            Toast.makeText(Signup.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (newuseraccountResponse.getMessage().trim().equals("Success")) {
                            pDialog.cancel();
                            Toast.makeText(Signup.this, "Signup successfully..!!!", Toast.LENGTH_SHORT).show();
                            Signup.super.onBackPressed();
                            finish();
                        } else {
                            pDialog.cancel();
                            Toast.makeText(Signup.this, newuseraccountResponse.getError().trim(), Toast.LENGTH_SHORT).show();
                        }
//                    }else if(response.code()==401 || response.code()==200){
//                        pDialog.cancel();
//                        Toast.makeText(Signup.this, "Invalid Credentials..!!", Toast.LENGTH_SHORT).show();
//                    }else {
//                        pDialog.cancel();
//                        Toast.makeText(Signup.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
//                    }

                } catch (Throwable e) {
                    pDialog.cancel();
                    Toast.makeText(Signup.this,"Unable to connect server"+ e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<NewuseraccountResponse> call, Throwable t) {
                pDialog.cancel();
                Toast.makeText(Signup.this, "Unable to connect server" + t.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
