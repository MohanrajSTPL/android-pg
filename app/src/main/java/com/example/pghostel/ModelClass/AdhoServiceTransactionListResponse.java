package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdhoServiceTransactionListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("Amount")
        private String Amount;
        @Expose
        @SerializedName("Notes")
        private String Notes;
        @Expose
        @SerializedName("AdhocService")
        private String AdhocService;
        @Expose
        @SerializedName("AdhocServiceId")
        private String AdhocServiceId;
        @Expose
        @SerializedName("TransactionDate")
        private String TransactionDate;
        @Expose
        @SerializedName("BillingCycleId")
        private String BillingCycleId;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;

        public String getAmount() {
            return Amount;
        }

        public String getNotes() {
            return Notes;
        }

        public String getAdhocService() {
            return AdhocService;
        }

        public String getAdhocServiceId() {
            return AdhocServiceId;
        }

        public String getTransactionDate() {
            return TransactionDate;
        }

        public String getBillingCycleId() {
            return BillingCycleId;
        }

        public String getTenantId() {
            return TenantId;
        }
    }
}
