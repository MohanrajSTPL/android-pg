package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoomFacilityListResponse {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("RoomTypeFacilityId")
        @Expose
        private String roomTypeFacilityId;
        @SerializedName("RoomTypeId")
        @Expose
        private String roomTypeId;
        @SerializedName("FacilityId")
        @Expose
        private String facilityId;
        @SerializedName("FacilityName")
        @Expose
        private String facilityName;
        @SerializedName("FacilityDesc")
        @Expose
        private String facilityDesc;
        @SerializedName("Notes")
        @Expose
        private String notes;
        @SerializedName("RoomTypeFacilityActiveInd")
        @Expose
        private String roomTypeFacilityActiveInd;

        public String getRoomTypeFacilityId() {
            return roomTypeFacilityId;
        }

        public void setRoomTypeFacilityId(String roomTypeFacilityId) {
            this.roomTypeFacilityId = roomTypeFacilityId;
        }

        public String getRoomTypeId() {
            return roomTypeId;
        }

        public void setRoomTypeId(String roomTypeId) {
            this.roomTypeId = roomTypeId;
        }

        public String getFacilityId() {
            return facilityId;
        }

        public void setFacilityId(String facilityId) {
            this.facilityId = facilityId;
        }

        public String getFacilityName() {
            return facilityName;
        }

        public void setFacilityName(String facilityName) {
            this.facilityName = facilityName;
        }

        public String getFacilityDesc() {
            return facilityDesc;
        }

        public void setFacilityDesc(String facilityDesc) {
            this.facilityDesc = facilityDesc;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getRoomTypeFacilityActiveInd() {
            return roomTypeFacilityActiveInd;
        }

        public void setRoomTypeFacilityActiveInd(String roomTypeFacilityActiveInd) {
            this.roomTypeFacilityActiveInd = roomTypeFacilityActiveInd;
        }

    }

}
