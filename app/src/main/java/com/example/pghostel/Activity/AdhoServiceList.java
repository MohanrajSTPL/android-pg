package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.AdhoServiceListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.ServiceTypeResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.example.pghostel.adapter.AdhoServiceListAdapter;
import com.example.pghostel.adapter.RoomListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdhoServiceList extends AppCompatActivity implements AdapterInterface {

    private RecyclerView recyclerView;
    private LinearLayout bar,norecordsLL;
    private TextView norecords;
    private AdhoServiceListResponse adhoServiceListResponse;
    private AddBranchResponse addBranchResponse;
    private ServiceTypeResponse serviceTypeResponse;
    private SessionManager manager;
    private AlertDialog alertDialog;
    private ProgressDialog pDialog;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        norecordsLL = (LinearLayout) findViewById(R.id.norecordsLL);
        norecords = (TextView) findViewById(R.id.norecords);
        norecordsLL.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(AdhoServiceList.this, 1));


        manager = new SessionManager();

        FloatingActionButton fab = findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bar = (LinearLayout) findViewById(R.id.progressBar_cyclic);
        bar.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(AdhoServiceList.this, 1));

        Map Adhodetail = new LinkedHashMap(1);
        Map<String, Object> requestBody = new HashMap<>();
        Adhodetail.put("BranchId", manager.getSharedPreferencesValues(AdhoServiceList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(AdhoServiceList.this, "JWT"));
        requestBody.put("AdhocServices", Adhodetail);


        getBranchListDetails(requestBody);
        //getServiceTypeDetails(requestBody);


        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showFilterDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(AdhoServiceList.this)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton a = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton b = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton c = (RadioButton) dialogView.findViewById(R.id.roomsdetails);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(AdhoServiceList.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        title.setText("Filter");
        a.setText("Active");
        b.setText("In Active");
        c.setText("Both");
        view.setText("Apply Filter");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if (selectedId == -1) {
                    Toast.makeText(AdhoServiceList.this, "Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<AdhoServiceListResponse.Response> responses = new ArrayList<>();

                if (radioButton.getText().toString().equals("Active")) {
                    for (AdhoServiceListResponse.Response list : adhoServiceListResponse.getResponse()) {

                        if (list.getAdhocServiceActiveInd().equals("Y")) {
                            responses.add(list);
                        }

                    }
                } else if (radioButton.getText().toString().equals("In Active")) {
                    for (AdhoServiceListResponse.Response list : adhoServiceListResponse.getResponse()) {

                        if (list.getAdhocServiceActiveInd().equals("N")) {
                            responses.add(list);
                        }

                    }
                } else {
                    responses = adhoServiceListResponse.getResponse();
                }

                if(responses.isEmpty()){
                    Toast.makeText(AdhoServiceList.this, "No Records found..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                AdhoServiceListAdapter b = new AdhoServiceListAdapter(AdhoServiceList.this, responses, AdhoServiceList.this);
                recyclerView.setAdapter(b);
                alertDialog.dismiss();

            }
        });
    }

    public void Refereshview() {
        Map Adhodetail = new LinkedHashMap(1);
        Map<String, Object> requestBody = new HashMap<>();
        Adhodetail.put("BranchId", manager.getSharedPreferencesValues(AdhoServiceList.this, "BranchID"));
        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(AdhoServiceList.this, "JWT"));
        requestBody.put("AdhocServices", Adhodetail);
        getBranchListDetails(requestBody);
    }

    private void getServiceTypeDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getServiceTypeDetails(requestBody).enqueue(new Callback<ServiceTypeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {
                try {
                    // if (response.code() == 201 || response.code() == 200) {
                    serviceTypeResponse = response.body();
                    if (serviceTypeResponse == null) {
                        Toast.makeText(AdhoServiceList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (serviceTypeResponse.getResponse().isEmpty()) {
                        Toast.makeText(AdhoServiceList.this, "No ServiceType Records Found..!!", Toast.LENGTH_SHORT).show();
                    }
                    //  }

                } catch (Throwable e) {
                    Toast.makeText(AdhoServiceList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                Toast.makeText(AdhoServiceList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAdhoServiceList(requestBody).enqueue(new Callback<AdhoServiceListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AdhoServiceListResponse> call, Response<AdhoServiceListResponse> response) {
                try {
                    adhoServiceListResponse = new AdhoServiceListResponse();
                    adhoServiceListResponse = response.body();
                    norecordsLL.setVisibility(View.GONE);
                    if (adhoServiceListResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(AdhoServiceList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (adhoServiceListResponse.getMessage().trim().equals("Success")) {
                        bar.setVisibility(View.GONE);
                        if (adhoServiceListResponse.getResponse().isEmpty()) {
                            bar.setVisibility(View.GONE);
                            norecordsLL.setVisibility(View.VISIBLE);
                        }
                        AdhoServiceListAdapter b = new AdhoServiceListAdapter(AdhoServiceList.this, adhoServiceListResponse.getResponse(), AdhoServiceList.this);
                        recyclerView.setAdapter(b);

                    } else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(AdhoServiceList.this, adhoServiceListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    bar.setVisibility(View.GONE);
                    Toast.makeText(AdhoServiceList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AdhoServiceListResponse> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(AdhoServiceList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(AdhoServiceList.this)).inflate(R.layout.adhoservicepopup, viewGroup, false);


        final EditText adhoservice = (EditText) dialogView.findViewById(R.id.adhoservice);
        final EditText adhoservicedesc = (EditText) dialogView.findViewById(R.id.adhoservicedesc);
        final EditText adhoservicenotes = (EditText) dialogView.findViewById(R.id.adhoservicenotes);
        final EditText adhoservicecost = (EditText) dialogView.findViewById(R.id.adhoservicecost);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(AdhoServiceList.this));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (adhoservicedesc.length() == 0 || adhoservicecost.length() == 0) {
                    Toast.makeText(AdhoServiceList.this, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Map<String, Object> requestBody = new HashMap<>();
                Map AdhocServices = new LinkedHashMap(5);
                AdhocServices.put("BranchId", manager.getSharedPreferencesValues(AdhoServiceList.this, "BranchID"));
                AdhocServices.put("AdhocService", adhoservice.getText().toString().trim());
                AdhocServices.put("AdhocServiceDesc", adhoservicedesc.getText().toString().trim());
                AdhocServices.put("Notes", adhoservicenotes.getText().toString().trim());
                AdhocServices.put("CostPerUse", adhoservicecost.getText().toString().trim());

                requestBody.put("Operator", "C");
                requestBody.put("JWT", manager.getSharedPreferencesValues(AdhoServiceList.this, "JWT"));
                requestBody.put("AdhocServices", AdhocServices);
                bar.setVisibility(View.VISIBLE);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(AdhoServiceList.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
            }
        });
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddAdhoService(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(AdhoServiceList.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    bar.setVisibility(View.GONE);
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(AdhoServiceList.this, "Adho Service has been added successfully..!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Refereshview();

                    } else {
                        Toast.makeText(AdhoServiceList.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    bar.setVisibility(View.GONE);
                    Toast.makeText(AdhoServiceList.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                bar.setVisibility(View.GONE);
                Toast.makeText(AdhoServiceList.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMethodCallback(int pos) {

    }

    @Override
    public void onMethodCallbackActivate() {
        Refereshview();
    }
}

