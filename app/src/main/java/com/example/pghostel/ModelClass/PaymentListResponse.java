package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("Amount")
        private String Amount;
        @Expose
        @SerializedName("Status")
        private String Status;
        @Expose
        @SerializedName("PaymentTxnType")
        private String PaymentTxnType;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;
        @Expose
        @SerializedName("PaymentTxnDate")
        private String PaymentTxnDate;
        @Expose
        @SerializedName("PaymentTxnId")
        private String PaymentTxnId;

        public String getAmount() {
            return Amount;
        }

        public String getStatus() {
            return Status;
        }

        public String getPaymentTxnType() {
            return PaymentTxnType;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public String getTenantId() {
            return TenantId;
        }

        public String getPaymentTxnDate() {
            return PaymentTxnDate;
        }

        public String getPaymentTxnId() {
            return PaymentTxnId;
        }
    }
}
