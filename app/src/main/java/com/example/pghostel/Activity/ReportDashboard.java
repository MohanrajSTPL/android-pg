package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.BranchListResponse;
import com.example.pghostel.ModelClass.PhysicalRoomListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportDashboard extends AppCompatActivity {

    private MaterialBetterSpinner reportSpin1;
    private MaterialBetterSpinner reportSpin2;
    private MaterialBetterSpinner reportSpin3;
    private MaterialBetterSpinner reportSpin4;

    private ProgressDialog pDialog;
    private SessionManager manager;
    private int reportSpin1ID = -1;
    private int reportSpin2ID = -1;
    private int reportSpin3ID = -1;
    private BranchListResponse branchListResponse;
    private PhysicalRoomListResponse physicalRoomListResponse;
    private RoomListresponse roomListresponse;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dashboard);

        manager = new SessionManager();

        Objects.requireNonNull(getSupportActionBar()).hide();

        GridLayout gridLayout=(GridLayout)findViewById(R.id.ReportGrid);

        setSingleEvent(gridLayout);
    }
    private void setSingleEvent(GridLayout gridLayout) {
        for(int i = 0; i<gridLayout.getChildCount();i++){
            CardView cardView=(CardView)gridLayout.getChildAt(i);
            final int finalI= i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View view) {
                    if(finalI==0) {
                        Intent i=new Intent(ReportDashboard.this, Reports.class);
                        i.putExtra("repo",0);
                        startActivity(i);
                    }else if(finalI==1) {
                        if(manager.getSharedPreferencesValues(getApplicationContext(), "LoginCheck").equals("BYes")){
                            Intent i=new Intent(ReportDashboard.this, Reports.class);
                            i.putExtra("repo",1);
                            i.putExtra("BID",Integer.parseInt(manager.getSharedPreferencesValues(ReportDashboard.this, "BranchID")));
                            startActivity(i);
                        }else {
                            getReports(1);
                        }
                    }else if(finalI==2) {
                        getReports(2);
                    }else if(finalI==3) {
                        getReports(3);
                    }
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getReports(final int spinvalue){
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(ReportDashboard.this)).inflate(R.layout.reportview, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.getreport);
        reportSpin1 = (MaterialBetterSpinner) dialogView.findViewById(R.id.reportspin1);
        reportSpin2 = (MaterialBetterSpinner) dialogView.findViewById(R.id.reportspin2);
        reportSpin3 = (MaterialBetterSpinner) dialogView.findViewById(R.id.reportspin3);
        reportSpin4 = (MaterialBetterSpinner) dialogView.findViewById(R.id.reportspin4);

        if(spinvalue == 1){
            reportSpin1.setVisibility(View.VISIBLE);
            reportSpin2.setVisibility(View.GONE);
            reportSpin3.setVisibility(View.GONE);
            reportSpin4.setVisibility(View.GONE);
        }else if(spinvalue == 2){
            reportSpin1.setVisibility(View.VISIBLE);
            reportSpin2.setVisibility(View.VISIBLE);
            reportSpin3.setVisibility(View.GONE);
            reportSpin4.setVisibility(View.GONE);
        }else if(spinvalue == 3){
            reportSpin1.setVisibility(View.VISIBLE);
            reportSpin2.setVisibility(View.VISIBLE);
            reportSpin3.setVisibility(View.VISIBLE);
            reportSpin4.setVisibility(View.GONE);
        }

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(ReportDashboard.this));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Map<String, String> requestBody = new HashMap<>();

        requestBody.put("Operator", "R");
        requestBody.put("JWT", manager.getSharedPreferencesValues(ReportDashboard.this, "JWT"));

        pDialog = ProgressDialog.show(ReportDashboard.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

        getBranchListDetails(requestBody);

//        List<String> branchlist = new ArrayList<String>();
//        branchlist.add("Select Branch..!!");
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReportDashboard.this, android.R.layout.simple_dropdown_item_1line, branchlist);
//        reportSpin2.setAdapter(adapter);
//
//        List<String> branchlist1 = new ArrayList<String>();
//        branchlist.add("Select Room Type Id ..!!");
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(ReportDashboard.this, android.R.layout.simple_dropdown_item_1line, branchlist1);
//        reportSpin3.setAdapter(adapter1);

        reportSpin1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                reportSpin1ID = position;

                if(!branchListResponse.getResponse().isEmpty() && spinvalue == 2) {
                    Map<String, String> requestBody = new HashMap<>();
                    requestBody.put("BranchId", String.valueOf(branchListResponse.getResponse().get(reportSpin1ID).getBranchId()));
                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(ReportDashboard.this, "JWT"));

                    pDialog = ProgressDialog.show(ReportDashboard.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");
                    getRoomTypeListDetails(requestBody);
                }
            }
        });
        reportSpin2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                reportSpin2ID = position;
                if(!roomListresponse.getResponse().isEmpty() && spinvalue == 3) {
                    Map<String, Object> requestBody = new HashMap<>();
                    Map Adhodetail = new LinkedHashMap(1);
                    requestBody.put("BranchId", String.valueOf(branchListResponse.getResponse().get(reportSpin1ID).getBranchId()));
                    requestBody.put("Operator", "R");
                    Adhodetail.put("RoomTypeId", roomListresponse.getResponse().get(reportSpin2ID).getRoomTypeId());
                    requestBody.put("JWT", manager.getSharedPreferencesValues(ReportDashboard.this, "JWT"));
                    requestBody.put("Rooms", Adhodetail);

                    pDialog = ProgressDialog.show(ReportDashboard.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");
                    getPhysicalRoomListResponse(requestBody);
                }



            }
        });
        reportSpin3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                reportSpin3ID = position;
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isvalue = false;
                //AppUserDetails.put("TenantId", branchListResponse.getResponse().get(reportSpin1ID).getBranchId());
                //Toast.makeText(ReportDashboard.this, String.valueOf(branchListResponse.getResponse().get(reportSpin1ID).getBranchId()), Toast.LENGTH_SHORT).show();

                Intent i=new Intent(ReportDashboard.this, Reports.class);

                if(spinvalue == 1){
                    if(reportSpin1ID!=-1){
                        isvalue = true;
                        i.putExtra("repo",1);
                        i.putExtra("BID",branchListResponse.getResponse().get(reportSpin1ID).getBranchId());
                    }
                }else if(spinvalue == 2){
                    if(reportSpin1ID!=-1 && reportSpin2ID!=-1){
                        isvalue = true;
                        i.putExtra("repo",2);
                        i.putExtra("BID",branchListResponse.getResponse().get(reportSpin1ID).getBranchId());
                        i.putExtra("RTID",roomListresponse.getResponse().get(reportSpin2ID).getRoomTypeId());
                    }
                }else if(spinvalue == 3){
                    if(reportSpin1ID!=-1 && reportSpin2ID!=-1 && reportSpin3ID!=-1){
                        isvalue = true;
                        i.putExtra("repo",3);
                        i.putExtra("BID",branchListResponse.getResponse().get(reportSpin1ID).getBranchId());
                        i.putExtra("RTID",roomListresponse.getResponse().get(reportSpin2ID).getRoomTypeId());
                        i.putExtra("RID",physicalRoomListResponse.getResponse().get(reportSpin3ID).getRoomId());
                    }
                }

                if(!isvalue){
                    Toast.makeText(ReportDashboard.this, "Selected value are incorrect..!!", Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(i);
                    alertDialog.dismiss();
                }

            }
        });
    }

    private void getBranchListDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getBranchListDetails(requestBody).enqueue(new Callback<BranchListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<BranchListResponse> call, Response<BranchListResponse> response) {
                try {
                    pDialog.dismiss();
                    branchListResponse = new BranchListResponse();
                    branchListResponse = response.body();

                    if (branchListResponse == null) {
                        Toast.makeText(ReportDashboard.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (branchListResponse.getMessage().trim().equals("Success")) {

                        if (branchListResponse.getResponse().isEmpty()) {
                            Toast.makeText(ReportDashboard.this, "Branch list is empty..!!", Toast.LENGTH_SHORT).show();
                        }
                        List<String> branchlist = new ArrayList<String>();
                        if (branchListResponse != null) {
                            for (BranchListResponse.Response list : branchListResponse.getResponse()) {
                                branchlist.add(list.getBranchName());
                            }
                        }
                        if(branchlist.isEmpty()){
                            branchlist.add("Branch is Empty..!!");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReportDashboard.this, android.R.layout.simple_dropdown_item_1line, branchlist);
                        reportSpin1.setAdapter(adapter);

                    } else {

                        Toast.makeText(ReportDashboard.this, branchListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(ReportDashboard.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(ReportDashboard.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getRoomTypeListDetails(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getRoomTypeListDetails(requestBody).enqueue(new Callback<RoomListresponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<RoomListresponse> call, Response<RoomListresponse> response) {
                try {
                    pDialog.dismiss();
                    roomListresponse = new RoomListresponse();
                    roomListresponse = response.body();

                    if (roomListresponse == null) {
                        Toast.makeText(ReportDashboard.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (roomListresponse.getMessage().trim().equals("Success")) {

                        if (roomListresponse.getResponse().isEmpty()) {
                            Toast.makeText(ReportDashboard.this, "Room Type list is empty..!!", Toast.LENGTH_SHORT).show();
                        }
                        List<String> roomtypelist = new ArrayList<String>();
                        if (roomListresponse != null) {
                            for (RoomListresponse.Response list : roomListresponse.getResponse()) {
                                roomtypelist.add(list.getRoomType());
                            }
                        }
                        if(roomtypelist.isEmpty()){
                            roomtypelist.add("Room Type is Empty..!!");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReportDashboard.this, android.R.layout.simple_dropdown_item_1line, roomtypelist);
                        reportSpin2.setAdapter(adapter);

                    } else {

                        Toast.makeText(ReportDashboard.this, branchListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(ReportDashboard.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RoomListresponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(ReportDashboard.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPhysicalRoomListResponse(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListPhysicalRoom(requestBody).enqueue(new Callback<PhysicalRoomListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<PhysicalRoomListResponse> call, Response<PhysicalRoomListResponse> response) {
                try {
                    pDialog.dismiss();
                    physicalRoomListResponse = new PhysicalRoomListResponse();
                    physicalRoomListResponse = response.body();

                    if (physicalRoomListResponse == null) {
                        Toast.makeText(ReportDashboard.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (physicalRoomListResponse.getMessage().trim().equals("Success")) {

                        if (physicalRoomListResponse.getResponse().isEmpty()) {
                            Toast.makeText(ReportDashboard.this, "Room list is empty..!!", Toast.LENGTH_SHORT).show();
                        }
                        List<String> roomtypelist = new ArrayList<String>();
                        if (physicalRoomListResponse != null) {
                            for (PhysicalRoomListResponse.ResponseEntity list : physicalRoomListResponse.getResponse()) {
                                roomtypelist.add(list.getRoomNo());
                            }
                        }
                        if(roomtypelist.isEmpty()){
                            roomtypelist.add("Room is Empty..!!");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReportDashboard.this, android.R.layout.simple_dropdown_item_1line, roomtypelist);
                        reportSpin3.setAdapter(adapter);

                    } else {

                        Toast.makeText(ReportDashboard.this, branchListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(ReportDashboard.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PhysicalRoomListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(ReportDashboard.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}

