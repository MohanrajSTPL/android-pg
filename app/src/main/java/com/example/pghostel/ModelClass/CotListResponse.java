package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CotListResponse {


    @Expose
    @SerializedName("Response")
    private List<ResponseBean> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<ResponseBean> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class ResponseBean {
        @Expose
        @SerializedName("IsOccupied")
        private String IsOccupied;
        @Expose
        @SerializedName("TenantNo")
        private String TenantNo;
        @Expose
        @SerializedName("TenantName")
        private String TenantName;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;
        @Expose
        @SerializedName("CotActiveInd")
        private String CotActiveInd;
        @Expose
        @SerializedName("CostPerCotPerMonth")
        private String CostPerCotPerMonth;
        @Expose
        @SerializedName("CostPerCotPerDay")
        private String CostPerCotPerDay;
        @Expose
        @SerializedName("NoOfCots")
        private String NoOfCots;
        @Expose
        @SerializedName("RoomTypeDesc")
        private String RoomTypeDesc;
        @Expose
        @SerializedName("RoomType")
        private String RoomType;
        @Expose
        @SerializedName("Area")
        private String Area;
        @Expose
        @SerializedName("Block")
        private String Block;
        @Expose
        @SerializedName("Floor")
        private String Floor;
        @Expose
        @SerializedName("RoomNo")
        private String RoomNo;
        @Expose
        @SerializedName("CotTypeDesc")
        private String CotTypeDesc;
        @Expose
        @SerializedName("CotType")
        private String CotType;
        @Expose
        @SerializedName("Notes")
        private String Notes;
        @Expose
        @SerializedName("Discount")
        private String Discount;
        @Expose
        @SerializedName("CotNo")
        private String CotNo;
        @Expose
        @SerializedName("RoomTypeId")
        private String RoomTypeId;
        @Expose
        @SerializedName("CotTypeId")
        private String CotTypeId;
        @Expose
        @SerializedName("RoomId")
        private String RoomId;
        @Expose
        @SerializedName("CotId")
        private String CotId;

        public String getIsOccupied() {
            return IsOccupied;
        }

        public String getTenantNo() {
            return TenantNo;
        }

        public String getTenantName() {
            return TenantName;
        }

        public String getTenantId() {
            return TenantId;
        }

        public String getCotActiveInd() {
            return CotActiveInd;
        }

        public String getCostPerCotPerMonth() {
            return CostPerCotPerMonth;
        }

        public String getCostPerCotPerDay() {
            return CostPerCotPerDay;
        }

        public String getRoomTypeDesc() {
            return RoomTypeDesc;
        }

        public String getNoOfCots() {
            return NoOfCots;
        }

        public String getRoomType() {
            return RoomType;
        }

        public String getArea() {
            return Area;
        }

        public String getBlock() {
            return Block;
        }

        public String getFloor() {
            return Floor;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public String getCotTypeDesc() {
            return CotTypeDesc;
        }

        public String getCotType() {
            return CotType;
        }

        public String getNotes() {
            return Notes;
        }

        public String getDiscount() {
            return Discount;
        }

        public String getCotNo() {
            return CotNo;
        }

        public String getRoomTypeId() {
            return RoomTypeId;
        }

        public String getCotTypeId() {
            return CotTypeId;
        }

        public String getRoomId() {
            return RoomId;
        }

        public String getCotId() {
            return CotId;
        }
    }
}
