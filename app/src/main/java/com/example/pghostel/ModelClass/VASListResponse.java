package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VASListResponse {
    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("VASActiveInd")
        private String VASActiveInd;
        @Expose
        @SerializedName("CostPerDay")
        private String CostPerDay;
        @Expose
        @SerializedName("CostPerMonth")
        private String CostPerMonth;
        @Expose
        @SerializedName("VASDesc")
        private String VASDesc;
        @Expose
        @SerializedName("VAS")
        private String VAS;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("VASId")
        private String VASId;

        public String getVASActiveInd() {
            return VASActiveInd;
        }

        public String getCostPerDay() {
            return CostPerDay;
        }

        public String getCostPerMonth() {
            return CostPerMonth;
        }

        public String getVASDesc() {
            return VASDesc;
        }

        public String getVAS() {
            return VAS;
        }

        public String getBranchId() {
            return BranchId;
        }

        public String getVASId() {
            return VASId;
        }
    }
}
