package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceTypeResponse {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private List<Response> response = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("SERVICE_TYPE_ID")
        @Expose
        private String sERVICETYPEID;
        @SerializedName("SERVICE_TYPE")
        @Expose
        private String sERVICETYPE;
        @SerializedName("SERVICE_TYPE_DESC")
        @Expose
        private String sERVICETYPEDESC;

        public String getSERVICETYPEID() {
            return sERVICETYPEID;
        }

        public void setSERVICETYPEID(String sERVICETYPEID) {
            this.sERVICETYPEID = sERVICETYPEID;
        }

        public String getSERVICETYPE() {
            return sERVICETYPE;
        }

        public void setSERVICETYPE(String sERVICETYPE) {
            this.sERVICETYPE = sERVICETYPE;
        }

        public String getSERVICETYPEDESC() {
            return sERVICETYPEDESC;
        }

        public void setSERVICETYPEDESC(String sERVICETYPEDESC) {
            this.sERVICETYPEDESC = sERVICETYPEDESC;
        }
    }

}
