package com.example.pghostel.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TenantCreateActivity extends AppCompatActivity {

    EditText Branchname;
    EditText picker;
    EditText Address,officeAddress;
    EditText City,officeCity;
    EditText State;
    EditText Country;
    EditText Pincode;
    EditText MobileNo;
    EditText profession;
    EditText officeState;
    EditText officeCountry;
    EditText officePincode;
    EditText homephoneno;
    EditText officeMobileNo;
    EditText EmailID;
    RadioButton male,female;
    RadioGroup gender;
    Button update;
    private SessionManager manager;
    private BranchDetailResponse branchDetailResponse;
    private AddBranchResponse addBranchResponse;
    int status,BranchID;
    String T_id;
    private TenantListResponse tenantListResponse;
    private ProgressDialog pDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant_create);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        Branchname = (EditText)findViewById(R.id.branch_name);
        picker = (EditText) findViewById(R.id.dob);
        Address = (EditText)findViewById(R.id.address);
        City = (EditText)findViewById(R.id.city);
        State = (EditText)findViewById(R.id.state);
        Country = (EditText)findViewById(R.id.country);
        Pincode = (EditText)findViewById(R.id.pincode);
        MobileNo = (EditText)findViewById(R.id.mobileno);
        profession = (EditText)findViewById(R.id.profession);
        officeAddress = (EditText)findViewById(R.id.officeaddress);
        officeCity = (EditText)findViewById(R.id.officecity);
        officeState = (EditText)findViewById(R.id.officestate);
        officeCountry = (EditText)findViewById(R.id.officecountry);
        officePincode = (EditText)findViewById(R.id.officepincode);
        officeMobileNo = (EditText)findViewById(R.id.phoneno);
        homephoneno = (EditText)findViewById(R.id.homephoneno);
        EmailID = (EditText)findViewById(R.id.emailID);
        male = (RadioButton)findViewById(R.id.radio_gender_male);
        female = (RadioButton)findViewById(R.id.radio_gender_female);
        gender = (RadioGroup)findViewById(R.id.radio_group_gender);
        update = (Button)findViewById(R.id.updatebranchdetails);


        status = getIntent().getIntExtra("status", 0);
        BranchID = getIntent().getIntExtra("BranchID", 0);
        T_id = getIntent().getStringExtra("ID");

        if(status==0) {
            update.setText("Add");
        }
        else {
            update.setText("Update");

            Map<String, Object> requestBody = new HashMap<>();
            final Map TenantDetail = new LinkedHashMap(1);
           // TenantDetail.put("TenantMobileNo", "9630852740");
            TenantDetail.put("BranchId", BranchID);
            requestBody.put("Operator", "R");
            requestBody.put("JWT", manager.getSharedPreferencesValues(TenantCreateActivity.this, "JWT"));
            requestBody.put("TenantDetails", TenantDetail);
            getTenantItemDetails(requestBody);
            pDialog = ProgressDialog.show(TenantCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

        }


        picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(TenantCreateActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String dayConverted = ""+dayOfMonth;
                                if(dayOfMonth<10){
                                    dayConverted = "0"+dayConverted;
                                }

                                String monthConverted = ""+(monthOfYear + 1);
                                if((monthOfYear + 1)<10){
                                    monthConverted = "0"+monthConverted;
                                }

                                picker.setText(year + "-" + monthConverted + "-" + dayConverted);
                                //picker.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = gender.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if(selectedId==-1){
                    Toast.makeText(TenantCreateActivity.this,"Plese select the Gender.!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String radio_value = "";
                if(radioButton.getText().toString().equals("Men"))
                    radio_value = "MALE";
                else
                    radio_value = "FEMALE";

                Map<String, Object> requestBody = new HashMap<>();
                Map TenantDetails = new LinkedHashMap(21);
//                if(status==1) {
//                    TenantDetails.put("BranchId", String.valueOf(B_id));
//                }
//                TenantDetails.put("TenantId", Branchname.getText().toString().trim());
                TenantDetails.put("BranchId", BranchID);
                TenantDetails.put("TenantName", Branchname.getText().toString().trim());
                TenantDetails.put("TenantGender", radio_value);
                TenantDetails.put("TenantDOB", picker.getText().toString().trim());
                TenantDetails.put("TenantProfession", profession.getText().toString().trim());
                TenantDetails.put("TenantHomeAddress", Address.getText().toString());
                TenantDetails.put("TenantHomeCity", City.getText().toString());
                TenantDetails.put("TenantHomeState", State.getText().toString());
                TenantDetails.put("TenantHomeCountry", Country.getText().toString());
                TenantDetails.put("TenantHomePincode", Pincode.getText().toString());
                TenantDetails.put("TenantEmailId", EmailID.getText().toString());
                TenantDetails.put("TenantMobileNo", MobileNo.getText().toString());
                TenantDetails.put("TenantHomePhone", homephoneno.getText().toString());
                TenantDetails.put("TenantOfficeAddress", officeAddress.getText().toString());
                TenantDetails.put("TenantOfficeCity", officeCity.getText().toString());
                TenantDetails.put("TenantOfficeState", officeState.getText().toString());
                TenantDetails.put("TenantOfficeCountry", officeCountry.getText().toString());
                TenantDetails.put("TenantOfficePincode", officePincode.getText().toString());
                TenantDetails.put("TenantOfficePhone", officeMobileNo.getText().toString());
                TenantDetails.put("TenantActiveInd", "Y");
                if(status==1) {
                    requestBody.put("Operator", "U");
                    TenantDetails.put("TenantId", T_id);
                }else {
                    requestBody.put("Operator", "C");
                }

                requestBody.put("JWT", manager.getSharedPreferencesValues(TenantCreateActivity.this, "JWT"));
                requestBody.put("TenantDetails", TenantDetails);

                if(status==1) {
                    pDialog = ProgressDialog.show(TenantCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
                    updateTenantDetails(requestBody,"Updated");
                }else {
                    pDialog = ProgressDialog.show(TenantCreateActivity.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
                    updateTenantDetails(requestBody,"Added");
                }

            }
        });
    }

    private void getTenantItemDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListTenant(requestBody).enqueue(new Callback<TenantListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<TenantListResponse> call, Response<TenantListResponse> response) {
                try {
                    // if (response.code() == 201) {
                    pDialog.dismiss();
                    tenantListResponse = new TenantListResponse();
                    tenantListResponse = response.body();

                    if (tenantListResponse == null) {
                        Toast.makeText(TenantCreateActivity.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (tenantListResponse.getMessage().trim().equals("Success")) {

                        for (TenantListResponse.Response list : tenantListResponse.getResponse()) {
                            Branchname.setText(list.getTenantName());
                            City.setText(list.getTenantHomeCity());
                            picker.setText(list.getTenantDOB());
                            Address.setText(list.getTenantHomeAddress());
                            State.setText(list.getTenantHomeState());
                            Country.setText(list.getTenantHomeCountry());
                            Pincode.setText(list.getTenantHomePincode());
                            MobileNo.setText(list.getTenantMobileNo());
                            profession.setText(list.getTenantProfession());
                            EmailID.setText(list.getTenantEmailId());
                            homephoneno.setText(list.getTenantHomePhone());
                            officeCity.setText(list.getTenantOfficeCity());
                            officeAddress.setText(list.getTenantOfficeAddress());
                            officeState.setText(list.getTenantOfficeState());
                            officeCountry.setText(list.getTenantOfficeCountry());
                            officePincode.setText(list.getTenantOfficePincode());
                            officeMobileNo.setText(list.getTenantOfficePhone());
                            if(list.getTenantGender().equals("MALE")){
                                male.setChecked(true);
                            }else {
                                female.setChecked(true);
                            }
                        }
                        pDialog.dismiss();
                    } else {
                        Toast.makeText(TenantCreateActivity.this, tenantListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(TenantCreateActivity.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TenantListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(TenantCreateActivity.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateTenantDetails(Map<String, Object> requestBody, final String s) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddTenant(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(TenantCreateActivity.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(TenantCreateActivity.this, "Tenant has been "+s+" successfully..!!", Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(TenantCreateActivity.this, TenantList.class);
                        i.putExtra("ID",BranchID);
                        startActivity(i);
                    } else {
                        if(addBranchResponse.getError()!=null && !addBranchResponse.getError().equals("")) {
                            Toast.makeText(TenantCreateActivity.this, addBranchResponse.getError(), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(TenantCreateActivity.this, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(TenantCreateActivity.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(TenantCreateActivity.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
