package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.TenantCreateActivity;
import com.example.pghostel.Activity.TenantList;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TenantListAdapter extends RecyclerView.Adapter<TenantListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<TenantListResponse.Response> responses;
    private AddBranchResponse addBranchResponse;

    //getting the context and product list with constructor
    public TenantListAdapter(Context mCtx, List<TenantListResponse.Response> rommfacilitylist) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = rommfacilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final TenantListResponse.Response tenantlistdetails = responses.get(position);

        //binding the data with the viewholder views
        holder.facilityname.setText(tenantlistdetails.getTenantName());
        holder.facilityPrice.setText(tenantlistdetails.getTenantMobileNo());
        holder.facilityNotes.setText("Tenant No: "+tenantlistdetails.getTenantNo());


        if (tenantlistdetails.getTenantActiveInd().equals("N")) {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.facilityStatus.setText("IN ACTIVE");
        } else {
            holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.facilityStatus.setText("ACTIVE");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mCtx, TenantCreateActivity.class);
                i.putExtra("status",1);
                i.putExtra("ID",responses.get(position).getTenantId());
                i.putExtra("BranchID",Integer.parseInt(responses.get(position).getBranchId()));
                mCtx.startActivity(i);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if(!responses.get(position).getTenantActiveInd().equals("N")) {
                    AlertDialog diaBox = AskOption(responses.get(position));
                    diaBox.show();
                }

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;


        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
            // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }

    private AlertDialog AskOption(final TenantListResponse.Response branchlist) {

        return new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to Delete this Tenant?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        Map<String, Object> requestBody = new HashMap<>();

                        Map CotTypeDetails = new LinkedHashMap(1);
                        CotTypeDetails.put("TenantId", branchlist.getTenantId());
                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("TenantDetails", CotTypeDetails);


                        DeleteTenant(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
    }


    private void DeleteTenant(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddTenant(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().trim().equals("Success")) {
                        Toast.makeText(mCtx, "Tenant has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((TenantList) mCtx).Refereshview();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}

