package com.example.pghostel.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TenantListResponse {


    @Expose
    @SerializedName("Response")
    private List<Response> Response;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("ResponseCode")
    private int ResponseCode;

    public List<Response> getResponse() {
        return Response;
    }

    public String getMessage() {
        return Message;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public class Response {
        @Expose
        @SerializedName("RoomType")
        private String RoomType;
        @Expose
        @SerializedName("RoomNo")
        private String RoomNo;
        @Expose
        @SerializedName("CotNo")
        private String CotNo;
        @Expose
        @SerializedName("IsOccupied")
        private String IsOccupied;
        @Expose
        @SerializedName("TenantActiveInd")
        private String TenantActiveInd;
        @Expose
        @SerializedName("TenantOfficePhone")
        private String TenantOfficePhone;
        @Expose
        @SerializedName("TenantOfficePincode")
        private String TenantOfficePincode;
        @Expose
        @SerializedName("TenantOfficeCountry")
        private String TenantOfficeCountry;
        @Expose
        @SerializedName("TenantOfficeState")
        private String TenantOfficeState;
        @Expose
        @SerializedName("TenantOfficeCity")
        private String TenantOfficeCity;
        @Expose
        @SerializedName("TenantOfficeAddress")
        private String TenantOfficeAddress;
        @Expose
        @SerializedName("TenantHomePhone")
        private String TenantHomePhone;
        @Expose
        @SerializedName("TenantHomePincode")
        private String TenantHomePincode;
        @Expose
        @SerializedName("TenantHomeCountry")
        private String TenantHomeCountry;
        @Expose
        @SerializedName("TenantHomeState")
        private String TenantHomeState;
        @Expose
        @SerializedName("TenantHomeCity")
        private String TenantHomeCity;
        @Expose
        @SerializedName("TenantHomeAddress")
        private String TenantHomeAddress;
        @Expose
        @SerializedName("TenantProfession")
        private String TenantProfession;
        @Expose
        @SerializedName("TenantGender")
        private String TenantGender;
        @Expose
        @SerializedName("TenantDOB")
        private String TenantDOB;
        @Expose
        @SerializedName("TenantEmailId")
        private String TenantEmailId;
        @Expose
        @SerializedName("TenantMobileNo")
        private String TenantMobileNo;
        @Expose
        @SerializedName("TenantName")
        private String TenantName;
        @Expose
        @SerializedName("TenantNo")
        private String TenantNo;
        @Expose
        @SerializedName("TenantId")
        private String TenantId;
        @Expose
        @SerializedName("BranchId")
        private String BranchId;
        @Expose
        @SerializedName("BranchName")
        private String BranchName;

        public String getRoomType() {
            return RoomType;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public String getCotNo() {
            return CotNo;
        }

        public String getIsOccupied() {
            return IsOccupied;
        }

        public String getTenantActiveInd() {
            return TenantActiveInd;
        }

        public String getTenantOfficePhone() {
            return TenantOfficePhone;
        }

        public String getTenantOfficePincode() {
            return TenantOfficePincode;
        }

        public String getTenantOfficeCountry() {
            return TenantOfficeCountry;
        }

        public String getTenantOfficeState() {
            return TenantOfficeState;
        }

        public String getTenantOfficeCity() {
            return TenantOfficeCity;
        }

        public String getTenantOfficeAddress() {
            return TenantOfficeAddress;
        }

        public String getTenantHomePhone() {
            return TenantHomePhone;
        }

        public String getTenantHomePincode() {
            return TenantHomePincode;
        }

        public String getTenantHomeCountry() {
            return TenantHomeCountry;
        }

        public String getTenantHomeState() {
            return TenantHomeState;
        }

        public String getTenantHomeCity() {
            return TenantHomeCity;
        }

        public String getTenantHomeAddress() {
            return TenantHomeAddress;
        }

        public String getTenantProfession() {
            return TenantProfession;
        }

        public String getTenantGender() {
            return TenantGender;
        }

        public String getTenantDOB() {
            return TenantDOB;
        }

        public String getTenantEmailId() {
            return TenantEmailId;
        }

        public String getTenantMobileNo() {
            return TenantMobileNo;
        }

        public String getTenantName() {
            return TenantName;
        }

        public String getTenantNo() {
            return TenantNo;
        }

        public String getTenantId() {
            return TenantId;
        }
        public String getBranchId() {
            return BranchId;
        }

        public String getBranchName() {
            return BranchName;
        }
    }
}
