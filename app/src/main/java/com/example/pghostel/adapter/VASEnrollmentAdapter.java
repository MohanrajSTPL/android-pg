package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.BranchDetails;
import com.example.pghostel.Activity.CotTypesList;
import com.example.pghostel.Activity.Facility;
import com.example.pghostel.Activity.PhysicalRoomList;
import com.example.pghostel.Activity.RoomFacility;
import com.example.pghostel.Activity.Rooms;
import com.example.pghostel.Activity.TenantList;
import com.example.pghostel.Activity.VASEnrollmentList;
import com.example.pghostel.Activity.cotEnrollmentAllocation;
import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.CotEnrollmentListResponse;
import com.example.pghostel.ModelClass.RoomListresponse;
import com.example.pghostel.ModelClass.VASEnrollmentListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VASEnrollmentAdapter extends RecyclerView.Adapter<VASEnrollmentAdapter.BranchlistViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AdapterInterface buttonListener;
    //we are storing all the products in a list
    private List<VASEnrollmentListResponse.Response> responses;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private AddBranchResponse addBranchResponse;
    private AlertDialog alertDialog;

    public VASEnrollmentAdapter(Context mCtx, List<VASEnrollmentListResponse.Response> roomlist, AdapterInterface fragment) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.responses = roomlist;
        this.buttonListener = fragment;

    }

    @Override
    public VASEnrollmentAdapter.BranchlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childbranchlist, null);
        return new VASEnrollmentAdapter.BranchlistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VASEnrollmentAdapter.BranchlistViewHolder holder, final int position) {
        //getting the product of the specified position
        final VASEnrollmentListResponse.Response roomlist = responses.get(position);

        //binding the data with the viewholder views
        holder.branchname.setText(roomlist.getTenantName());
        holder.Servicetype.setText(roomlist.getVAS() +" & Date"+roomlist.getStartDate());
        holder.branchgender.setText("Plan: "+roomlist.getBillPlan());

//        if (roomlist.g().equals("N")) {
//            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
//            holder.branchStatus.setText("IN ACTIVE");
//        } else {
//            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
//            holder.branchStatus.setText("ACTIVE");
//        }
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onClick(View v) {
//                if (responses.get(position).getRoomTypeActiveInd().equals("Y")) {
//                    //showCustomPopup(responses.get(position));
//                    Intent i=new Intent(mCtx, PhysicalRoomList.class);
//                    i.putExtra("ID",Integer.parseInt(responses.get(position).getBranchId()));
//                    i.putExtra("RTID",Integer.parseInt(responses.get(position).getRoomTypeId()));
//                    mCtx.startActivity(i);
//                } else {
//                    AlertDialog diaBox = AskOption(responses.get(position));
//                    diaBox.show();
//                }
//            }
//        });
//
//        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                if(!responses.get(position).getRoomTypeActiveInd().equals("N")) {
//                    //buttonListener.onMethodCallback(position);
//                    AlertDialog diaBox = Option(responses.get(position));
//                    diaBox.show();
//                }
//                return true;
//            }
//        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mCtx, cotEnrollmentAllocation.class);
                i.putExtra("VID",responses.get(position).getVASEnrollmentId());
                mCtx.startActivity(i);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                    AlertDialog diaBox = Option(responses.get(position));
                    diaBox.show();

                return true;
            }
        });

    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class BranchlistViewHolder extends RecyclerView.ViewHolder {

        TextView branchname;
        TextView Servicetype;
        TextView branchgender;
        TextView branchStatus;


        public BranchlistViewHolder(View itemView) {
            super(itemView);

            branchname = itemView.findViewById(R.id.branchname);
            Servicetype = itemView.findViewById(R.id.Servicetype);
            branchgender = itemView.findViewById(R.id.branchgender);
            branchStatus = itemView.findViewById(R.id.branchStatus);

        }
    }

    private AlertDialog Option(final VASEnrollmentListResponse.Response branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure want to delete this Cot Enrollment?")
                //.setMessage("Are you sure want to Delete this Room Type?")
                .setIcon(R.drawable.ic_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();

                        Map RoomDetails = new LinkedHashMap(1);
                        RoomDetails.put("VASEnrollmentId", branchlist.getVASEnrollmentId());
                        requestBody.put("Operator", "D");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("VASEnrollment", RoomDetails);

                        DeleteBranchDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Deleting..</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    private void DeleteBranchDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddVASEnrollment(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        pDialog.dismiss();
                        Toast.makeText(mCtx, "VAS Enrollment has been deleted successfully..!!", Toast.LENGTH_SHORT).show();
                        ((VASEnrollmentList) mCtx).Refereshview();


                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private AlertDialog AskOption(final RoomListresponse.Response branchlist) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Activate")
                .setMessage("Are you sure want to Activate this Room?")


                .setPositiveButton("Activate", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        Map RoomDetails = new LinkedHashMap(6);
                        requestBody.put("BranchId", branchlist.getBranchId());
                        RoomDetails.put("RoomTypeId", branchlist.getRoomTypeId());
                        RoomDetails.put("RoomType", branchlist.getRoomType());
                        RoomDetails.put("RoomTypeDesc", branchlist.getRoomTypeDesc());
                        RoomDetails.put("NoOfCots", branchlist.getNoOfCosts());
                        RoomDetails.put("CostPerCotPerMonth", branchlist.getCostPerCotsPerMonth());
                        RoomDetails.put("CostPerCotPerDay", branchlist.getCostPerCotsPerDay());
                        RoomDetails.put("RoomTypeActiveInd", "Y");
                        requestBody.put("Operator", "U");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("RoomTypes", RoomDetails);
                        AddBranchListDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Activating...</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddRoomTypeDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(mCtx, "Room Type has been Updated successfully..!!", Toast.LENGTH_SHORT).show();
                        buttonListener.onMethodCallbackActivate();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showViewDialog(final RoomListresponse.Response branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.viewlist, viewGroup, false);


        Button view = (Button) dialogView.findViewById(R.id.buttonview);
        final RadioGroup listofviews = (RadioGroup) dialogView.findViewById(R.id.listofviews);
        final RadioButton roomsdetails = (RadioButton) dialogView.findViewById(R.id.roomsdetails);
        final RadioButton facilitydetails = (RadioButton) dialogView.findViewById(R.id.facilitydetails);
        final RadioButton roomdetails = (RadioButton) dialogView.findViewById(R.id.branchdetails);
        final RadioButton room = (RadioButton) dialogView.findViewById(R.id.room);

        room.setVisibility(View.VISIBLE);
        room.setText("Room List");
        roomsdetails.setText("Cot Type");
        roomdetails.setText("Room Type Details");
        facilitydetails.setText("Room Type Facility");
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = listofviews.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
                if(selectedId==-1){
                    Toast.makeText(mCtx,"Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(radioButton.getText().toString().equals("Room Type Details")){
                    showCustomDialog(branchlist);
                }else if(radioButton.getText().toString().equals("Room Type Facility")){
                    Intent i=new Intent(mCtx, RoomFacility.class);
                    i.putExtra("ID",Integer.parseInt(branchlist.getRoomTypeId()));
                    mCtx.startActivity(i);
                }else if(radioButton.getText().toString().equals("Cot Type")){
                    Intent i=new Intent(mCtx, CotTypesList.class);
                    i.putExtra("ID",Integer.parseInt(branchlist.getBranchId()));
                    mCtx.startActivity(i);
                }else if(radioButton.getText().toString().equals("Room List")){
                    Intent i=new Intent(mCtx, PhysicalRoomList.class);
                    i.putExtra("ID",Integer.parseInt(branchlist.getBranchId()));
                    i.putExtra("RTID",Integer.parseInt(branchlist.getRoomTypeId()));
                    mCtx.startActivity(i);
                }

                alertDialog.dismiss();

            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomPopup(final RoomListresponse.Response branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.viewbranchlist, viewGroup, false);


        Button branchBtn = (Button) dialogView.findViewById(R.id.branchdetailBtn);
        Button facilityBtn = (Button) dialogView.findViewById(R.id.facilityBtn);
        Button roomBtn = (Button) dialogView.findViewById(R.id.roomBtn);
        Button tenant = (Button) dialogView.findViewById(R.id.tenantBtn);

        branchBtn.setText("Room Type Details");
        facilityBtn.setText("Room Type Facility");
        roomBtn.setText("Cot Type");
        tenant.setText("Room List");
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        branchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomDialog(branchlist);
                alertDialog.dismiss();
            }
        });
        facilityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, RoomFacility.class);
                i.putExtra("ID",Integer.parseInt(branchlist.getRoomTypeId()));
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });
        roomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, CotTypesList.class);
                i.putExtra("ID",Integer.parseInt(branchlist.getBranchId()));
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });

        tenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, PhysicalRoomList.class);
                i.putExtra("ID",Integer.parseInt(branchlist.getBranchId()));
                i.putExtra("RTID",Integer.parseInt(branchlist.getRoomTypeId()));
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final RoomListresponse.Response branchlist) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addroompopup, viewGroup, false);


        final TextView roomtittle = (TextView) dialogView.findViewById(R.id.roomtittle);
        final TextView roomnametv = (TextView) dialogView.findViewById(R.id.roomnametv);
        final EditText name = (EditText) dialogView.findViewById(R.id.roomname);
        final EditText cot = (EditText) dialogView.findViewById(R.id.Noofcot);
        final EditText cost = (EditText) dialogView.findViewById(R.id.cost);
        final EditText costpermonth = (EditText) dialogView.findViewById(R.id.costpermonth);
        final EditText desc = (EditText) dialogView.findViewById(R.id.note);

        Button buttonadd = (Button) dialogView.findViewById(R.id.buttonadd);

        roomnametv.setText("Room Type");
        roomtittle.setText("Room Type");
        buttonadd.setText("Update");
        desc.setText(branchlist.getRoomTypeDesc());
        cost.setText(branchlist.getCostPerCotsPerDay());
        costpermonth.setText(branchlist.getCostPerCotsPerMonth());
        cot.setText(branchlist.getNoOfCosts());
        name.setText(branchlist.getRoomType());
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.length() == 0 || cot.length() == 0 || cost.length() == 0) {
                    Toast.makeText(mCtx, "All fields are required..!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map RoomDetails = new LinkedHashMap(5);
                RoomDetails.put("RoomType", name.getText().toString().trim());
                RoomDetails.put("RoomTypeId", branchlist.getRoomTypeId());
                RoomDetails.put("RoomTypeDesc", desc.getText().toString().trim());
                RoomDetails.put("NoOfCots", cot.getText().toString().trim());
                RoomDetails.put("CostPerCotPerMonth", costpermonth.getText().toString().trim());
                RoomDetails.put("CostPerCotPerDay", cost.getText().toString().trim());
                RoomDetails.put("RoomTypeActiveInd", "Y");

                requestBody.put("BranchId",branchlist.getBranchId());
                requestBody.put("Operator", "U");
                requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                requestBody.put("RoomTypes", RoomDetails);
                AddBranchListDetails(requestBody);
                alertDialog.dismiss();
                pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Updating...</font></b>"), "Please wait ...");
            }
        });


    }
}
