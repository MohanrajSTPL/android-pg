package com.example.pghostel.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.CotEnrollmentListResponse;
import com.example.pghostel.ModelClass.CotListResponse;
import com.example.pghostel.ModelClass.PhysicalRoomListResponse;
import com.example.pghostel.ModelClass.TenantListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class cotEnrollmentAllocation extends AppCompatActivity {

    private MaterialBetterSpinner tenant;
    private MaterialBetterSpinner RoomList;
    private MaterialBetterSpinner CotList;
    private Spinner BillPlan;
    private EditText startDate;
    private Button CotAllocation;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private CotListResponse cotListResponse;
    List<CotListResponse.ResponseBean> getIsOccupiedlist = new ArrayList<CotListResponse.ResponseBean>();
    private PhysicalRoomListResponse physicalRoomListResponse;
    private TenantListResponse tenantListResponse;
    private CotEnrollmentListResponse cotEnrollmentListResponse;
    private int tenantID, cotID;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String CID="",planBill = "";
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(cotEnrollmentAllocation.this,cotEnrollmentList.class));
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(cotEnrollmentAllocation.this,cotEnrollmentList.class));
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotenrollment);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        manager = new SessionManager();

        tenant = (MaterialBetterSpinner) findViewById(R.id.material_spinner4);
        RoomList = (MaterialBetterSpinner) findViewById(R.id.material_spinner5);
        CotList = (MaterialBetterSpinner) findViewById(R.id.material_spinner3);
        BillPlan = (Spinner) findViewById(R.id.material_spinner6);
        startDate = (EditText) findViewById(R.id.password);
        CotAllocation = (Button) findViewById(R.id.adduser);

        CID = getIntent().getStringExtra("CID");

        if(Objects.equals(CID, "")) {
            CotAllocation.setText("Add Cot Allocation");

            pDialog = ProgressDialog.show(cotEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

            Map<String, Object> requestBody = new HashMap<>();

            requestBody.put("BranchId", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "BranchID"));
            requestBody.put("Operator", "R");
            requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));


            getRoomlist(requestBody);
        }
        else {
            CotAllocation.setText("Update Cot Allocation");

            Map<String, Object> requestBody = new HashMap<>();
            Map RoomDetails = new LinkedHashMap(1);
            RoomDetails.put("CotEnrollmentId", CID);
            requestBody.put("Operator", "R");
            requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));
            requestBody.put("CotEnrollment", RoomDetails);

            pDialog = ProgressDialog.show(cotEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");

            getcotEnrollmentlist(requestBody);
        }


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(cotEnrollmentAllocation.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String dayConverted = ""+dayOfMonth;
                                if(dayOfMonth<10){
                                    dayConverted = "0"+dayConverted;
                                }

                                String monthConverted = ""+(monthOfYear + 1);
                                if((monthOfYear + 1)<10){
                                    monthConverted = "0"+monthConverted;
                                }

                                startDate.setText(year + "-" + monthConverted + "-" + dayConverted);
                                //picker.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        tenant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                tenantID = position;
            }
        });
        RoomList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Map<String, String> requestBody = new HashMap<>();
                requestBody.put("RoomId",physicalRoomListResponse.getResponse().get(position).getRoomId());
                requestBody.put("Operator", "R");
                requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));
                pDialog = ProgressDialog.show(cotEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Getting Details..</font></b>"), "Please wait ...");
                getCotList(requestBody);
            }
        });
        List<String> cotempty = new ArrayList<String>();
        cotempty.add("Select Room First..!!");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(cotEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, cotempty);
        CotList.setAdapter(adapter);
        CotList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                cotID = position;
            }
        });
        final List<String> billplanlist = new ArrayList<String>();
        billplanlist.add("Daily");
        billplanlist.add("Weekly");
        billplanlist.add("Monthly");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(cotEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, billplanlist);
        BillPlan.setAdapter(adapter1);
       // BillPlan.setSelection(1);

        BillPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                planBill =  billplanlist.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        BillPlan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                planBill = adapterView.getItemAtPosition(position).toString();
//
//            }
//        });


        CotAllocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(planBill.equals("")){
                    Toast.makeText(cotEnrollmentAllocation.this, "", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String, Object> requestBody = new HashMap<>();
                Map AppUserDetails = new LinkedHashMap(3);
                AppUserDetails.put("TenantId", tenantListResponse.getResponse().get(tenantID).getTenantId());
                AppUserDetails.put("CotId", getIsOccupiedlist.get(cotID).getCotId());
                AppUserDetails.put("StartDate", startDate.getText().toString().trim());
                AppUserDetails.put("BillPlan", "Monthly");
                //AppUserDetails.put("BillPlan", planBill);
                AppUserDetails.put("CotEnrollmentActiveInd", "Y");
                requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));



                if(Objects.equals(CID, "")) {
                    requestBody.put("Operator", "C");
                }else {
                    requestBody.put("Operator", "U");
                    AppUserDetails.put("CotEnrollmentId", CID);
                }
                requestBody.put("CotEnrollment", AppUserDetails);

                if(Objects.equals(CID, "")) {
                    pDialog = ProgressDialog.show(cotEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Adding..</font></b>"), "Please wait ...");
                    AllocatingCot(requestBody,"Added");
                }else {
                    pDialog = ProgressDialog.show(cotEnrollmentAllocation.this, Html.fromHtml("<b><font color='#ff8f61'>Updating..</font></b>"), "Please wait ...");
                    AllocatingCot(requestBody,"Updated");
                }
            }
        });
    }

    private void getcotEnrollmentlist(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getCotEnrollmentList(requestBody).enqueue(new Callback<CotEnrollmentListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<CotEnrollmentListResponse> call, Response<CotEnrollmentListResponse> response) {
                try {

                    cotEnrollmentListResponse = new CotEnrollmentListResponse();
                    cotEnrollmentListResponse = response.body();
                    if (cotEnrollmentListResponse == null) {
                        Toast.makeText(cotEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (cotEnrollmentListResponse.getMessage().trim().equals("Success")) {
                        startDate.setText(cotEnrollmentListResponse.getResponse().get(0).getStartDate());
                        Map<String, Object> requestBody = new HashMap<>();

                        requestBody.put("BranchId", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "BranchID"));
                        requestBody.put("Operator", "R");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));


                        getRoomlist(requestBody);
                    } else {
                        Toast.makeText(cotEnrollmentAllocation.this, cotEnrollmentListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CotEnrollmentListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void AllocatingCot(Map<String, Object> requestBody, final String s) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.AddCotEnrollment(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    pDialog.dismiss();
                    AddBranchResponse addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(cotEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(cotEnrollmentAllocation.this, "Allocating has been "+s+" successfully..!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(cotEnrollmentAllocation.this,cotEnrollmentList.class));
                        finish();
                    } else {
                        Toast.makeText(cotEnrollmentAllocation.this, addBranchResponse.getError(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getCotList(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListCot(requestBody).enqueue(new Callback<CotListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<CotListResponse> call, Response<CotListResponse> response) {
                try {
                    cotListResponse = response.body();
                    pDialog.dismiss();
                    if (cotListResponse == null) {
                        Toast.makeText(cotEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (cotListResponse.getResponse().isEmpty()) {
                        Toast.makeText(cotEnrollmentAllocation.this, "No Cot Records Found..!!", Toast.LENGTH_SHORT).show();
                    }
                    getIsOccupiedlist = new ArrayList<CotListResponse.ResponseBean>();
                    List<String> usertypelist = new ArrayList<String>();
                    if (cotListResponse != null) {
                        for (CotListResponse.ResponseBean list : cotListResponse.getResponse()) {
                            if(list.getIsOccupied().equals("N")) {
                                getIsOccupiedlist.add(list);
                                usertypelist.add(list.getCotType() + " & Cot No: " + list.getCotNo());
                            }
                        }
                    }
                    //OccupaiedcotListResponse = getIsOccupiedlist;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(cotEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, usertypelist);
                    CotList.setAdapter(adapter);

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CotListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getRoomlist(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListPhysicalRoom(requestBody).enqueue(new Callback<PhysicalRoomListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<PhysicalRoomListResponse> call, Response<PhysicalRoomListResponse> response) {
                try {
                    physicalRoomListResponse = response.body();
                    if (physicalRoomListResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(cotEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (physicalRoomListResponse.getResponse().isEmpty()) {
                        pDialog.dismiss();
                        Toast.makeText(cotEnrollmentAllocation.this, "No Room Records Found..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<String> usertypelist = new ArrayList<String>();
                    int index = 0;
                    int pos = 0;
                    if (physicalRoomListResponse != null) {
                        for(int i =0; i<physicalRoomListResponse.getResponse().size();i++){
                            usertypelist.add(physicalRoomListResponse.getResponse().get(i).getRoomType()+" & Room No: "+physicalRoomListResponse.getResponse().get(i).getRoomNo());
                            if(cotEnrollmentListResponse!=null&& !cotEnrollmentListResponse.getResponse().isEmpty()){
                                if(cotEnrollmentListResponse.getResponse().get(0).getRoomNo().equals(physicalRoomListResponse.getResponse().get(i).getRoomNo())){
                                    pos = i;
                                }
                            }
                        }
//                        for (PhysicalRoomListResponse.ResponseEntity list : physicalRoomListResponse.getResponse()) {
//                            usertypelist.add(list.getRoomType()+" & Room No: "+list.getRoomNo());
//                            if(cotEnrollmentListResponse!=null&& !cotEnrollmentListResponse.getResponse().isEmpty()){
//                                if(cotEnrollmentListResponse.getResponse().get(0).getRoomNo().equals(list.getRoomNo())){
//                                    pos = index;
//                                }
//                            }
//                            index++;
//                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(cotEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, usertypelist);
                    RoomList.setAdapter(adapter);
                    if(!Objects.equals(CID, "")){
                        RoomList.setSelection(pos);
                    }

                    Map<String, Object> requestBody = new HashMap<>();

                    requestBody.put("Operator", "R");
                    requestBody.put("JWT", manager.getSharedPreferencesValues(cotEnrollmentAllocation.this, "JWT"));

                    getListTenant(requestBody);

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PhysicalRoomListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getListTenant(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getListTenant(requestBody).enqueue(new Callback<TenantListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<TenantListResponse> call, Response<TenantListResponse> response) {
                try {
                    pDialog.dismiss();
                    tenantListResponse = new TenantListResponse();
                    tenantListResponse = response.body();

                    if (tenantListResponse == null) {
                        Toast.makeText(cotEnrollmentAllocation.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (tenantListResponse.getMessage().trim().equals("Success")) {

                        if (tenantListResponse.getResponse().isEmpty()) {
                            Toast.makeText(cotEnrollmentAllocation.this, "Tenant list is empty..!!", Toast.LENGTH_SHORT).show();
                        }
                        List<String> employeelist = new ArrayList<String>();
                        if (tenantListResponse != null) {
                            for (TenantListResponse.Response list : tenantListResponse.getResponse()) {
                                employeelist.add(list.getTenantName());
                            }
                        }
                        if(employeelist.isEmpty()){
                            employeelist.add("Tenant is Empty..!!");
                            //tenant.setEnabled(false);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(cotEnrollmentAllocation.this, android.R.layout.simple_dropdown_item_1line, employeelist);
                        tenant.setAdapter(adapter);

                    } else {

                        Toast.makeText(cotEnrollmentAllocation.this, tenantListResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TenantListResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(cotEnrollmentAllocation.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
