package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.BranchList;
import com.example.pghostel.Activity.CotList;
import com.example.pghostel.Activity.CotTypesList;
import com.example.pghostel.Activity.ReportDashboard;
import com.example.pghostel.Activity.Reports;
import com.example.pghostel.Activity.informatiomReports.BranchReports;
import com.example.pghostel.Activity.informatiomReports.RoomReports;
import com.example.pghostel.Activity.informatiomReports.RoomTypeReports;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.CotListResponse;
import com.example.pghostel.ModelClass.ReportsResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;



public class ReportsListAdapter extends RecyclerView.Adapter<ReportsListAdapter.FacilityHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AlertDialog alertDialog;
    private SessionManager manager;
    private ProgressDialog pDialog;
    //we are storing all the products in a list
    private List<ReportsResponse.Response> responses;
    private AddBranchResponse addBranchResponse;
    private int reportSec = 0;

    //getting the context and product list with constructor
    public ReportsListAdapter(Context mCtx, List<ReportsResponse.Response> rommfacilitylist,int i) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.reportSec = i;
        this.responses = rommfacilitylist;
    }

    @Override
    public FacilityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childfacilitylist, null);
        return new FacilityHolder(view);
    }

    @Override
    public void onBindViewHolder(final FacilityHolder holder, final int position) {
        //getting the product of the specified position
        final ReportsResponse.Response roomfacilitylist = responses.get(position);

        //binding the data with the viewholder views
//        holder.facilityname.setText(roomfacilitylist.getBranchName());
//        holder.facilityPrice.setText("Room No: "+roomfacilitylist.getRoomNo());
//        holder.facilityStatus.setText("Available Cots: "+roomfacilitylist.getNoOfCotsAvailable());
//        holder.facilityStatus.setTextColor(mCtx.getResources().getColor(R.color.black));
//
//        if (roomfacilitylist.getIsOccupied().equals("N")) {
//            holder.facilityNotes.setText("IsOccupied: No");
//        }else {
//            holder.facilityNotes.setText("IsOccupied: Yes");
//        }
        if(reportSec==0 || reportSec==1){
            holder.facilityname.setText(roomfacilitylist.getBranchName());
            holder.facilityPrice.setText("Available Cots: "+roomfacilitylist.getNoOfCotsAvailable());
            holder.facilityStatus.setVisibility(View.GONE);
            holder.facilityNotes.setVisibility(View.GONE);
        }else if(reportSec==2){
            holder.facilityname.setText(roomfacilitylist.getRoomType());
            holder.facilityPrice.setText("Available Cots: "+roomfacilitylist.getNoOfCotsAvailable());
            holder.facilityStatus.setVisibility(View.GONE);
            holder.facilityNotes.setVisibility(View.GONE);
        }else if(reportSec==3){
            holder.facilityname.setText("Room No: "+roomfacilitylist.getRoomNo());
            holder.facilityPrice.setText("Available Cots: "+roomfacilitylist.getNoOfCotsAvailable());
            holder.facilityStatus.setVisibility(View.GONE);
            holder.facilityNotes.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //showCustomDialog(responses.get(position));
                if(reportSec==0){
                    if(manager.getSharedPreferencesValues(mCtx, "LoginCheck").equals("BYes")){
                        Intent i=new Intent(mCtx, BranchReports.class);
                        i.putExtra("BID",Integer.parseInt(manager.getSharedPreferencesValues(mCtx, "BranchID")));
                        mCtx.startActivity(i);
                    }else {
                        Intent i=new Intent(mCtx, BranchList.class);
                        i.putExtra("status",1);
                        mCtx.startActivity(i);
                    }

                }else if(reportSec==1){
                    Intent i=new Intent(mCtx, RoomTypeReports.class);
                    i.putExtra("BID",Integer.parseInt(responses.get(position).getBranchId()));
                    i.putExtra("RTID",responses.get(position).getRoomTypeId());
                    mCtx.startActivity(i);
                }else if(reportSec==2){
                    Intent i=new Intent(mCtx, RoomReports.class);
                    i.putExtra("BID",Integer.parseInt(responses.get(position).getBranchId()));
                    i.putExtra("RTID",responses.get(position).getRoomTypeId());
                    i.putExtra("RID",responses.get(position).getRoomId());
                    mCtx.startActivity(i);
                }else if(reportSec==3){

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }


    class FacilityHolder extends RecyclerView.ViewHolder {

        TextView facilityname;
        TextView facilityPrice;
        TextView facilityNotes;
        TextView facilityStatus;


        public FacilityHolder(View itemView) {
            super(itemView);

            facilityname = itemView.findViewById(R.id.facilityName);
            facilityPrice = itemView.findViewById(R.id.facilityPrice);
            facilityNotes = itemView.findViewById(R.id.facilityNotes);
            facilityStatus = itemView.findViewById(R.id.facilityStatus);
            // Servicetype = itemView.findViewById(R.id.Servicetype);

        }
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final ReportsResponse.Response branchlist) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //viewGroup = root.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.addcot, viewGroup, false);


        Button add = (Button) dialogView.findViewById(R.id.buttonadd);
        final EditText cotno = (EditText) dialogView.findViewById(R.id.cotno);
        final EditText discount = (EditText) dialogView.findViewById(R.id.discount);
        final EditText notes = (EditText) dialogView.findViewById(R.id.notes);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();


//        add.setText("Update");
//        cotno.setText(branchlist.getCotNo());
//        discount.setText(branchlist.getDiscount());
//        notes.setText(branchlist.getNotes());

    }
}
