package com.example.pghostel.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pghostel.ModelClass.BranchDetailResponse;
import com.example.pghostel.ModelClass.LoginResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

//    @BindView(R.id.login_emailid)
//    EditText emailid;
//    @BindView(R.id.login_password) EditText password;
//    @BindView(R.id.show_hide_password) CheckBox show_hide_password;
//    @BindView(R.id.loginBtn) Button loginBtn;

    EditText emailid;
    EditText password;
    TextView createAccount;
    CheckBox show_hide_password;
    Button loginBtn;
    SessionManager manager;
    ProgressDialog pDialog;
    LoginResponse loginResponse;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //   ButterKnife.bind(this);
        emailid = (EditText) findViewById(R.id.login_emailid);
        password = (EditText) findViewById(R.id.login_password);
        createAccount = (TextView) findViewById(R.id.createAccount);
        show_hide_password = (CheckBox) findViewById(R.id.show_hide_password);
        loginBtn = (Button) findViewById(R.id.loginBtn);

        Objects.requireNonNull(getSupportActionBar()).hide();

        manager = new SessionManager();

        if (manager.getSharedPreferencesValues(getApplicationContext(), "LoginCheck").equals("Yes")) {
            Intent it = new Intent(Login.this, HomeDashBoard.class);
            startActivity(it);
            finish();
        }else if(manager.getSharedPreferencesValues(getApplicationContext(), "LoginCheck").equals("BYes")) {
            Intent i=new Intent(Login.this, BranchDashboard.class);
           // i.putExtra("ID",manager.getSharedPreferencesValues(Login.this, "BranchID"));
           // manager.setjwt(Login.this, "BranchName", list.getBranchName());
            startActivity(i);
            finish();
        }
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailid.getText().toString().trim().length() == 0) {
                    Toast.makeText(Login.this, "Username can't be Empty..!!", Toast.LENGTH_SHORT).show();
                } else if (password.getText().toString().trim().length() == 0) {
                    Toast.makeText(Login.this, "Password can't be Empty..!!", Toast.LENGTH_SHORT).show();
                } else {
                    pDialog = ProgressDialog.show(Login.this, Html.fromHtml("<b><font color='#ff8f61'>Signing In</font></b>"), "Please wait ...");

                    Map<String, String> requestBody = new HashMap<>();

                    requestBody.put("UserId", emailid.getText().toString().trim());
                    requestBody.put("Password", password.getText().toString().trim());

                    manager.Clear(Login.this);
                    login(requestBody);
                }
            }
        });
        password.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (emailid.getText().toString().trim().length() == 0) {
                        Toast.makeText(Login.this, "Username can't be Empty..!!", Toast.LENGTH_SHORT).show();
                    } else if (password.getText().toString().trim().length() == 0) {
                        Toast.makeText(Login.this, "Password can't be Empty..!!", Toast.LENGTH_SHORT).show();
                    } else {
                        pDialog = ProgressDialog.show(Login.this, Html.fromHtml("<b><font color='#ff8f61'>Signing In</font></b>"), "Please wait ...");

                        Map<String, String> requestBody = new HashMap<>();

                        requestBody.put("UserId", emailid.getText().toString().trim());
                        requestBody.put("Password", password.getText().toString().trim());

                        manager.Clear(Login.this);
                        ;
                        login(requestBody);
                    }
                    return true;
                }
                return false;
            }
        });
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(Login.this, Signup.class);
                startActivity(it);
            }
        });
        show_hide_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    show_hide_password.setText(R.string.hide_pwd);// change

                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    password.setTransformationMethod(HideReturnsTransformationMethod
                            .getInstance());// show password
                } else {
                    show_hide_password.setText(R.string.show_pwd);// change

                    password.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setTransformationMethod(PasswordTransformationMethod
                            .getInstance());// hide password
                }
            }
        });

    }

    private void login(Map<String, String> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getLoginDetails(requestBody).enqueue(new Callback<LoginResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                try {
                    loginResponse = response.body();
                    if (loginResponse == null) {
                        Toast.makeText(Login.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (loginResponse.getMessage().trim().equals("Success")) {


                        if(loginResponse.getResponse().get(0).getUserType().equals("Admin")) {
                            manager.setjwt(Login.this, "LoginCheck", "Yes");
                            manager.setjwt(Login.this, "JWT", loginResponse.getResponse().get(0).getJWT());


                            pDialog.cancel();
                            //Write whatever to want to do after delay specified (1 sec)
                            Intent it = new Intent(Login.this, HomeDashBoard.class);
                            startActivity(it);
                            finish();
                        }else {
                            manager.setjwt(Login.this, "LoginCheck", "BYes");
                            manager.setjwt(Login.this, "JWT", loginResponse.getResponse().get(0).getJWT());
                            manager.setjwt(Login.this, "BranchID", loginResponse.getResponse().get(0).getBranchId());

                            Map<String, Object> requestBody = new HashMap<>();
                            final Map BranchDetails = new LinkedHashMap(1);
                            BranchDetails.put("BranchId", loginResponse.getResponse().get(0).getBranchId());
                            requestBody.put("Operator", "R");
                            requestBody.put("JWT", loginResponse.getResponse().get(0).getJWT());
                            requestBody.put("BranchDetails", BranchDetails);
                            getBranchItemDetails(requestBody);

                        }

                    } else {
                        pDialog.cancel();
                        Toast.makeText(Login.this, loginResponse.getMessage().trim(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable e) {
                    pDialog.cancel();
                    Toast.makeText(Login.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pDialog.cancel();
                Toast.makeText(Login.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBranchItemDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getBranchItemDetails(requestBody).enqueue(new Callback<BranchDetailResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<BranchDetailResponse> call, Response<BranchDetailResponse> response) {
                try {
                    // if(response.code()==201) {
                    BranchDetailResponse branchDetailResponse = response.body();
                    if (branchDetailResponse == null) {
                        pDialog.dismiss();
                        Toast.makeText(Login.this, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (branchDetailResponse.getMessage().equals("Success")) {
                        for (BranchDetailResponse.Response list : branchDetailResponse.getResponse()) {
                            Intent i=new Intent(Login.this, BranchDashboard.class);
                            manager.setjwt(Login.this, "BranchName", list.getBranchName());
                            startActivity(i);
                            finish();
                        }
                        pDialog.cancel();
                    } else {
                        pDialog.dismiss();
                        Toast.makeText(Login.this, branchDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(Login.this, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchDetailResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(Login.this, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


}
