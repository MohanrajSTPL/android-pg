package com.example.pghostel.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pghostel.Activity.BranchDashboard;
import com.example.pghostel.Activity.BranchDetails;
import com.example.pghostel.Activity.Facility;
import com.example.pghostel.Activity.Login;
import com.example.pghostel.Activity.Rooms;
import com.example.pghostel.Activity.TenantList;
import com.example.pghostel.Activity.informatiomReports.BranchReports;
import com.example.pghostel.Activity.informatiomReports.RoomTypeReports;
import com.example.pghostel.Interface.AdapterInterface;
import com.example.pghostel.ModelClass.AddBranchResponse;
import com.example.pghostel.ModelClass.BranchListResponse;
import com.example.pghostel.R;
import com.example.pghostel.Retrofit.ApiClient;
import com.example.pghostel.Retrofit.SessionManager;
import com.example.pghostel.ServiceAPI.ApiInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Branchlistadapter extends RecyclerView.Adapter<Branchlistadapter.BranchlistViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    private ViewGroup viewGroup;
    private AdapterInterface buttonListener;
    //we are storing all the products in a list
    private List<BranchListResponse.Response> branchListResponseList;
    private SessionManager manager;
    private ProgressDialog pDialog;
    private AddBranchResponse addBranchResponse;
    private int status = 0;

    //getting the context and product list with constructor
    public Branchlistadapter(Context mCtx, List<BranchListResponse.Response> branchlist,AdapterInterface fragment,int i) {
        manager = new SessionManager();
        this.mCtx = mCtx;
        this.status = i;
        this.branchListResponseList = branchlist;
        this.buttonListener = fragment;

    }
    @Override
    public BranchlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        this.viewGroup = parent;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.childbranchlist, null);
        return new BranchlistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BranchlistViewHolder holder, final int position) {
        //getting the product of the specified position
        final BranchListResponse.Response branchlist = branchListResponseList.get(position);

        //binding the data with the viewholder views
        holder.branchname.setText(branchlist.getBranchName());
        holder.Servicetype.setText(branchlist.getServiceType());
        holder.branchgender.setText(String.valueOf(branchlist.getBranchGender()));

        if(branchlist.getBranchActiveInd().equals("N")){
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.red));
            holder.branchStatus.setText("IN ACTIVE");
        }else {
            holder.branchStatus.setTextColor(mCtx.getResources().getColor(R.color.green));
            holder.branchStatus.setText("ACTIVE");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if(status==1){
                    Intent i=new Intent(mCtx, BranchReports.class);
                    i.putExtra("BID",branchListResponseList.get(position).getBranchId());
                    mCtx.startActivity(i);
                    return;
                }
                if(branchListResponseList.get(position).getBranchActiveInd().equals("Y") || branchListResponseList.get(position).getBranchActiveInd().equals("")) {

                    Intent i=new Intent(mCtx, BranchDashboard.class);
                    manager.setjwt(mCtx, "BranchID",String.valueOf(branchListResponseList.get(position).getBranchId()));
                    //i.putExtra("ID",branchListResponseList.get(position).getBranchId());
                    manager.setjwt(mCtx, "BranchName", branchListResponseList.get(position).getBranchName());
                    mCtx.startActivity(i);
                    //showCustomDialog(branchListResponseList.get(position).getBranchId());
                }else {
                    AlertDialog diaBox = AskOption(branchListResponseList.get(position));
                    diaBox.show();
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if(!branchListResponseList.get(position).getBranchActiveInd().equals("N") && status !=1) {
                    buttonListener.onMethodCallback(position);
                }
                return true;
            }
        });


    }


    @Override
    public int getItemCount() {
        return branchListResponseList.size();
    }


    class BranchlistViewHolder extends RecyclerView.ViewHolder {

        TextView branchname;
        TextView Servicetype;
        TextView branchgender;
        TextView branchStatus;



        public BranchlistViewHolder(View itemView) {
            super(itemView);

            branchname = itemView.findViewById(R.id.branchname);
            Servicetype = itemView.findViewById(R.id.Servicetype);
            branchgender = itemView.findViewById(R.id.branchgender);
            branchStatus = itemView.findViewById(R.id.branchStatus);

        }
    }
    private AlertDialog AskOption(final BranchListResponse.Response branchlist)
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mCtx)
                // set message, title, and icon
                .setTitle("Activate")
                .setMessage("Are you sure want to Activate this Branch?")


                .setPositiveButton("Activate", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Map<String, Object> requestBody = new HashMap<>();
                        Map BranchDetails = new LinkedHashMap(11);
                        BranchDetails.put("BranchId", branchlist.getBranchId());
                        BranchDetails.put("BranchName", branchlist.getBranchName());
                        BranchDetails.put("ServiceType", branchlist.getServiceType());
                        BranchDetails.put("BranchGender", branchlist.getBranchGender());
                        BranchDetails.put("BranchAddress", "");
                        BranchDetails.put("BranchCity", "");
                        BranchDetails.put("BranchState", "");
                        BranchDetails.put("BranchCountry", "");
                        BranchDetails.put("BranchPincode", "");
                        BranchDetails.put("BranchEmailId", "");
                        BranchDetails.put("BranchMobileNo", "");
                        BranchDetails.put("BranchActiveInd", "Y");
                        requestBody.put("Operator", "U");
                        requestBody.put("JWT", manager.getSharedPreferencesValues(mCtx, "JWT"));
                        requestBody.put("BranchDetails", BranchDetails);
                        AddBranchListDetails(requestBody);
                        pDialog = ProgressDialog.show(mCtx, Html.fromHtml("<b><font color='#ff8f61'>Activating...</font></b>"), "Please wait ...");

                        dialog.dismiss();
                    }

                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    private void AddBranchListDetails(Map<String, Object> requestBody) {

        ApiInterface apiInterface = ApiClient.getLogin().create(ApiInterface.class);

        apiInterface.getAddbranchDetails(requestBody).enqueue(new Callback<AddBranchResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
                try {
                    // if(response.code()==201) {
                    pDialog.dismiss();
                    addBranchResponse = response.body();
                    if (addBranchResponse == null) {
                        Toast.makeText(mCtx, "Something went wrong..!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (addBranchResponse.getMessage().equals("Success")) {
                        Toast.makeText(mCtx, "Branch has been Activated successfully..!!", Toast.LENGTH_SHORT).show();
                        buttonListener.onMethodCallbackActivate();
                    } else {
                        Toast.makeText(mCtx, addBranchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Throwable e) {
                    pDialog.dismiss();
                    Toast.makeText(mCtx, "Unable to connect server" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddBranchResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mCtx, "Unable to connect server" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showCustomDialog(final int ID) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(Objects.requireNonNull(mCtx)).inflate(R.layout.viewbranchlist, viewGroup, false);


        Button branchBtn = (Button) dialogView.findViewById(R.id.branchdetailBtn);
        Button facilityBtn = (Button) dialogView.findViewById(R.id.facilityBtn);
        Button roomBtn = (Button) dialogView.findViewById(R.id.roomBtn);
        Button tenant = (Button) dialogView.findViewById(R.id.tenantBtn);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(mCtx));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        branchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, BranchDetails.class);
                i.putExtra("ID",ID);
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });
        facilityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, Facility.class);
                i.putExtra("ID",ID);
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });
        roomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, Rooms.class);
                i.putExtra("ID",ID);
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });

        tenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mCtx, TenantList.class);
                i.putExtra("ID",ID);
                mCtx.startActivity(i);
                alertDialog.dismiss();
            }
        });
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int selectedId = listofviews.getCheckedRadioButtonId();
//                // find the radiobutton by returned id
//                RadioButton radioButton = (RadioButton) dialogView.findViewById(selectedId);
//                if(selectedId==-1){
//                    Toast.makeText(mCtx,"Plese select anyone of these..!!", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                Intent i;
//                if(radioButton.getText().toString().equals("Branch Detail")){
//                    i=new Intent(mCtx, BranchDetails.class);
//                    i.putExtra("ID",ID);
//                }else if(radioButton.getText().toString().equals("Room")){
//                    i=new Intent(mCtx, Rooms.class);
//                    i.putExtra("ID",ID);
//                }else {
//                    i=new Intent(mCtx, Facility.class);
//                    i.putExtra("ID",ID);
//                }
//
//                    mCtx.startActivity(i);
//                    alertDialog.dismiss();
//
//            }
//        });
    }
}
