package com.example.pghostel.Retrofit;

import android.content.Context;
import android.widget.Toast;

public class Utils {

    public void showToast(Context context,String s){
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }
}
